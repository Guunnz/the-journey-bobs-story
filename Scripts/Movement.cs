﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{

	public KeyCode izq;
	public KeyCode der;
	public KeyCode jump;
	public bool ground;
	
	// Update is called once per frame
	void Update ()
	{
			if (Input.GetKey (izq)) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (-7f, GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKey (der)) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (7f, GetComponent<Rigidbody2D> ().velocity.y);
			}
			if (Input.GetKey (jump) && ground == true) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, 17f);
			}
		}

	}


