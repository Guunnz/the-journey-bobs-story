﻿using UnityEngine;
using System.Collections;

public class ScriptEnabler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (UpgradeSystem.onPause)
        {
            this.gameObject.GetComponent<WaveSpawner>().enabled = false;
        }
        if (!UpgradeSystem.onPause)
        {
            this.gameObject.GetComponent<WaveSpawner>().enabled = true;
        }
    }
}
