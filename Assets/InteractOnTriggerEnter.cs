﻿using UnityEngine;
using System.Collections;

public class InteractOnTriggerEnter : MonoBehaviour
{

    private Transform interact;


    private void Awake()
    {
        interact = transform.Find("Interact");
    }


    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            interact.gameObject.SetActive(true);
        }
    }
    void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            interact.gameObject.SetActive(false);
        }
    }
}


