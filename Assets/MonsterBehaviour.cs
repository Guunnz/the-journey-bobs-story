﻿using UnityEngine;
using System.Collections;

public class MonsterBehaviour : MonoBehaviour {

    public float monsterHP = 100;
    [Tooltip("Target to follow.")]
    public Transform target;
    public GameObject Isaac;
    public GameObject FollowMeIa;
    public bool beingDamaged;
    public bool beingDamaged1;
    public bool beingDamaged2;
    public int moneyDrop;
    public int moneyMinDrop = 50;
    public int pointsDrop;
    public bool ifKamikaze;
    [Tooltip("Optional offset from the target's position to move towards.")]
    public Vector3 followOffset;

    [Tooltip("Speed to move towards the target.")]
    public float followSpeed;

    [Tooltip("True if this object should rotate to face the target.")]
    public bool lookAtTarget;

    [Tooltip("Speed to rotate towards the target.")]
    public float lookSpeed;
    private void Awake()
    {
        Isaac = GameObject.Find("Isaac");
        target = Isaac.transform;
    }


    private void Update()
    {

        if (monsterHP <= 0)
        {
            FindObjectOfType<AudioManager>().Play("Death");
            IsaacController.points += pointsDrop;
            IsaacController.monay += Random.Range(moneyMinDrop, moneyDrop);
            Destroy(gameObject);
        }
    }
   

    private void LateUpdate()
    { 
        // move towards the target position ( plus the offset ), never moving farther than "followSpeed" in one frame.
        transform.position = Vector3.MoveTowards(transform.position, target.position + followOffset, followSpeed);

        if (lookAtTarget)
        {
            // get a rotation that points Z axis forward, and the Y axis towards the target
            Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, (target.position - transform.position));

            // rotate toward the target rotation, never rotating farther than "lookSpeed" in one frame.
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, lookSpeed);

            // rotate 90 degrees around the Z axis to point X axis instead of Y
            transform.Rotate(0, 0, 90);
        }
    }


    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Brimstone") && !beingDamaged)
        {
            FindObjectOfType<AudioManager>().Play("Tdmg");
            beingDamaged = true;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
           this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            monsterHP -= IsaacBrimstoneManager.StaticBrimDamage;
            Debug.Log(monsterHP + "HP");
            Invoke("AfterDamage", 0.5f);
        }
        if (other.CompareTag("Brimstone1") && !beingDamaged1)
        {
            FindObjectOfType<AudioManager>().Play("Tdmg");
            beingDamaged1 = true;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            monsterHP -= IsaacBrimstoneManager.StaticBrimDamage;
            Debug.Log(monsterHP + "HP");
            Invoke("AfterDamage", 0.5f);
        }
        if (other.CompareTag("Brimstone2") && !beingDamaged2)
        {
            FindObjectOfType<AudioManager>().Play("Tdmg");
            beingDamaged2 = true;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            monsterHP -= IsaacBrimstoneManager.StaticBrimDamage;
            Debug.Log(monsterHP + "HP");
            Invoke("AfterDamage", 0.5f);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Shield" && !beingDamaged)
        {
            FindObjectOfType<AudioManager>().Play("Shield");
            beingDamaged = true;
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
            monsterHP -= 300;
            Debug.Log(monsterHP + "HP");
            Invoke("AfterDamage", 0.2f);
        }
        if (other.gameObject.tag == "Player" && ifKamikaze)
        {
            FindObjectOfType<AudioManager>().Play("Kamikaze");
            Invoke("DestroyThis", 0.1f);
        }
    }

    void DestroyThis()
    {
        Destroy(this.gameObject);
    }
    void AfterDamage()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        beingDamaged = false;
        beingDamaged1 = false;
        beingDamaged2 = false;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }
}
