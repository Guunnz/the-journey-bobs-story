﻿using UnityEngine;
using System.Collections;

public class ShieldScript : MonoBehaviour {

    public GameObject pj;
    float angle = 360.0f; // Degree per time unit
    float time = 1.0f; // Time unit in sec
    Vector3 axis = Vector3.up; // Rotation axis, here it the yaw axis
    static public float shieldSpeed = 2.5f;


    // Update is called once per frame
    void Update ()
    {
        gameObject.transform.position = new Vector3(pj.transform.position.x, pj.transform.position.y, pj.transform.position.z);
        this.gameObject.transform.Rotate(Vector3.back * shieldSpeed);
    }
}
