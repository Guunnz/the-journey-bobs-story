﻿using UnityEngine;
using System.Collections;

public class AfterYetiTP : MonoBehaviour {

    public GameObject Player;
    public GameObject EndLevelFade;

	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D info)
    {
        if (info.tag == "Player")
        {
            EndLevelFade.SetActive(true);
            Invoke("MovePlayer", 1.2f);
        }
	
	}

    void FadeOff()
    {
        EndLevelFade.SetActive(false);
    }

    void MovePlayer()
    {
        Player.transform.localPosition = new Vector2(-600.3f, 501.8f);
        Invoke("FadeOff", 1.2f);
    }
        
}
