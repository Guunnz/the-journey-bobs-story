﻿using UnityEngine;
using System.Collections;

public class xAxisButtonScript : MonoBehaviour {

    public float objectXTarget;
    public float objectXBase;
    public float objectActualX;
    public GameObject objectToMove;
    public bool buttonTouched;
    public float objectSpeed = 20;
    public bool goingLeft;
    private float yValue;
    private float syValue;
    private float syMovementValue;
    public bool boxin;
    public bool playerin;
    private void Start()
    {
        syValue = transform.position.y;
        yValue = transform.position.y - 0.4f;
    }
    private void Update()
    {
        if (boxin)
        {
            buttonTouched = true;
        }
        if (playerin)
        {
            buttonTouched = true;
        }
        if (!playerin && !boxin)
        {
            buttonTouched = false;
        }
        if (buttonTouched && !goingLeft)
        {
            objectToMove.transform.position = new Vector3(objectActualX, objectToMove.transform.position.y, objectToMove.transform.position.z);
            objectActualX += objectSpeed * Time.deltaTime;
        }
        if (objectActualX > objectXTarget && !goingLeft)
        {
            goingLeft = true;
        }
        if (buttonTouched && goingLeft)
        {
            objectToMove.transform.position = new Vector3(objectActualX, objectToMove.transform.position.y, objectToMove.transform.position.z);
            objectActualX -= objectSpeed * Time.deltaTime;
        }
        if (objectActualX < objectXBase && goingLeft)
        {
            goingLeft = false;
        }
    }

    void OnTriggerEnter2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            playerin = true;
            FindObjectOfType<AudioManager>().Play("PressurePlate");
            gameObject.transform.position = new Vector2(transform.position.x, yValue);
            buttonTouched = true;
        }
        if (info.tag == "Box")
        {
            boxin = true;
            FindObjectOfType<AudioManager>().Play("PressurePlate");
            gameObject.transform.position = new Vector2(transform.position.x, yValue);
            buttonTouched = true;
        }
    }
    private void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            playerin = false;
            buttonTouched = false;
            gameObject.transform.position = new Vector2(transform.position.x, syValue);
        }
        if (info.tag == "Box")
        {
            boxin = false;
            buttonTouched = false;
            gameObject.transform.position = new Vector2(transform.position.x, syValue);
        }
    }
}
