﻿using UnityEngine;
using System.Collections;

public class StopMusicOnTrigger : MonoBehaviour {

    public string AudioToStop;
    public bool lowerVolume;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            lowerVolume = true;  
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (lowerVolume)
        {
            FindObjectOfType<AudioManager>().VolumeDown(AudioToStop);
        }
	}
}
