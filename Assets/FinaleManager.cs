﻿using UnityEngine;
using System.Collections;

public class FinaleManager : MonoBehaviour {

    public GameObject superSecret;
   static public int buttonsPressedGreen;
   static public int buttonsPressedBlue;
   static public int buttonsPressedRed;
    public int whichPhase;
    public bool greenON;
    public bool blueON;
    public bool redON;
    public bool onLever;
    public bool JumpersDeactivated;
    public float greenTimer = 120;
    public float blueTimer = 180;
    public float redTimer = 330;
    public GameObject principalDoor;
    public GameObject blueDoors;
    public GameObject redDoors;
    public GameObject EndGameDoor;
    public GameObject bouncingPlat0;
    public GameObject bouncingPlat1;
    public GameObject bouncingPlat2;
    public GameObject bouncingPlat3;
    public GameObject bouncingPlat4;
    public GameObject bouncingPlat5;
    public GameObject bouncingPlat6;
    public GameObject bouncingPlat7;
    public GameObject bouncingPlat8;
    public GameObject green1;
    public GameObject green2;
    public GameObject green3;
    public GameObject blue1;
    public GameObject blue2;
    public GameObject blue3;
    public GameObject blue4;
    public GameObject red1;
    public GameObject red2;
    public GameObject red3;
    public GameObject red4;
    public GameObject red5;
    public GameObject timer;
    public float timerTime;
    public float opacityValue = 0.5f;
    public bool opacityDecrease;
    static public bool reset;
    public bool greenDone;
    public bool blueDone;
    public bool redDone;
    public ParticleSystem finalePS;

    // Necesito gameobjects:
    // Puerta principal.
    // Puertas azules.
    // Puertas rojas.
    // onLever Trigger.
    // Puerta del final.
    // Plataformas cerca del startup.
    // Botones de todos los colores.

    void Update ()
    {
        if (timerTime <= 0)
        {
            timerTime = 0;
        }
        timer.transform.localScale = new Vector2(timerTime, transform.localScale.y);
        if (buttonsPressedGreen == 1 && greenON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
        }
        if (buttonsPressedGreen == 2 && greenON)
        {
            green2.GetComponent<SpriteRenderer>().color = Color.green;
        }
        if (buttonsPressedGreen == 3 && greenON)
        {
            green3.GetComponent<SpriteRenderer>().color = Color.green;
        }
        if (buttonsPressedGreen == 0)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.gray;
            green2.GetComponent<SpriteRenderer>().color = Color.gray;
            green3.GetComponent<SpriteRenderer>().color = Color.gray;
        }
        if (buttonsPressedBlue == 0)
        {
            blue1.GetComponent<SpriteRenderer>().color = Color.gray;
            blue2.GetComponent<SpriteRenderer>().color = Color.gray;
            blue3.GetComponent<SpriteRenderer>().color = Color.gray;
            blue4.GetComponent<SpriteRenderer>().color = Color.gray;
        }
        if (buttonsPressedBlue == 1 && blueON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (buttonsPressedBlue == 2 && blueON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (buttonsPressedBlue == 3 && blueON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (buttonsPressedBlue == 4 && blueON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (buttonsPressedRed == 0)
        {
            red1.GetComponent<SpriteRenderer>().color = Color.gray;
            red2.GetComponent<SpriteRenderer>().color = Color.gray;
            red3.GetComponent<SpriteRenderer>().color = Color.gray;
            red4.GetComponent<SpriteRenderer>().color = Color.gray;
            red5.GetComponent<SpriteRenderer>().color = Color.gray;
        }
        if (buttonsPressedRed == 1 && redON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
            red1.GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (buttonsPressedRed == 2 && redON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
            red2.GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (buttonsPressedRed == 3 && redON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
            red3.GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (buttonsPressedRed == 4 && redON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
            red4.GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (buttonsPressedRed == 5 && redON)
        {
            green1.GetComponent<SpriteRenderer>().color = Color.green;
            green2.GetComponent<SpriteRenderer>().color = Color.green;
            green3.GetComponent<SpriteRenderer>().color = Color.green;
            blue1.GetComponent<SpriteRenderer>().color = Color.blue;
            blue2.GetComponent<SpriteRenderer>().color = Color.blue;
            blue3.GetComponent<SpriteRenderer>().color = Color.blue;
            blue4.GetComponent<SpriteRenderer>().color = Color.blue;
            red5.GetComponent<SpriteRenderer>().color = Color.red;
        }

        if (JumpersDeactivated == true)
        {
            bouncingPlat0.SetActive(false);
            bouncingPlat1.SetActive(false);
            bouncingPlat2.SetActive(false);
            bouncingPlat3.SetActive(false);
            bouncingPlat4.SetActive(false);
            bouncingPlat5.SetActive(false);
            bouncingPlat6.SetActive(false);
            bouncingPlat7.SetActive(false);
            bouncingPlat8.SetActive(false);
            JumpersDeactivated = false;
        }
        if (opacityDecrease)
        {
            red5.GetComponent<SpriteRenderer>().color = Color.red;
            red4.GetComponent<SpriteRenderer>().color = Color.red;
            red3.GetComponent<SpriteRenderer>().color = Color.red;
            red2.GetComponent<SpriteRenderer>().color = Color.red;
            red1.GetComponent<SpriteRenderer>().color = Color.red;
            EndGameDoor.GetComponent<SpriteRenderer>().color = new Color(255f, 0f, 206f, opacityValue);
            EndGameDoor.transform.position = new Vector2(EndGameDoor.transform.position.x, EndGameDoor.transform.position.y + 20f * Time.deltaTime);
            opacityValue -= 0.2f * Time.deltaTime;
        }
        if (opacityValue <= 0)
        {
            EndGameDoor.GetComponent<BoxCollider2D>().enabled = false;
            opacityDecrease = false;
        }
        if (Input.GetKeyDown(InputManager.E) && whichPhase == 0 && onLever)
        {
            FindObjectOfType<AudioManager>().Play("ColorPlatform");
            FindObjectOfType<AudioManager>().Play("novel");
            whichPhase = 1;
            finalePS.startColor = new Color(0, 255, 0, 0.1f);
            Debug.Log("game starts");
            principalDoor.SetActive(false);
        }
        if (Input.GetKeyDown(InputManager.E) && buttonsPressedGreen == 3 && onLever && greenTimer > 1 && !greenDone)
        {
            FinalePressurePlate.reset = true;
            finalePS.startColor = new Color(0, 0, 255, 0.1f);
            greenDone = true;
            Debug.Log("GreenCompleted");
            GreenCompleted();
            BlueReset();
        }
        if (Input.GetKeyDown(InputManager.E) && buttonsPressedBlue == 4 && onLever && blueTimer > 1 && !blueDone)
        {
            FinalePressurePlate.reset = true;
            blueDone = true;
            Debug.Log("bluecompleted");
            finalePS.startColor = new Color(255, 0, 0, 0.1f);
            BlueCompleted();
            RedReset();
        }
        if (Input.GetKeyDown(InputManager.E) && buttonsPressedRed == 5 && onLever && redTimer > 1)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("redcompleted");
            RedCompleted();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever && !greenDone && buttonsPressedGreen != 3)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("greenreset");
            GreenReset();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever && !blueDone && buttonsPressedBlue != 4)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("bluereset");
            BlueReset();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("redreset");
            RedReset();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever && greenTimer <= 0 && !greenDone)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("greenreset");
            GreenReset();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever && blueTimer <= 0 && greenDone && !blueDone)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("bluereset");
            BlueReset();
        }
        if (Input.GetKeyDown(InputManager.E) && onLever && redTimer <= 0 && blueDone && !redDone)
        {
            FinalePressurePlate.reset = true;
            Debug.Log("redreset");
            RedReset();
        }
        if (whichPhase == 1)
         {
            timerTime -= 0.0916666666666667f * Time.deltaTime;
            greenTimer -= Time.deltaTime;
            greenON = true;
            blueON = false;
            redON = false;
         }
         if (whichPhase == 2)
         {
            timerTime -= 0.0611111111111111f * Time.deltaTime;
            blueTimer -= Time.deltaTime;
            greenON = false;
            blueON = true;
            redON = false;
         }
         if (whichPhase == 3 && !redDone)
         {
            timerTime -= 0.0333333333333333f * Time.deltaTime;
            redTimer -= Time.deltaTime;
            greenON = false;
            blueON = false;
            redON = true;
         }
         if (redDone)
        {
            redTimer = 330;
            FindObjectOfType<AudioManager>().VolumeFade("novel");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            onLever = true;
            finalePS.enableEmission = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            onLever = false;
        }
    }

    void GreenCompleted()
    {
        FinalePressurePlate.reset = true;
        timer.GetComponent<SpriteRenderer>().color = Color.blue;
        blueDoors.SetActive(false);
        JumpersDeactivated = true;
        whichPhase = 2;
        timerTime = 11;
    }
    void BlueCompleted()
    {
        FinalePressurePlate.reset = true;
        timerTime = 11;
        redDoors.SetActive(false);
        JumpersDeactivated = true;
        whichPhase = 3;
        timer.transform.localScale = new Vector2(11, transform.localScale.y);
        timer.GetComponent<SpriteRenderer>().color = Color.red;
    }
    void RedCompleted()
    {
        redDone = true;
        FinalePressurePlate.reset = true;
        JumpersDeactivated = true;
        opacityDecrease = true;
        superSecret.SetActive(false);
        red5.GetComponent<SpriteRenderer>().color = Color.red;
        red4.GetComponent<SpriteRenderer>().color = Color.red;
        red3.GetComponent<SpriteRenderer>().color = Color.red;
        red2.GetComponent<SpriteRenderer>().color = Color.red;
        red1.GetComponent<SpriteRenderer>().color = Color.red;
    }
    void GreenReset()
    {
        FinalePressurePlate.reset = true;
        timerTime = 11;
        reset = true;
        buttonsPressedGreen = 0;
        JumpersDeactivated = true;
        greenTimer = 120;
    }
    void BlueReset()
    {
        FinalePressurePlate.reset = true;
        timerTime = 11;
        reset = true;
        buttonsPressedBlue = 0;
        JumpersDeactivated = true;
        blueTimer = 180;
    }
    void RedReset()
    {
        if (redDone == false)
        {
            FinalePressurePlate.reset = true;
            timerTime = 11;
            reset = true;
            buttonsPressedRed = 0;
            JumpersDeactivated = true;
            redTimer = 330;
        }
    }
}
