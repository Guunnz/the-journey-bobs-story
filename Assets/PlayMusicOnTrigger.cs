﻿using UnityEngine;
using System.Collections;

public class PlayMusicOnTrigger : MonoBehaviour {

    public string AudioToPlay;
    public bool increaseVolume;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play(AudioToPlay);
            increaseVolume = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (increaseVolume)
        {
            FindObjectOfType<AudioManager>().VolumeUp(AudioToPlay);
            Invoke("Destroy", 0.5f);
        }
    }
    void Destroy()
    {
        Destroy(gameObject);
    }
}
