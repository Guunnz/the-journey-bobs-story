﻿using UnityEngine;
using System.Collections;

public class AddRigidbodyToTree : MonoBehaviour {

    public GameObject Tree;



    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player" )
        {
                FindObjectOfType<AudioManager>().Play("TreeFalling");
            Tree.AddComponent<Rigidbody2D>();
        }
    }
}
