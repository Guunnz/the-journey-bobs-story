﻿using UnityEngine;
using System.Collections;

public class finaleButton : MonoBehaviour {
   private bool activated;
    public bool greenButton;
    public bool blueButton;
    public bool redButton;
    public ParticleSystem buttonPS;
    static public bool reset;


    private void Update()
    {
        if (FinaleManager.reset == true && greenButton && FinaleManager.buttonsPressedGreen < 3)
        {
            activated = false;
            Invoke("Reset", 0.5f);
        }
        if (FinaleManager.reset == true && redButton && FinaleManager.buttonsPressedRed < 5)
        {
            activated = false;
            Invoke("Reset", 0.5f);
        }
        if (FinaleManager.reset == true && blueButton && FinaleManager.buttonsPressedBlue < 4)
        {
            activated = false;
            Invoke("Reset", 0.5f);
        }
        if (activated)
        {
            buttonPS.enableEmission = false;
        }
        if (!activated)
        {
            buttonPS.enableEmission = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && greenButton && !activated)
        {
            FindObjectOfType<AudioManager>().Play("ColorPlatform");
               activated = true;
            FinaleManager.buttonsPressedGreen += 1;
            Debug.Log(FinaleManager.buttonsPressedGreen + " de 3 verdes");
        }
        if (collision.tag == "Player" && blueButton && !activated)
        {
            FindObjectOfType<AudioManager>().Play("ColorPlatform");
            activated = true;
            FinaleManager.buttonsPressedBlue += 1;
            Debug.Log(FinaleManager.buttonsPressedBlue + " de 4 azules");
        }
        if (collision.tag == "Player" && redButton && !activated)
        {
            FindObjectOfType<AudioManager>().Play("ColorPlatform");
            activated = true;
            FinaleManager.buttonsPressedRed += 1;
            Debug.Log(FinaleManager.buttonsPressedRed + " de 5 rojos");
        }
    }
    private void Reset()
    {
        FinaleManager.reset = false;
        
    }
}
