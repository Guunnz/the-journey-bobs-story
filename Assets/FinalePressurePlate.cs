﻿using UnityEngine;
using System.Collections;

public class FinalePressurePlate : MonoBehaviour {

    public GameObject OnOff;
    public Transform pressurePlate;
    public bool triggerON;
    static public bool reset;
    private void Awake()
    {
        pressurePlate = transform.FindChild("PressurePlate");
    }

    private void Reset()
    {
        reset = false;
    }
    void Update ()
    {

        Debug.Log(reset);
        if (reset == true)
        {
            triggerON = false;
            Invoke("Reset", 0.01f);
        }
        if (triggerON)
        {
            pressurePlate.gameObject.transform.localPosition = new Vector2(pressurePlate.localPosition.x, -5.5f);
            OnOff.SetActive(true);
        }
        if (!triggerON)
        {
            pressurePlate.gameObject.transform.localPosition = new Vector2(pressurePlate.localPosition.x, -4);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play("PressurePlate");
            triggerON = true;  
        }
    }
}
