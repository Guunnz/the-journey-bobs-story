﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class IngameDialogue : MonoBehaviour
{
    public string sceneToLoad;
    public int dialogueNumber;
   public int dialogueManage;
    public int maxDialogue;
    public int dialogueReach = 5;
    public GameObject[] dialogos;
    public GameObject EmptySpot;
    public bool called;
    public bool onTrigger;
    public int emptySpot;
    public int dialogueReachAfterEmpty = 5;
    public int dialogueReachSuma = 5;
    
    // Update is called once per frame
    void Update()
    {
        if (dialogueNumber == dialogueReach && onTrigger)
        {
            DisableText();
            dialogueReach += dialogueReachSuma;
            Debug.Log("Siguiente Hoja");
        }
        if (dialogueNumber == dialogueManage && dialogueNumber < maxDialogue && !called && onTrigger)
        {
            called = true;
            if (dialogueNumber != 0 && onTrigger)
            {
                dialogos[dialogueManage - 1].SetActive(false);
            }
            dialogos[dialogueManage].SetActive(true);
        }
        if (Input.GetKeyDown(InputManager.E) && onTrigger)
        {
            NextDialogue();
        }
        if (dialogueNumber == maxDialogue && onTrigger)
        {
            DisableText();
            onTrigger = false;
        }
    }

    void NextDialogue()
    {
        // play audio.
        called = false;
        dialogueNumber += 1;
        dialogueManage += 1;
    }

    void DisableText()
    {
        for (int i = 0; i < dialogos.Length; i++)
        {
            dialogos[i].SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            onTrigger = true;
        }
    }
    void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            called = false;
            dialogueManage = dialogueNumber;
            dialogueNumber = dialogueManage;
            dialogueReach = 1;
            DisableText();
            onTrigger = false;
        }
    }
}

