﻿using UnityEngine;
using System.Collections;

public class MenuSprite : MonoBehaviour {


    public int whichSprite;
    public Sprite[] sprites;



    private void Update()
    {
        whichSprite = PlayerPrefs.GetInt("MenuSprite");
        gameObject.GetComponent<SpriteRenderer>().sprite = sprites[whichSprite];
    }
}
