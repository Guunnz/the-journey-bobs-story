﻿using UnityEngine;
using System.Collections;

public class ScoreBoard : MonoBehaviour {


    public GameObject[] scoreBoard; 
    static public float Score;
    public int scoreBoardNum;
    public GameObject Big1;
    public GameObject Big2;
    public GameObject Big3;
    public GameObject Big4;

    void Update ()
    {
        if (Score == 5)
        {
            scoreBoardNum = 0;
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
        }
        if (Score == 10)
        {
            scoreBoardNum = 0;
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
        }
        if (Score == 15)
        {
            scoreBoardNum = 0;
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
        }

        if (Score <= 4.5f)
        {
            Big1.SetActive(false);
            Big2.SetActive(false);
            Big3.SetActive(false);
            Big4.SetActive(false);
        }
        if (Score >= 5 && Score <= 9.5f)
        {
            Big1.SetActive(true);
            Big2.SetActive(false);
            Big3.SetActive(false);
            Big4.SetActive(false);
        }
        if (Score >= 10 && Score <= 14.5f)
        {
            Big1.SetActive(true);
            Big2.SetActive(true);
            Big3.SetActive(false);
            Big4.SetActive(false);
        }
        if (Score >= 15 && Score <= 19.5f)
        {
            Big1.SetActive(true);
            Big2.SetActive(true);
            Big3.SetActive(true);
            Big4.SetActive(false);
        }
        if (Score >= 20)
        {
            scoreBoardNum = 0;
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
        
        Big1.SetActive(true);
            Big2.SetActive(true);
            Big3.SetActive(true);
            Big4.SetActive(true);
        }
        if (Score == 0.0f)
        {
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
        }
        if (Score == 0.5f)
        {
            scoreBoardNum = 0;

            for (int i = 1; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 1)
        {
            scoreBoardNum = 1;
            for (int i = 2; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 1.5f)
        {
            scoreBoardNum = 2;
            for (int i = 3; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 2.0f)
        {
            scoreBoardNum = 3;
            for (int i = 4; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 2.5f)
        {
            scoreBoardNum = 4;
            for (int i = 5; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 3.0f)
        {
            scoreBoardNum = 5;
            for (int i = 6; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 3.5f)
        {
            scoreBoardNum = 6;
            for (int i = 7; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 4.0f)
        {
            scoreBoardNum = 7;
            for (int i = 8; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 4.5f)
        {
            scoreBoardNum = 8;
            for (int i = 9; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 5.5f)
        {
            scoreBoardNum = 0;
            for (int i = 1; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 6.0f)
        {
            scoreBoardNum = 1;
            for (int i = 2; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 6.5f)
        {
            scoreBoardNum = 2;
            for (int i = 3; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 7.0f)
        {
            scoreBoardNum = 3;
            for (int i = 4; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 7.5f)
        {
            scoreBoardNum = 4;
            for (int i = 5; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 8.0f)
        {
            scoreBoardNum = 5;
            for (int i = 6; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 8.5f)
        {
            scoreBoardNum = 6;
            for (int i = 7; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 9.0f)
        {
            scoreBoardNum = 7;
            for (int i = 8; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 9.5f)
        {
            scoreBoardNum = 8;
            for (int i =  9; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==10.5f)
        {
            scoreBoardNum = 0;
            for (int i = 1; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==11.0f)
        {
            scoreBoardNum = 1;
            for (int i = 2; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==11.5f)
        {
            scoreBoardNum = 2;
            for (int i = 3; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==12.0f)
        {
            scoreBoardNum = 3;
            for (int i = 4; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==12.5f)
        {
            scoreBoardNum = 4;
            for (int i = 5; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==13.0f)
        {
            scoreBoardNum = 5;
            for (int i = 6; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==13.5f)
        {
            scoreBoardNum = 6;
            for (int i = 7; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==14.0f)
        {
            scoreBoardNum = 7;
            for (int i = 8; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==14.5f)
        {
            scoreBoardNum = 8;
            for (int i =  9; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score ==15.5f)
        {
            scoreBoardNum = 0;
            for (int i = 1; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 16.0f)
        {
            scoreBoardNum = 1;
            for (int i = 2; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 16.5f)
        {
            scoreBoardNum = 2;
            for (int i = 3; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 17.0f)
        {
            scoreBoardNum = 3;
            for (int i = 4; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 17.5f)
        {
            scoreBoardNum = 4;
            for (int i = 5; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 18.0f)
        {
            scoreBoardNum = 5;
            for (int i = 6; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 18.5f)
        {
            scoreBoardNum = 6;
            for (int i = 7; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 19.0f)
        {
            scoreBoardNum = 7;
            for (int i = 8; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
        if (Score == 19.5f)
        {
            scoreBoardNum = 8;
            for (int i =  9; i < scoreBoard.Length; i++)
            {
                scoreBoard[i].SetActive(false);
            }
            for (int u = 0; u <= scoreBoardNum; u++)
            {
                scoreBoard[u].SetActive(true);
            }
        }
    }
}
