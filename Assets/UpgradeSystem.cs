﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradeSystem : MonoBehaviour {

    public int damageUpgradeCost;
    public int HPUpgradeCost;
    public int UpgradeLenght;
    public int UpgradeBounce;
    public bool chargeBought;
    public bool bounceBought;
    public bool triBought;
    public Text charge;
    public Text bounce;
    public Text triple;
    public GameObject FinalBoss;
    [SerializeField]
    private GameObject UpgradeMenu;
    public static UpgradeSystem gm;
    static public bool onPause;
    public GameObject bossSpawner;
    public GameObject WaveSpawner2;
    public GameObject WaveSpawner3;
    public GameObject WaveSpawnerStandard;
    public GameObject Shield;
    public GameObject dShot;
    public GameObject tShot;
    public GameObject timer;

    void Awake()
    {
        if (gm == null)
        {
            gm = GameObject.FindGameObjectWithTag("GM").GetComponent<UpgradeSystem>();
        }
    }
    public delegate void UpgradeMenuCallback(bool active);
    public UpgradeMenuCallback onToggleUpgradeMenu;
    private void Update()
    {
        if (IsaacController.points >= 85000 && FinalBoss != null)
        {
            WaveSpawner2.SetActive(false);
            WaveSpawner3.SetActive(false);
            WaveSpawnerStandard.SetActive(false);
            FinalBoss.SetActive(true);
        }
        if (IsaacController.points >= 25000)
        {
            WaveSpawner2.SetActive(true);
        }
        if (IsaacController.points >= 50000)
        {
            WaveSpawner3.SetActive(true);
        }
        if (IsaacController.points >= 250000)
        {
            PlayerPrefs.SetInt("BOSS", 1);
            timer.GetComponent<Timer>().enabled = false;
            SteamScript.whichLogro = "Logro27";
            SteamScript.logro = "Logro27";
            bossSpawner.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleUpgradeMenu();
    }
            
    }

    private void ToggleUpgradeMenu()
    {
       onPause = !onPause;
        UpgradeMenu.SetActive(!UpgradeMenu.activeSelf);
        onToggleUpgradeMenu.Invoke(UpgradeMenu.activeSelf); 
    }
    // Update is called once per frame
    public void UpgradeDamage()
    {
        if (IsaacController.monay >= damageUpgradeCost)
        {
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= damageUpgradeCost;
            IsaacBrimstoneManager.upgradeDamage += 25f;
        }
    }
    public void UpgradeLives()
    {
        if (IsaacController.monay >= HPUpgradeCost)
        {
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= HPUpgradeCost;
            IsaacController.isaacHP += 1;
        }
    }
    public void UpgradeChargeSpeed()
    {
        if (IsaacController.monay >= 2000 && chargeBought == false)
        {
            charge.text = "SOLD OUT!";
            FindObjectOfType<AudioManager>().Play("Upgrade");
            chargeBought = true;
            SteamScript.whichLogro = "Logro26";
            SteamScript.logro = "Logro26";
            IsaacController.monay -= 2000;
            IsaacBrimstoneManager.upgradeChargeSpeed += 50f;
        }
    }
   public void UpgradeBouncing()
    {
        if (IsaacController.monay >= UpgradeBounce && !bounceBought)
        {
            bounce.text = "SOLD OUT!";
            FindObjectOfType<AudioManager>().Play("Upgrade");
            bounceBought = true;
            SteamScript.whichLogro = "Logro25";
            SteamScript.logro = "Logro25";
            IsaacController.monay -= UpgradeBounce;
            IsaacBrimstoneManager.bouncing = true;
        }
    }
    public void UpgradeShield()
    {
        if (IsaacController.monay >= 1500f)
        {
            SteamScript.whichLogro = "Logro36";
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= 1500;
            Shield.SetActive(true);
            ShieldScript.shieldSpeed += 1.5f;
        }
    }
    public void TripleShot()
    {
        if (IsaacController.monay >= 3000f && !triBought)
        {
            SteamScript.whichLogro = "Logro37";
            triple.text = "SOLD OUT!";
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= 3000;
            triBought = true;
            dShot.SetActive(true);
            tShot.SetActive(true);
        }
    }
    public void UpgradeLength()
    {
        if (IsaacController.monay >= UpgradeLenght)
        {
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= UpgradeLenght;
            IsaacBrimstoneManager.increaseLength = true;
        }
    }
    public void UpgradeSpeed()
    {
        if (IsaacController.monay >= UpgradeLenght)
        {
            FindObjectOfType<AudioManager>().Play("Upgrade");
            IsaacController.monay -= UpgradeLenght;
            IsaacBrimstoneManager.increaseSpeed = true;
        }
    }
}
