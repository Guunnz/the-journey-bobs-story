﻿using UnityEngine;
using System.Collections;

public class AttackManagerOnPause : MonoBehaviour {
    public Transform Attack;

    public bool ifBrim;
    public bool ifNorm;
    private int timeX = 1;
    public float timer;
    void Start()
    {
        gameObject.GetComponent<AudioSource>().Play();
        UpgradeSystem.gm.onToggleUpgradeMenu += OnUpgradeMenuToggle;
    }
    void Awake()
    {
        Attack = this.gameObject.transform.Find("MonsterTears");
    }
    void OnUpgradeMenuToggle(bool active)
    {
        if (active && Attack != null)
        {
            timeX = 0;
            Attack.GetComponent<ParticleSystem>().playbackSpeed = 0;
            GetComponent<MonsterBehaviour>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
        }
        if (!active && Attack != null)
        {
            timeX = 1;
            Attack.GetComponent<ParticleSystem>().playbackSpeed = 1;
            GetComponent<MonsterBehaviour>().enabled = true;
            GetComponent<AudioSource>().enabled = true;
        }
        if (active && Attack == null)
        {
            return;
        }
        if (!active && Attack == null)
        {
            return;
        }
    }
    private void Update()
    {
        timer += timeX * Time.deltaTime;
        if (ifBrim && timer >= 6)
        {
            timer = 0;
            gameObject.GetComponent<AudioSource>().Play();
        }
        if (ifNorm && timer >= 1)
        {
            timer = 0;
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
}