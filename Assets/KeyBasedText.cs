﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KeyBasedText : MonoBehaviour
{

    public string firstLine;
    public string secondLine;
    public Text text;
    public bool Interact;
    public bool Jetpack;
    public bool Gauntlet;
    private void Awake()
    {
       text = gameObject.GetComponent<Text>();
    }


    // Use this for initialization
    void Update()
    {
        text = gameObject.GetComponent<Text>();
        if (Interact)
        {
            text.text = firstLine + " " + "<" + InputManager.E.ToString() + ">" + " " + secondLine;
        }
        if (Jetpack)
        {
            text.text = firstLine + " " + "<" + InputManager.LShift.ToString() + ">" + " " + secondLine;
        }
        if (Gauntlet)
        {
            text.text = firstLine + " " + "<" + InputManager.LControl.ToString() + ">" + " " + secondLine;
        }
    }
}
