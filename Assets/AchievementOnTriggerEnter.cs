﻿using UnityEngine;
using System.Collections;

public class AchievementOnTriggerEnter : MonoBehaviour {

    public string achievement;



    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            SteamScript.whichLogro = achievement;
            SteamScript.logro = achievement;
        }
    }
}
