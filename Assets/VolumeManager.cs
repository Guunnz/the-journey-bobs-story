﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour {
public Slider Volume;

    private void Start()
    {
        Volume.value = 1;
        AudioListener.volume = Volume.value;
    }
    // Update is called once per frame
    void Update ()
    {
        AudioListener.volume = Volume.value;
        PlayerPrefs.SetFloat("audio", AudioListener.volume);
        Volume.value = PlayerPrefs.GetFloat("audio");
    }
}
