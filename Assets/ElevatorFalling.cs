﻿using UnityEngine;
using System.Collections;

public class ElevatorFalling : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        FindObjectOfType<AudioManager>().Play("Elevator");
        Invoke("PlayAudio", 1.37f);
	}
	
	void PlayAudio()
    {
        Invoke("Sparks", 1.5f);
        GetComponent<AudioSource>().Play();
    }
    void Sparks()
    {
        FindObjectOfType<AudioManager>().Stop("Elevator");
        FindObjectOfType<AudioManager>().Play("Sparks");
    }
}
