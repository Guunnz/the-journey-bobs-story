﻿using UnityEngine;
using System.Collections;

public class StopAudio : MonoBehaviour {

    public string AudioToStop;

    // Update is called once per frame
    void Update ()
    {
        FindObjectOfType<AudioManager>().Stop(AudioToStop);
    }
}
