﻿using UnityEngine;
using System.Collections;

public class LightsDownTrigger : MonoBehaviour {


    public GameObject lightsDownTriggerObject;

	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D info)
    {
        if (info.tag == "Player" && lightsDownTriggerObject != null)
        {
            lightsDownTriggerObject.SetActive(true);
        }
	}
}
