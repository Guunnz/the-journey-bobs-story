﻿using UnityEngine;
using System.Collections;

public class RockSoundManager : MonoBehaviour


{

    public void Start()
    {
        if (JGCheckpoint.RockOn != 1)
        {
            FindObjectOfType<AudioManager>().Play("RockRolling");
            Invoke("StopRock", 1.4f);
        }
    }
    public void StopRock()
    {
        FindObjectOfType<AudioManager>().Play("RockCrashing");
        FindObjectOfType<AudioManager>().Stop("RockRolling");
    }
}
