﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndingNumber : MonoBehaviour {

    public Text Code;
    public int onDigit;
    public int Text1;
    public int Text2;
    public int Text3;
    public int Text4;
    public int Text5;
    public int Text6;
    public bool called;
    public bool rocketLaunched;
    public float uncall = 0.2f;

    // Update is called once per frame

    private void Start()
    {
        PlayerPrefs.SetString("Ending", "Ending1");
    }
    void Update()
    {
        Code.text = Text1.ToString() + Text2.ToString() + Text3.ToString() + Text4.ToString() + Text5.ToString() + Text6.ToString();
    }

    void Uncall()
    {
        called = false;
    }

    public void Press0()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 0;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 0;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 0;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 0;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 0;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 0;
            Invoke("Uncall", uncall);
        }
    }
    public void Press1()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 1;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 1;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 1;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 1;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 1;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 1;
            Invoke("Uncall", uncall);
        }
    }
    public void Press2()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 2;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 2;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 2;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 2;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 2;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 2;
            Invoke("Uncall", uncall);
        }
    }
    public void Press3()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 3;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 3;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 3;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 3;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 3;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 3;
            Invoke("Uncall", uncall);
        }
    }
    public void Press4()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 4;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 4;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 4;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 4;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 4;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 4;
            Invoke("Uncall", uncall);
        }
    }
    public void Press5()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 5;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 5;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 5;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 5;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 5;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 5;
            Invoke("Uncall", uncall);
        }
    }
    public void Press6()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 6;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 6;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 6;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 6;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 6;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 6;
            Invoke("Uncall", uncall);
        }
    }
    public void Press7()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 7;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 7;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 7;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 7;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 7;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 7;
            Invoke("Uncall", uncall);
        }
    }
    public void Press8()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 8;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 8;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 8;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 8;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 8;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 8;
            Invoke("Uncall", uncall);
        }
    }
    public void Press9()
    {
        FindObjectOfType<AudioManager>().Play("TypeCode");
        if (onDigit == 0 && !called)
        {
            called = true;
            onDigit = 1;
            Text1 = 9;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 1 && !called)
        {
            called = true;
            onDigit = 2;
            Text2 = 9;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 2 && !called)
        {
            called = true;
            onDigit = 3;
            Text3 = 9;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 3 && !called)
        {
            called = true;
            onDigit = 4;
            Text4 = 9;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 4 && !called)
        {
            called = true;
            onDigit = 5;
            Text5 = 9;
            Invoke("Uncall", uncall);
        }
        if (onDigit == 5 && !called)
        {
            called = true;
            onDigit = 6;
            Text6 = 9;
            Invoke("Uncall", uncall);
        }
    } 
    public void Clear()
    {
        FindObjectOfType<AudioManager>().Play("CodeFail");
        called = false;
        onDigit = 0;
        Text1 = 0;
        Text2 = 0;
        Text3 = 0;
        Text4 = 0;
        Text5 = 0;
        Text6 = 0;
    }
    public void Submit()
    {
        if (Text1 == 1 && Text2 == 2 && Text3 == 0 && Text4 == 3 && Text5 == 2 && Text6 == 8 && !rocketLaunched)
        {
            FindObjectOfType<AudioManager>().Play("CodeCorrect");
            rocketLaunched = true;
            PlayerPrefs.SetString("Ending", "Ending3");
            SteamScript.whichLogro = "Logro28";
            SteamScript.logro = "Logro28";
            Debug.Log("Rocket launched");
        }
        if (Text1 == 1 && Text2 == 2 && Text3 == 0 && Text4 == 3 && Text5 == 2 && Text6 == 5 && rocketLaunched)
        {
            FindObjectOfType<AudioManager>().Play("CodeCorrect");
            PlayerPrefs.SetString("Ending", "Ending2");
            SteamScript.whichLogro = "Logro29";
            SteamScript.logro = "Logro29";
            Debug.Log("Rocket Changed Direction");
        }
        if (Text1 != 1 && Text2 != 2 && Text3 != 0 && Text4 != 3 && Text5 != 2 && Text6 != 8 && !rocketLaunched)
        {
            Clear();
        }
        if (Text1 != 1 && Text2 != 2 && Text3 != 0 && Text4 != 3 && Text5 != 2 && Text6 != 5 && rocketLaunched)
        {
            Clear();
        }
    }
}









