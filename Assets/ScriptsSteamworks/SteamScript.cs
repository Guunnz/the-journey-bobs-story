﻿using UnityEngine;
using System.Collections;
using Steamworks;

public class SteamScript : MonoBehaviour
{
    static public string logro = "";
    static public string whichLogro = "standard";
    public static SteamScript instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    void Start()
    {
        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            
        }
    }


    private void OnGUI()
    {
        if (Event.current.type == EventType.keyUp && Event.current.keyCode == KeyCode.SysReq)
        {
            whichLogro = "Logro35";
            logro = "Logro35";
        }
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.F12))
        {
            whichLogro = "Logro35";
            logro = "Logro35";
        }
        if (SteamManager.Initialized && logro == whichLogro)
        {
            SteamUserStats.SetAchievement(logro);
            SteamUserStats.StoreStats();   
            Invoke("ResetLogro", 0.5f);
        }
    }

    void ResetLogro()
    {
        whichLogro = "standard";
        logro = "";
    }
}
