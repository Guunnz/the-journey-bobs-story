﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameOverManager : MonoBehaviour {


    public Text scoreGotten;
    public Text highScore;
    public Text timeGotten;
    public Text survived;
    public int score;
    public int hScore;
    public int boss;

    void Awake()
    {
        IsaacController.isaacHP = 3;
        hScore = PlayerPrefs.GetInt("HighScore");
        score = PlayerPrefs.GetInt("Points");
        boss = PlayerPrefs.GetInt("BOSS");
    }

    void Update()
    {
        scoreGotten.text = "Score: " + score.ToString();
        highScore.text = "High Score: " + hScore.ToString();
        if (boss != 1)
        {
            float t = PlayerPrefs.GetFloat("Time");
            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f2");
            timeGotten.text = minutes + ":" + seconds;
            survived.text = "You survived for:";
        }
        if (boss == 1)
        {
            PlayerPrefs.SetInt("BOSS", 0);
            float t = PlayerPrefs.GetFloat("Time");
            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f2");
            timeGotten.text = minutes + ":" + seconds;
            survived.text = " Boss kill time:";
        }
    }



    public void MainMenu()
    {
        SceneManager.LoadScene("IsaacInfiniteMenu", LoadSceneMode.Single);
    }
    public void ResetInfinite()
    {
        SceneManager.LoadScene("IsaacInfinite", LoadSceneMode.Single);
    }

}
