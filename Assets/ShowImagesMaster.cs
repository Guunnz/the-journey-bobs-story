﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ShowImagesMaster : MonoBehaviour {

	
    public GameObject draw1;
    public GameObject draw2;
    public GameObject draw3;
    public GameObject draw4;
    public GameObject draw5;
    public GameObject draw6;
    public GameObject cuaderno;
    public GameObject cuaderno2;
    public GameObject cuaderno3;
    public GameObject cuaderno4;
    public GameObject cuaderno5;
    public GameObject scientist1;
    public GameObject scientist2;
    public GameObject scientist3;
    public GameObject Option;
    public GameObject Resume;
    public GameObject Fade;
    public AudioSource AS;

    private void Start()
    {
        Fade.SetActive(false);
    }

    public void Draw1()
    {
        AS.Play();
        draw1.SetActive(!draw1.activeSelf);
    }
    public void Draw2()
    {
        AS.Play();
        draw2.SetActive(!draw2.activeSelf);
    }
    public void Draw3()
    {
        AS.Play();
        draw3.SetActive(!draw3.activeSelf);
    }
    public void Draw4()
    {
        AS.Play();
        draw4.SetActive(!draw4.activeSelf);
    }
    public void Draw5()
    {
        AS.Play();
        draw5.SetActive(!draw5.activeSelf);
    }
    public void Draw6()
    {
        AS.Play();
        draw6.SetActive(!draw6.activeSelf);
    }
    public void Cuaderno()
    {
        AS.Play();
        cuaderno.SetActive(false);
        cuaderno2.SetActive(false);
        cuaderno3.SetActive(false);
        cuaderno4.SetActive(false);
        cuaderno5.SetActive(false);
    }
    public void Page1()
    {
        AS.Play();
        cuaderno.SetActive(true);
        cuaderno2.SetActive(false);
    }
    public void Page2()
    {
        AS.Play();
        cuaderno.SetActive(false);
        cuaderno2.SetActive(true);
        cuaderno3.SetActive(false);
    }
    public void Page3()
    {
        AS.Play();
        cuaderno2.SetActive(false);
        cuaderno3.SetActive(true);
        cuaderno4.SetActive(false);
    }
    public void Page4()
    {
        AS.Play();
        cuaderno3.SetActive(false);
        cuaderno4.SetActive(true);
        cuaderno5.SetActive(false);
    }
    public void Page5()
    {
        AS.Play();
        cuaderno4.SetActive(false);
        cuaderno5.SetActive(true);
    }
    public void Scientist1()
    {
        AS.Play();
        scientist1.SetActive(!scientist1.activeSelf);
    }
    public void Scientist2()
    {
        AS.Play();
        scientist2.SetActive(!scientist2.activeSelf);
    }
    public void Scientist3()
    {
        AS.Play();
        scientist3.SetActive(!scientist3.activeSelf);
    }
    public void Quit()
    {
        AS.Play();
        Application.Quit();
    }
    public void Options()
    {    AS.Play();
        Option.SetActive(!Option.activeSelf);
    }
    public void ResumeGame()
    {
        AS.Play();
        Resume.SetActive(!Resume.activeSelf);
        OpenPause.pause = false;
        Option.SetActive(false);
        scientist3.SetActive(false);
        draw1.SetActive(false);
        draw2.SetActive(false);
        draw3.SetActive(false);
        draw4.SetActive(false);
        draw5.SetActive(false);
        draw6.SetActive(false);
        cuaderno.SetActive(false);
        scientist1.SetActive(false);
        scientist2.SetActive(false);
        scientist3.SetActive(false);

}
    public void MainMenu()
    {
        AS.Play();
        Fade.SetActive(true);
        Time.timeScale = 1;
        OpenPause.squarethon = true;
        Invoke("ToMainMenu", 0.5f);
    }

    public void ToMainMenu()
    {
        OpenPause.pause = false;
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);

    }
}
