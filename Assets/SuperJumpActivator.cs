﻿using UnityEngine;
using System.Collections;

public class SuperJumpActivator : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SuperJump.EnableCollider = true;
        }
    }
}
