﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {



    public bool elevatorGoingUp;
    public GameObject Player;
    public GameObject rwallcheck;
    public GameObject collider;
    public GameObject slope;
    public bool onBox;
    public bool boxPickedUp;
    public float boxX;
    public float boxXNeg;


    private void Update()
    {
        if (Input.GetKeyDown(InputManager.E) && onBox && !boxPickedUp)
        {
            Platformer2DJG.onBox = true;
            slope.SetActive(true);
            collider.SetActive(false);
            rwallcheck.transform.parent = gameObject.transform;
            gameObject.transform.parent = Player.transform;
            Destroy(GetComponent<Rigidbody2D>());
            gameObject.transform.localPosition = new Vector3(2.2f, 0, 0);
            rwallcheck.transform.localPosition = new Vector3(1.61f, -0.22f, 0);
            boxPickedUp = true;
            transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
        }

        if (boxPickedUp && onBox)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (Input.GetAxisRaw("Vertical") < 0 && boxPickedUp && Player.transform.localScale.x == 2)
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.22f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 8;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        if (Input.GetAxisRaw("Vertical") < 0 && boxPickedUp && Player.transform.localScale.x == -2)
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.75f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 8;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        if (Input.GetAxisRaw("Vertical") > 0 && boxPickedUp && Player.transform.localScale.x == -2)
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.75f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().velocity = new Vector2(-50, 50);
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 14;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        if (Input.GetAxisRaw("Vertical") > 0 && boxPickedUp && Player.transform.localScale.x == 2)
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.75f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().velocity = new Vector2(50, 50);
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 14;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
    }

    void OnTriggerEnter2D(Collider2D info)

    {
        if (info.tag == "FrontCollider")
        {
            onBox = true;
        }
        if (info.tag == "HardDeath")
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.22f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 8;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
        if (info.tag == "EndLevel")
        {
            Platformer2DJG.onBox = false;
            slope.SetActive(false);
            collider.SetActive(true);
            onBox = false;
            rwallcheck.transform.parent = Player.transform;
            rwallcheck.transform.localPosition = new Vector3(1.1f, -0.22f, 0);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 8;
            transform.parent = null;
            boxPickedUp = false;
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }
    }
    void OnTriggerExit2D(Collider2D info)

    {
        if (info.tag == "FrontCollider")
        {
            onBox = false;
        }
    }
}