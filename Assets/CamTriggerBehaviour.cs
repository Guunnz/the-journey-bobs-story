﻿using UnityEngine;
using System.Collections;

public class CamTriggerBehaviour : MonoBehaviour
{
    private Transform luz;
    private Transform ps;

    private void Awake()
    {
        ps = transform.Find("LightFocus");
        luz = transform.Find("Light");
    }

    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            ps.GetComponent<ParticleSystem>().startColor = new Color(255, 0, 0, 0.05f);
            ps.gameObject.SetActive(false);
            Invoke("Red", 0.01f);
            GetComponent<CameraMovement>().enabled = true;
            luz.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    void Red()
    {
        ps.gameObject.SetActive(true);
    }
    void White()
    {
        ps.gameObject.SetActive(true);
    }
    private void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            ps.GetComponent<ParticleSystem>().startColor = new Color(255, 255, 255, 0.05f);
            ps.gameObject.SetActive(false);
            Invoke("White", 0.01f);
            GetComponent<CameraMovement>().enabled = false;
         luz.GetComponent<SpriteRenderer>().color = Color.green;
            ps.gameObject.SetActive(true);
           
        }
    }












}
