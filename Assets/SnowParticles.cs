﻿using UnityEngine;
using System.Collections;

public class SnowParticles : MonoBehaviour {
    public ParticleSystem ps;

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Platform")
        {
            ps.enableEmission = true;
        }
    }
    void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Platform")
        {
            ps.enableEmission = false;
        }
    }
}
