﻿using UnityEngine;
using System.Collections;

public class SoundPuzzle : MonoBehaviour
{
    public string audio1;
    public string audio2;
    public string audio3;
    public string audio4;
    public string audio5;
    public bool collided;
    public bool a1;
    public bool a2;
    public bool a3;
    public bool a4;
    public bool a5;
    public bool called;
    public bool real;
    public GameObject[] MusicBoard;
    public GameObject DoorDown;
    public GameObject tablero;
    public GameObject DoorUp;
    public GameObject camera1;
    public GameObject camera2;
    public bool wincalled;
    public bool resetcalled;

    // Update is called once per frame
    void Update()
    {
        if (a1 && !a2 && !a3 && !a4 && !a5 && !called)
        {
            called = true;
            PlayAudio1();
            Invoke("Loop", 2);
        }
        if (a1 && a2 && !a3 && !a4 && !a5 && !called)
        {
            called = true;
            PlayAudio1();
            Invoke("PlayAudio2", 2);
            Invoke("Loop", 4);
        }
        if (a1 && a2 && a3 && !a4 && !a5 && !called)
        {
            called = true;
            PlayAudio1();
            Invoke("PlayAudio2", 2);
            Invoke("PlayAudio3", 4);
            Invoke("Loop", 6);
        }
        if (a1 && a2 && a3 && a4 && !a5 && !called)
        {
            called = true;
            PlayAudio1();
            Invoke("PlayAudio2", 2);
            Invoke("PlayAudio3", 4);
            Invoke("PlayAudio4", 6);
            Invoke("Loop", 8);
        }
        if (a1 && a2 && a3 && a4 && a5 && !called)
        {
            called = true;
            PlayAudio1();
            Invoke("PlayAudio2", 2);
            Invoke("PlayAudio3", 4);
            Invoke("PlayAudio4", 6);
            Invoke("PlayAudio5", 8);
            Invoke("Loop", 10);
        }
        if (audio1 == "Blue" && audio2 == "Green" && audio3 == "Red" && audio4 == "Pink" && audio5 == "Yellow" && real && DoorDown != null && DoorUp != null && !wincalled)
        {
            wincalled = true;
            camera1.SetActive(false);
            camera2.SetActive(true);
            FindObjectOfType<AudioManager>().Play("Success");
            tablero.GetComponent<SpriteRenderer>().color = Color.green;
            DoorDown.SetActive(false);
            DoorUp.SetActive(true);
            Invoke("aftercam", 2f);
        }
    }

    void aftercam()
    {
        camera1.SetActive(true);
        camera2.SetActive(false);
    }

    void Loop()
    {
        called = false;
    }
    void PlayAudio1()
    {
        FindObjectOfType<AudioManager>().Play(audio1);
        if (audio1 == "Yellow")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
        if (audio1 == "Green")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(false);
        }
        if (audio1 == "Red")
        {
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[10].SetActive(false);
            MusicBoard[13].SetActive(false);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
        }
        if (audio1 == "Blue")
        {
            MusicBoard[1].SetActive(false);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(false);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(true);
           
        }
        if (audio1 == "Pink")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(true);
            MusicBoard[7].SetActive(false);
            MusicBoard[4].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
    }
    void PlayAudio2()
    {
        FindObjectOfType<AudioManager>().Play(audio2);
        if (audio2 == "Yellow")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(false);
        }
        if (audio2 == "Green")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(false);
        }
        if (audio2 == "Red")
        {
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[10].SetActive(false);
            MusicBoard[13].SetActive(false);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
        }
        if (audio2 == "Blue")
        {
            MusicBoard[1].SetActive(false);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(false);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(true);

        }
        if (audio2 == "Pink")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(true);
            MusicBoard[7].SetActive(false);
            MusicBoard[4].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
    }
    void PlayAudio3()
    {
        FindObjectOfType<AudioManager>().Play(audio3);
        if (audio3 == "Yellow")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(false);
        }
        if (audio3 == "Green")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(false);
        }
        if (audio3 == "Red")
        {
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[10].SetActive(false);
            MusicBoard[13].SetActive(false);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
        }
        if (audio3 == "Blue")
        {
            MusicBoard[1].SetActive(false);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(false);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(true);

        }
        if (audio3 == "Pink")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(true);
            MusicBoard[7].SetActive(false);
            MusicBoard[4].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
    }
    void PlayAudio4()
    {
        FindObjectOfType<AudioManager>().Play(audio4);
        if (audio4 == "Yellow")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(false);
        }
        if (audio4 == "Green")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(false);
        }
        if (audio4 == "Red")
        {
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[10].SetActive(false);
            MusicBoard[13].SetActive(false);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
        }
        if (audio4 == "Blue")
        {
            MusicBoard[1].SetActive(false);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(false);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(true);

        }
        if (audio4 == "Pink")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(true);
            MusicBoard[7].SetActive(false);
            MusicBoard[4].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
    }
    void PlayAudio5()
    {
        FindObjectOfType<AudioManager>().Play(audio5);
        if (audio5 == "Yellow")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(false);
        }
        if (audio5 == "Green")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(true);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(false);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(false);
        }
        if (audio5 == "Red")
        {
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(true);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[10].SetActive(false);
            MusicBoard[13].SetActive(false);
            MusicBoard[8].SetActive(true);
            MusicBoard[9].SetActive(true);
        }
        if (audio5 == "Blue")
        {
            MusicBoard[1].SetActive(false);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[4].SetActive(false);
            MusicBoard[5].SetActive(false);
            MusicBoard[6].SetActive(false);
            MusicBoard[7].SetActive(false);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(false);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(true);
            MusicBoard[12].SetActive(true);
            MusicBoard[13].SetActive(true);

        }
        if (audio5 == "Pink")
        {
            MusicBoard[1].SetActive(true);
            MusicBoard[2].SetActive(false);
            MusicBoard[3].SetActive(false);
            MusicBoard[5].SetActive(true);
            MusicBoard[6].SetActive(true);
            MusicBoard[7].SetActive(false);
            MusicBoard[4].SetActive(true);
            MusicBoard[8].SetActive(false);
            MusicBoard[9].SetActive(true);
            MusicBoard[10].SetActive(true);
            MusicBoard[11].SetActive(false);
            MusicBoard[12].SetActive(false);
            MusicBoard[13].SetActive(true);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        collided = true;
        if (col.gameObject.tag == "Yellow" && !a1 && collided)
        {
            collided = false;
            a1 = true;
            audio1 = "Yellow";
        }
        if (col.gameObject.tag == "Yellow" && a1 && !a2 && collided)
        {
            collided = false;
            a2 = true;
            audio2 = "Yellow";
        }
        if (col.gameObject.tag == "Yellow" && a1 && a2 && !a3 && collided)
        {
            collided = false;
            a3 = true;
            audio3 = "Yellow";
        }
        if (col.gameObject.tag == "Yellow" && a1 && a2 && a3 && !a4 && collided)
        {
            collided = false;
            a4 = true;
            audio4 = "Yellow";
        }
        if (col.gameObject.tag == "Yellow" && a1 && a2 && a3 && a4 && !a5 && collided)
        {
            collided = false;
            a5 = true;
            audio5 = "Yellow";
        }
        if (col.gameObject.tag == "Green" && !a1 && collided)
        {
            collided = false;
            a1 = true;
            audio1 = "Green";
        }
        if (col.gameObject.tag == "Green" && a1 && !a2 && collided)
        {
            collided = false;
            a2 = true;
            audio2 = "Green";
        }
        if (col.gameObject.tag == "Green" && a1 && a2 && !a3 && collided)
        {
            collided = false;
            a3 = true;
            audio3 = "Green";
        }
        if (col.gameObject.tag == "Green" && a1 && a2 && a3 && !a4 && collided)
        {
            collided = false;
            a4 = true;
            audio4 = "Green";
        }
        if (col.gameObject.tag == "Green" && a1 && a2 && a3 && a4 && !a5 && collided)
        {
            collided = false;
            a5 = true;
            audio5 = "Green";
        }
        if (col.gameObject.tag == "Red" && !a1 && collided)
        {
            collided = false;
            a1 = true;
            audio1 = "Red";
        }
        if (col.gameObject.tag == "Red" && a1 && !a2 && collided)
        {
            collided = false;
            a2 = true;
            audio2 = "Red";
        }
        if (col.gameObject.tag == "Red" && a1 && a2 && !a3 && collided)
        {
            collided = false;
            a3 = true;
            audio3 = "Red";
        }
        if (col.gameObject.tag == "Red" && a1 && a2 && a3 && !a4 && collided)
        {
            collided = false;
            a4 = true;
            audio4 = "Red";
        }
        if (col.gameObject.tag == "Red" && a1 && a2 && a3 && a4 && !a5 && collided)
        {
            collided = false;
            a5 = true;
            audio5 = "Red";
        }
        if (col.gameObject.tag == "Blue" && !a1 && collided)
        {
            collided = false;
            a1 = true;
            audio1 = "Blue";
        }
        if (col.gameObject.tag == "Blue" && a1 && !a2 && collided)
        {
            collided = false;
            a2 = true;
            audio2 = "Blue";
        }
        if (col.gameObject.tag == "Blue" && a1 && a2 && !a3 && collided)
        {
            collided = false;
            a3 = true;
            audio3 = "Blue";
        }
        if (col.gameObject.tag == "Blue" && a1 && a2 && a3 && !a4 && collided)
        {
            collided = false;
            a4 = true;
            audio4 = "Blue";
        }
        if (col.gameObject.tag == "Blue" && a1 && a2 && a3 && a4 && !a5 && collided)
        {
            collided = false;
            a5 = true;
            audio5 = "Blue";
        }
        if (col.gameObject.tag == "Pink" && !a1 && collided)
        {
            collided = false;
            a1 = true;
            audio1 = "Pink";
        }
        if (col.gameObject.tag == "Pink" && a1 && !a2 && collided)
        {
            collided = false;
            a2 = true;
            audio2 = "Pink";
        }
        if (col.gameObject.tag == "Pink" && a1 && a2 && !a3 && collided)
        {
            collided = false;
            a3 = true;
            audio3 = "Pink";
        }
        if (col.gameObject.tag == "Pink" && a1 && a2 && a3 && !a4 && collided)
        {
            collided = false;
            a4 = true;
            audio4 = "Pink";
        }
        if (col.gameObject.tag == "Pink" && a1 && a2 && a3 && a4 && !a5 && collided)
        {
            collided = false;
            a5 = true;
            audio5 = "Pink";
        }
        if (col.gameObject.tag == "Black" && collided && !resetcalled)
        {
            resetcalled = true;
            tablero.GetComponent<SpriteRenderer>().color = Color.red;
            Invoke("aftercam", 2f);
            Invoke("decolor", 2f);
            camera1.SetActive(false);
            camera2.SetActive(true);
            FindObjectOfType<AudioManager>().Play("Error");
            collided = false;
            a1 = false;
            a2 = false;
            a3 = false;
            a4 = false;
            a5 = false;
            audio1 = "";
            audio2 = "";
            audio3 = "";
            audio4 = "";
            audio5 = "";
        }
    }
    void decolor()
    {
        resetcalled = false;
        tablero.GetComponent<SpriteRenderer>().color = Color.white;
    }
}