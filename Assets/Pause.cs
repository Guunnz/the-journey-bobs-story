﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

    public GameObject draw1;
    public GameObject draw2;
    public GameObject draw3;
    public GameObject draw4;
    public GameObject draw5;
    public GameObject draw6;
    public GameObject cuaderno;
    public GameObject scientist1;
    public GameObject scientist2;
    public GameObject scientist3;
    static public int Idraw1;
    static public int Idraw2;
    static public int Idraw3;
    static public int Idraw4;
    static public int Idraw5;
    static public int Idraw6;
    static public int Icuaderno;
    static public int Iscientist1;
    static public int Iscientist2;
    static public int Iscientist3;


    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        Idraw1 = PlayerPrefs.GetInt("Idraw1");
        Idraw2 = PlayerPrefs.GetInt("Idraw2");
        Idraw3 = PlayerPrefs.GetInt("Idraw3");
        Idraw4 = PlayerPrefs.GetInt("Idraw4");
        Idraw5 = PlayerPrefs.GetInt("Idraw5");
        Idraw6 = PlayerPrefs.GetInt("Idraw6");
        Icuaderno = PlayerPrefs.GetInt("Icuaderno");
        Iscientist1 = PlayerPrefs.GetInt("Iscientist1");
        Iscientist2 = PlayerPrefs.GetInt("Iscientist2");
        Iscientist3 = PlayerPrefs.GetInt("Iscientist3");
    }

    // Update is called once per frame
    void Update ()
    {
        if (Idraw1 == 1)
        {
            PlayerPrefs.SetInt("Idraw1", Idraw1);
            draw1.SetActive(true);
        }
        if (Idraw2 == 1)
        {
            PlayerPrefs.SetInt("Idraw2", Idraw2);
            draw2.SetActive(true);
        }
        if (Idraw3 == 1)
        {
            PlayerPrefs.SetInt("Idraw3", Idraw3);
            draw3.SetActive(true);
        }
        if (Idraw4 == 1)
        {
            PlayerPrefs.SetInt("Idraw4", Idraw4);
            draw4.SetActive(true);
        }
        if (Idraw5 == 1)
        {
            PlayerPrefs.SetInt("Idraw5", Idraw5);
            draw5.SetActive(true);
        }
        if (Idraw6 == 1)
        {
            PlayerPrefs.SetInt("Idraw6", Idraw6);
            draw6.SetActive(true);
        }
        if (Icuaderno == 1)
        {
            PlayerPrefs.SetInt("Icuaderno", Icuaderno);
            cuaderno.SetActive(true);
        }
        if (Iscientist1 == 1)
        {
            PlayerPrefs.SetInt("Iscientist1", Iscientist1);
            scientist1.SetActive(true);
        }
        if (Iscientist2 == 1)
        {
            PlayerPrefs.SetInt("Iscientist1", Iscientist2);
            scientist2.SetActive(true);
        }
        if (Iscientist3 == 1)
        {
            PlayerPrefs.SetInt("Iscientist1", Iscientist3);
            scientist3.SetActive(true);
        }
    }
}
