﻿using UnityEngine;
using System.Collections;

public class OpenPause : MonoBehaviour {

    public GameObject PauseMaster;
    static public bool pause;
    static public bool squarethon = true;
    public static OpenPause instance;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (pause && !squarethon)
        {
            Time.timeScale = 0;
        }
        if (!pause && !squarethon)
        {
            Time.timeScale = 1;
            PauseMaster.SetActive(false);
        }
        if (!pause)
        {
            Time.timeScale = 1;
            PauseMaster.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Escape) && !squarethon)
        {
            pause = !pause;
            PauseMaster.SetActive(!PauseMaster.activeSelf);
        }
    }
}
