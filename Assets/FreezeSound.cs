﻿using UnityEngine;
using System.Collections;

public class FreezeSound : MonoBehaviour {

    public bool played;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CascadeTrigger" && !played)
            {
            played = true;
            FindObjectOfType<AudioManager>().Play("WaterFreeze");
            Invoke("playedfalse", 1f);
        }
    }

    void playedfalse()
    {
        played = false;
    }
}
