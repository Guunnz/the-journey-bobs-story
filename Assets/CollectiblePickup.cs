﻿using UnityEngine;
using System.Collections;

public class CollectiblePickup : MonoBehaviour {




    public bool ItemPickedUp;
    public bool draw1;
    public bool draw2;
    public bool draw3;
    public bool draw4;
    public bool draw5;
    public bool draw6;
    public bool cuaderno;
    public bool scientist1;
    public bool scientist2;
    public bool scientist3;


    void Update()
    {
        if (ItemPickedUp)
        {
            if (draw1)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro5";
                SteamScript.logro = "Logro5";
                Pause.Idraw1 = 1;
            }
            if (draw2)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro4";
                SteamScript.logro = "Logro4";
                Pause.Idraw2 = 1;
            }
            if (draw3)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro13";
                SteamScript.logro = "Logro13";
                Pause.Idraw3 = 1;
            }
            if (draw4)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro6";
                SteamScript.logro = "Logro6";
                Pause.Idraw4 = 1;
            }
            if (draw5)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro14";
                SteamScript.logro = "Logro14";
                Pause.Idraw5 = 1;
            }
            if (draw6)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro15";
                SteamScript.logro = "Logro15";
                Pause.Idraw6 = 1;
            }
            if (cuaderno)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro34";
                SteamScript.logro = "Logro34";
                Pause.Icuaderno = 1;
            }
            if (scientist1)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro16";
                SteamScript.logro = "Logro16";
                Pause.Iscientist1 = 1;
            }
            if (scientist2)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro17";
                SteamScript.logro = "Logro17";
                Pause.Iscientist2 = 1;
            }
            if (scientist3)
            {
                FindObjectOfType<AudioManager>().VolumeIn("Collectible");
                SteamScript.whichLogro = "Logro18";
                SteamScript.logro = "Logro18";
                Pause.Iscientist3 = 1;
            }
        }
    }

    void Destroy()
    {
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D info)
    {
        if (info.tag == "Player")
        {
            ItemPickedUp = true;
            Invoke("Destroy", 0.1f);
        }
	}
}
