﻿using UnityEngine;
using System.Collections;

public class AchievementBird : MonoBehaviour {

    public GameObject iceCube;
    public Animator anim;
    public bool flying;

    private void Update()
    {
        if(flying)
        {
            transform.position = new Vector2(transform.position.x - 0.5f, transform.position.y + 0.75f);
        }
    }




    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Gauntlet")
        {
            flying = true;
            anim.SetBool("DeFreezed", true);
            iceCube.SetActive(false);
            SteamScript.whichLogro = "Logro33";
            SteamScript.logro = "Logro33";
        }
    }

}

