﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class WaveSpawner : MonoBehaviour
{

    public enum SpawnState { SPAWNING, WAITING, COUNTING };


    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform enemy;
        public Transform enemy2;
        public Transform enemy3;
        public int count3;
        public int count2;
        public int count;
        public float rate;
        public float rate2;
        public float rate3;


    }
    public bool startCalled;
    public Wave[] waves;
   static public int nextWave = 0;
    public int currentWave = 0;
    public Transform[] spawnPoints;

    public float timeBetweenWaves = 5f;
    private float waveCountdown;

    public Text waveCountdownText;
    private float searchCountdown;

    private SpawnState state = SpawnState.COUNTING;
    void Start()
    {
        if (!startCalled)
        {
            startCalled = true;
            waveCountdown = timeBetweenWaves;
           
        }
    }

    void Update()
    {
        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                waveCountdownText.text = "Wave " + currentWave.ToString();
                StartCoroutine(SpawnWave2(waves[nextWave]));
                StartCoroutine(SpawnWave3(waves[nextWave]));
                StartCoroutine( SpawnWave (  waves[nextWave] ) );
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
            waveCountdownText.text = Mathf.Round(waveCountdown).ToString();
        }

    }



    IEnumerator SpawnWave(Wave _wave)
    {
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f * _wave.rate);
        }


        state = SpawnState.WAITING;

        yield break;
    }
    IEnumerator SpawnWave2(Wave _wave)
    {
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count2; i++)
        { 
            yield return new WaitForSeconds(1f * _wave.rate2);
              SpawnEnemy2(_wave.enemy2);
            yield return new WaitForSeconds(1f * _wave.rate2);
        }


        state = SpawnState.WAITING;

        yield break;
    }
    IEnumerator SpawnWave3(Wave _wave)
    {
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count3; i++)
        {

        yield return new WaitForSeconds(1f * _wave.rate3);
        SpawnEnemy3(_wave.enemy3);
            yield return new WaitForSeconds(1f * _wave.rate3);
        }


        state = SpawnState.WAITING;

        yield break;
    }
    void SpawnEnemy(Transform _enemy)
    {
        if (!UpgradeSystem.onPause)
        {
            Debug.Log("Spawning Enemy: " + _enemy.name);
            Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
            Instantiate(_enemy, _sp.position, _sp.rotation);
 
        }
    }
    void SpawnEnemy2(Transform _enemy2)
    {
        if (!UpgradeSystem.onPause)
        {
            Debug.Log("Spawning Enemy: " + _enemy2.name);
            Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
            Instantiate(_enemy2, _sp.position, _sp.rotation);
        }
    }
    void SpawnEnemy3(Transform _enemy3)
    {
        if (!UpgradeSystem.onPause)
        {
            Debug.Log("Spawning Enemy: " + _enemy3.name);
            Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
            Instantiate(_enemy3, _sp.position, _sp.rotation);
        }
    }
}
