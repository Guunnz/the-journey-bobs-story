﻿using UnityEngine;
using System.Collections;

public class ParticleMovement : MonoBehaviour {

    public float movement = 0.0f;
    public bool increase = true;
    public bool decrease = false;
    public float value = 0.2f;


    // Update is called once per frame
    void Update ()
    {
        if (increase)
        {
            movement += 0.1f * Time.deltaTime;
        }
        if (decrease)
        {
            movement -= 0.1f * Time.deltaTime;
        }
        gameObject.transform.localPosition = new Vector2(transform.localPosition.x, transform.localPosition.y + movement);
        if (movement >= value)
        {
            increase = false;
            decrease = true;
        }
        if (movement <= -value)
        {
            decrease = false;
            increase = true;
        }
	}
}
