﻿using UnityEngine;
using System.Collections;

public class ShieldAnim : MonoBehaviour {

    public bool gotHit;

    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Enemy") && gotHit == false)
        {
            FindObjectOfType<AudioManager>().Play("Shield");
            gotHit = true;
            gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
            Invoke("InvencibilityFrames", 0.2f);
        }
    }
    void InvencibilityFrames()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        gotHit = false;
    }
}