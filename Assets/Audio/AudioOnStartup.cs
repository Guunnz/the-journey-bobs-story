﻿using UnityEngine;
using System.Collections;

public class AudioOnStartup : MonoBehaviour {

    public string AudioToPlay;
    public float volume = 0.7f;

	// Use this for initialization
	void Start ()
    {
        AudioListener.volume = 0;
        FindObjectOfType<AudioManager>().Play(AudioToPlay);
    }
	
	// Update is called once per frame
	void Update ()
    {
        AudioListener.volume += 0.3f * Time.deltaTime;
        if (AudioListener.volume >= volume)
        {
            Destroy(gameObject);
        }
	}
}
