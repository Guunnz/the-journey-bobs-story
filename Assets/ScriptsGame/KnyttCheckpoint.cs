﻿using UnityEngine;
using System.Collections;

public class KnyttCheckpoint : MonoBehaviour {


    public GameObject cam;
    public string ThisSceneIs;
    public bool onKnyttSave;
    public ParticleSystem particles;
    public ParticleSystem baseparticles;
    public GameObject Saving;
    public bool saved = false;
    public bool stormcheck;
    public float camzpos2;
    public bool onJungle;

    void Start()
    {
        saved = false;
        MiscControl.camZPos = camzpos2;
    }


    void Update()
    {
        if (onKnyttSave == true && saved == false)
        {
            FindObjectOfType<AudioManager>().VolumeIn("Checkpoint");
            StartupCheck.onStartup = 1;
            Saving.SetActive(true);
            particles.startColor = new Color(255F, 255F, 255F, 0f);
            baseparticles.startColor = new Color(255F, 255F, 255F, 0f);
            PlayerPrefs.SetString("LevelToLoad", ThisSceneIs);
            MiscControl.camXPos = cam.transform.position.x;
            MiscControl.camYPos = cam.gameObject.transform.position.y;
            MiscControl.camYPos = cam.gameObject.transform.position.z;
            MiscControl.playerXPos = this.gameObject.transform.position.x;
            MiscControl.playerYPos = this.gameObject.transform.position.y;
            PlayerPrefs.SetInt("torrenteCheck", MiscControl.torrenteCheck);
            PlayerPrefs.SetInt("onStartup", StartupCheck.onStartup);
            PlayerPrefs.SetFloat("playerXPos", MiscControl.playerXPos);
            PlayerPrefs.SetFloat("playerYPos", MiscControl.playerYPos);
            PlayerPrefs.SetFloat("camXPos", MiscControl.camXPos);
            PlayerPrefs.SetFloat("camZPos", MiscControl.camZPos);
            PlayerPrefs.SetFloat("camYPos", MiscControl.camYPos);
            Invoke("CheckpointStats", 0.1f);
            Invoke("SavingFade", 2.07f);
            PlayerPrefs.Save();
            saved = true;
            if (stormcheck == true)
            {
                MiscControl.torrenteCheck = 1;
            }
            if (stormcheck == false)
            {
                MiscControl.torrenteCheck = 0;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && onJungle)
            {
                Debug.Log("Jungle Save Called");
                PlayerPrefs.SetInt("RockOn", JGCheckpoint.RockOn);
                PlayerPrefs.SetInt("GEMOff", JGCheckpoint.GEMoff);
                PlayerPrefs.SetInt("NPCoff", JGCheckpoint.NPCoff);
                PlayerPrefs.SetInt("yetiDone", JGCheckpoint.yetiDone);
                PlayerPrefs.SetInt("hugly1Off", JGCheckpoint.hugly1Off);
                PlayerPrefs.SetInt("hugly2On", JGCheckpoint.hugly2On);
                PlayerPrefs.SetInt("check1", JGCheckpoint.check1);
                PlayerPrefs.SetInt("check2", JGCheckpoint.check2);
                PlayerPrefs.SetInt("check3", JGCheckpoint.check3);
            }
        }
    }
    
    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            onKnyttSave = true;
        }
    }

        void OnTriggerExit2D(Collider2D info)
    {
            if (info.tag == "Player")
            {
                onKnyttSave = false;
            }
        }


        void CheckpointStats()
    {
        Debug.Log(PlayerPrefs.GetString("LevelToLoad"));
        Debug.Log(PlayerPrefs.GetFloat("playerXPos"));
        Debug.Log(PlayerPrefs.GetFloat("playerYPos"));
        Debug.Log(PlayerPrefs.GetFloat("camXPos"));
        Debug.Log(PlayerPrefs.GetFloat("camYPos"));
    }

    void SavingFade()
    {
        Saving.SetActive(false);
    }

}