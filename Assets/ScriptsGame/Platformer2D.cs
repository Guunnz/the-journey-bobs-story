﻿using UnityEngine;
using System.Collections;

public class Platformer2D : MonoBehaviour
{
    [SerializeField]
    private LayerMask whatIsGround; //
    private float groundedRadius = 1.5f; // 
    private bool grounded = false; // 
    public bool ground;
    private bool walledr = false;
    private float walledrRadius = 1.5f;
    private float walledlRadius = 0.8f;
    private bool walledl = false;
    private Transform groundCheck; //
    private Transform rwallCheck;
    private Transform lwallCheck;
    private Animator anim; //
    public float move;
    public float facing;
    public Transform player;
    private float speed = 30f;
    private Transform rwallCheck2;
    private bool walledr2 = false;
    public bool world1;
    public bool world2;
    public bool world3;
    private void Awake()
    {
        // Setting up references.
        player = transform.Find("Player");
        groundCheck = transform.Find("GroundCheck");
        rwallCheck = transform.Find("rwallCheck");
        lwallCheck = transform.Find("lwallCheck");
        rwallCheck2 = transform.Find("rwallCheck2");
        anim = GetComponent<Animator>();
    }
    private void Start()
    {
        if (world1)
        {
            anim.SetBool("World1", world1);
        }
        if (world2)
        {
            anim.SetBool("World2", world2);
        }
        if (world3)
        {
            anim.SetBool("World3", world3);
        }
    }
        // Update is called once per frame
        void Update()
    {

        if(Input.GetAxisRaw("Horizontal") < -0.9)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(-2, 2, 2);
            facing = -0.01f;
        }
        if(Input.GetAxisRaw("Horizontal") > 0.9)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(2, 2, 2);
            facing = 0.01f;
        }
        move = facing * GetComponent<Rigidbody2D>().velocity.x;
        if (Input.GetAxisRaw("Horizontal") < 0 && Time.timeScale != 0 && grounded && !walledr && !walledr2)
        {
            FindObjectOfType<AudioManager>().VolumeIn("Footsteps");
        }
        if (Input.GetAxisRaw("Horizontal") > 0 && Time.timeScale != 0 && grounded && !walledr && !walledr2)
        {
            FindObjectOfType<AudioManager>().VolumeIn("Footsteps");
        }
        if (Input.GetKeyDown(InputManager.Spacebar) && grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 50f);
        }
        if (Input.GetAxisRaw("Horizontal") == 0 && grounded)
        {
            NoMove();
        }
        if (walledr)
        {
        NoMove();
        }
        if (walledr2)
        {
            NoMove();
        }
        if (walledl)
        {
            grounded = false;
        }
        if (move >= 1)
        {
            move = 1;
        }
        if (move <= 0)
        {
            move = 0;
        }
        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
        anim.SetBool("isGrounded", grounded);
        walledr = Physics2D.OverlapCircle(rwallCheck.position, walledrRadius, whatIsGround);
        anim.SetBool("walledR", walledr);
        walledr2 = Physics2D.OverlapCircle(rwallCheck2.position, walledrRadius, whatIsGround);
        anim.SetBool("walledR", walledr);
        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(move));
        walledl = Physics2D.OverlapCircle(lwallCheck.position, walledlRadius, whatIsGround);
        anim.SetBool("walledL", walledl);
    }

    void NoMove()
    {
        FindObjectOfType<AudioManager>().VolumeOut("Footsteps");
        grounded = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
    }
}
