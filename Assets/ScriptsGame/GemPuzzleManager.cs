﻿using UnityEngine;
using System.Collections;

public class GemPuzzleManager : MonoBehaviour
{
    public GameObject puzzleDoorDown;
    public GameObject puzzleDoorIn;
    public GameObject ResultsBar;
    public GameObject Yellow1;
    public GameObject Yellow2;
    public GameObject Yellow3;
    public GameObject Yellow4;
    public GameObject Yellow5;
    public GameObject Blue1;
    public GameObject Blue2;
    public GameObject Blue3;
    public GameObject Blue4;
    public GameObject Blue5;
    public GameObject Red1;
    public GameObject Red2;
    public GameObject Red3;
    public GameObject Red4;
    public GameObject Red5;
    public GameObject Green1;
    public GameObject Green2;
    public GameObject Green3;
    public GameObject Green4;
    public GameObject Green5;
    public GameObject Pink1;
    public GameObject Pink2;
    public GameObject Pink3;
    public GameObject Pink4;
    public GameObject Pink5;
    public bool Square1Done;
    public bool Square2Done;
    public bool Square3Done;
    public bool Square4Done;
    public bool Square5Done;
    public float Square1 = 0;
    public float Square2 = 0;
    public float Square3 = 0;
    public float Square4 = 0;
    public float Square5 = 0;
    public float luces;
    public float Resultado = 30;
    public float Respuesta;
    public bool Pinkin1;
    public bool Pinkin2;
    public bool Pinkin3;
    public bool Pinkin4;
    public bool Pinkin5;
    public bool collided;
    public string Square1txt;
    public string Square2txt;
    public string Square3txt;
    public string Square4txt;
    public string Square5txt;
    private float lights;
    public bool callingReset;
    public bool callingSuccess;
    public GameObject Camera1;
    public GameObject Camera2;
    public GameObject tablero1;
    public GameObject tablero2;


    // Update is called once per frame
    void Update()
    {
        ScoreBoard.Score = Resultado;
        if (Resultado != Respuesta && Square1Done && Square2Done && Square3Done && Square4Done && Square5Done && !callingReset)
        {
            tablero1.GetComponent<SpriteRenderer>().color = Color.red;
            tablero2.GetComponent<SpriteRenderer>().color = Color.red;
            callingReset = true;
            FindObjectOfType<AudioManager>().Play("Error");

            Invoke("Reset", 2f);
        }
        Resultado = lights + Square1 + Square2 + Square3 + Square4 + Square5;
        if (Resultado == Respuesta && !callingSuccess)
        {
            tablero1.GetComponent<SpriteRenderer>().color = Color.green;
            tablero2.GetComponent<SpriteRenderer>().color = Color.green;
            callingSuccess = true;
            FindObjectOfType<AudioManager>().Play("Success");
            Decam();
            Invoke("LevelCompleted", 4f);

        }
        if (Resultado != Respuesta)
        {
            callingSuccess = false;
            puzzleDoorIn.SetActive(false);
            puzzleDoorDown.SetActive(true);
        }
    }
    void Decam()
    {
        Camera1.SetActive(false);
        Camera2.SetActive(true);

    }
    void Oncam()
    {

        Camera1.SetActive(true);
        Camera2.SetActive(false);
    }
    void LevelCompleted()
    {
        Oncam();
        puzzleDoorDown.SetActive(false);
        puzzleDoorIn.SetActive(true);
    }


    void Reset()
    {
        if (Resultado != Respuesta)
        {
            Invoke("Oncam", 2f);
            Invoke("WhiteTableros", 2f);
            lights = luces;
            callingReset = false;
            Yellow1.SetActive(false);
            Yellow2.SetActive(false);
            Yellow3.SetActive(false);
            Yellow4.SetActive(false);
            Yellow5.SetActive(false);
            Red1.SetActive(false);
            Red2.SetActive(false);
            Red3.SetActive(false);
            Red4.SetActive(false);
            Red5.SetActive(false);
            Blue1.SetActive(false);
            Blue2.SetActive(false);
            Blue3.SetActive(false);
            Blue4.SetActive(false);
            Blue5.SetActive(false);
            Green1.SetActive(false);
            Green2.SetActive(false);
            Green3.SetActive(false);
            Green4.SetActive(false);
            Green5.SetActive(false);
            Pink1.SetActive(false);
            Pink2.SetActive(false);
            Pink3.SetActive(false);
            Pink4.SetActive(false);
            Pink5.SetActive(false);
            Pinkin1 = false;
            Pinkin2 = false;
            Pinkin3 = false;
            Pinkin4 = false;
            Pinkin5 = false;
            Square1 = 0;
            Square2 = 0;
            Square3 = 0;
            Square4 = 0;
            Square5 = 0;
            Square1Done = false;
            Square2Done = false;
            Square3Done = false;
            Square4Done = false;
            Square5Done = false;
            Square1txt = "";
            Square2txt = "";
            Square3txt = "";
            Square4txt = "";
            Square5txt = "";
        }

    }
    void WhiteTableros()
    {
        tablero1.GetComponent<SpriteRenderer>().color = Color.white;
        tablero2.GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void Start()
    {
        lights = luces;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Yellow")
        {
            collided = true;
            if (!Square1Done && collided)
            {
                Square1txt = "Yellow";
                collided = false;
                Square1Done = true;
                Square1 = 1;
                Yellow1.SetActive(true);
            }
            if (Square1Done && !Square2Done && collided && Square1txt != "Yellow")
            {
                Square2txt = "Yellow";
                collided = false;
                Square2Done = true;
                Yellow2.SetActive(true);
                if (Square1 == 3f)
                {
                    Square2 = 0;
                }
                if (Square1 != 3f)
                {
                    Square2 = 1;
                }
                if (Square1txt == "Pink")
                {
                    Square2 = 0;
                    Square1txt = "Pink";
                    Square2txt = "";
                    Yellow2.SetActive(false);
                    Square2Done = false;
                }
                if (Square1txt == "Green")
                {
                    Square1txt = "Yellow";
                    Square2txt = "";
                    Yellow2.SetActive(false);
                    Yellow1.SetActive(true);
                    Green1.SetActive(false);
                    Square1 = 1;
                    Square2Done = false;
                }
                if (Square1 == 0 && Square1txt == "Blue")
                {
                    Square2 = -1;
                }
                if (Square1 == 0 && Square1txt == "Red")
                {
                    Square2 = 1;
                }
            }
            if (Square1Done && Square2Done && !Square3Done && collided && Square2txt != "Yellow")
            {
                Square3txt = "Yellow";
                collided = false;
                Square3Done = true;
                Yellow3.SetActive(true);
                if (Square2 == 3f)
                {
                    Square3 = 0;
                }
                if (Square2 != 3f)
                {
                    Square3 = 1;
                }
                if (Square2txt == "Pink")
                {
                    Square3 = 0;
                    Square2txt = "Pink";
                    Square3txt = "";
                    Yellow3.SetActive(false);
                    Square3Done = false;
                }
                if (Square2txt == "Green")
                {
                    Square2txt = "Yellow";
                    Square3txt = "";
                    Yellow3.SetActive(false);
                    Yellow2.SetActive(true);
                    Green2.SetActive(false);
                    Square2 = 1;
                    Square3Done = false;
                }
                if (Square2 == 0 && Square2txt == "Blue")
                {
                    Square3 = -1;
                }
                if (Square2 == 0 && Square2txt == "Red")
                {
                    Square3 = 1;
                }
            }
            if (Square1Done && Square2Done && Square3Done && !Square4Done && collided && Square3txt != "Yellow")
            {
                Square4txt = "Yellow";
                collided = false;
                Square4Done = true;
                Yellow4.SetActive(true);
                if (Square3 == 3f)
                {
                    Square4 = 0;
                }
                if (Square3 != 3f)
                {
                    Square4 = 1;
                }
                if (Square3txt == "Pink")
                {
                    Square4 = 0;
                    Square3txt = "Pink";
                    Square4txt = "";
                    Yellow4.SetActive(false);
                    Square4Done = false;
                }
                if (Square3txt == "Green")
                {
                    Square3txt = "Yellow";
                    Square4txt = "";
                    Yellow4.SetActive(false);
                    Yellow3.SetActive(true);
                    Green3.SetActive(false);
                    Square3 = 1;
                    Square4Done = false;
                }
                if (Square3 == 0 && Square3txt == "Blue")
                {
                    Square4 = -1;
                }
                if (Square3 == 0 && Square3txt == "Red")
                {
                    Square4 = 1;
                }
            }
            if (Square1Done && Square2Done && Square3Done && Square4Done && !Square5Done && collided && Square4txt != "Yellow")
            {
                Square5txt = "Yellow";
                collided = false;
                Square5Done = true;
                Square5 = 1;
                Yellow5.SetActive(true);
                if (Square4 == 3f)
                {
                    Square5 = 0;
                }
                if (Square4 != 3f)
                {
                    Square5 = 1;
                }
                if (Square4txt == "Pink")
                {
                    Square5 = 0;
                    Square4txt = "Pink";
                    Square5txt = "";
                    Yellow5.SetActive(false);
                    Square5Done = false;
                }
                if (Square4txt == "Green")
                {
                    Square4txt = "Yellow";
                    Square5txt = "";
                    Yellow5.SetActive(false);
                    Yellow4.SetActive(true);
                    Green4.SetActive(false);
                    Square4 = 1;
                    Square5Done = false;
                }
                if (Square4 == 0 && Square4txt == "Blue")
                {
                    Square5 = -1;
                }
                if (Square4 == 0 && Square4txt == "Red")
                {
                    Square5 = 1;
                }
            }
        }
        if (col.gameObject.tag == "Blue")
        {
            collided = true;
            if (!Square1Done && collided)
            {
                Square1txt = "Blue";
                collided = false;
                Square1Done = true;
                Blue1.SetActive(true);
            }
            if (Square1Done && !Square2Done && collided && Square1txt != "Blue")
            {
                Square2txt = "Blue";
                collided = false;
                Square2Done = true;
                Blue2.SetActive(true);
                if (Square1txt == "Red")
                {
                    Square1txt = "Blue";
                    Square2txt = "";
                    Red1.SetActive(false);
                    Blue1.SetActive(true);
                    Blue2.SetActive(false);
                    Square2Done = false;
                }
            }
            if (Square1Done && Square2Done && !Square3Done && collided && Square2txt != "Blue")
            {
                Square3txt = "Blue";
                collided = false;
                Square3Done = true;
                Blue3.SetActive(true);
                if (Square2txt == "Red")
                {
                    Square2txt = "Blue";
                    Square3txt = "";
                    Red2.SetActive(false);
                    Blue2.SetActive(true);
                    Blue3.SetActive(false);
                    Square3Done = false;
                }
            }
            if (Square1Done && Square2Done && Square3Done && !Square4Done && collided && Square3txt != "Blue")
            {
                Square4txt = "Blue";
                collided = false;
                Square4Done = true;
                Blue4.SetActive(true);
                if (Square3txt == "Red")
                {
                    Square3txt = "Blue";
                    Square4txt = "";
                    Red3.SetActive(false);
                    Blue3.SetActive(true);
                    Blue4.SetActive(false);
                    Square4Done = false;
                }
            }
            if (Square1Done && Square2Done && Square3Done && Square4Done && !Square5Done && collided && Square4txt != "Blue")
            {
                Square5txt = "Blue";
                collided = false;
                Blue5.SetActive(true);
                Square5Done = true;
                if (Square4txt == "Red")
                {
                    Square4txt = "Blue";
                    Square5txt = "";
                    Red4.SetActive(false);
                    Blue4.SetActive(true);
                    Blue5.SetActive(false);
                    Square3Done = false;
                }
            }
        }
        if (col.gameObject.tag == "Red")
        {
            collided = true;
            if (!Square1Done && collided)
            {
                Square1txt = "Red";
                collided = false;
                Square1Done = true;
                Red1.SetActive(true);
            }
            if (Square1Done && !Square2Done && collided && Square1txt != "Red")
            {
                Square2txt = "Red";
                collided = false;
                Square2Done = true;
                Red2.SetActive(true);
                if (Square1txt == "Blue")
                {
                    Square1txt = "Red";
                    Square2txt = "";
                    Red1.SetActive(true);
                    Blue1.SetActive(false);
                    Red2.SetActive(false);
                    Square2Done = false;
                }
            }
            if (Square1Done && Square2Done && !Square3Done && collided && Square2txt != "Red")
            {
                Square3txt = "Red";
                collided = false;
                Square3Done = true;
                Red3.SetActive(true);
                if (Square2txt == "Blue")
                {
                    Square2txt = "Red";
                    Square3txt = "";
                    Red2.SetActive(true);
                    Blue2.SetActive(false);
                    Red3.SetActive(false);
                    Square3Done = false;
                }
            }
            if (Square1Done && Square2Done && Square3Done && !Square4Done && collided && Square3txt != "Red")
            {
                Square4txt = "Red";
                collided = false;
                Square4Done = true;
                Red4.SetActive(true);
                if (Square3txt == "Blue")
                {
                    Square3txt = "Red";
                    Square4txt = "";
                    Red3.SetActive(true);
                    Blue3.SetActive(false);
                    Red4.SetActive(false);
                    Square4Done = false;
                }
            }
            if (Square1Done && Square2Done && Square3Done && Square4Done && !Square5Done && collided && Square4txt != "Red")
            {
                Square5txt = "Red";
                collided = false;
                Square5Done = true;
                Red5.SetActive(true);
                if (Square4txt == "Blue")
                {
                    Square4txt = "Red";
                    Square5txt = "";
                    Red4.SetActive(true);
                    Blue4.SetActive(false);
                    Red5.SetActive(false);
                    Square5Done = false;
                }
            }
        }
        if (col.gameObject.tag == "Green")
        {
            collided = true;
            if (!Square1Done && collided)
            {
                Square1txt = "Green";
                collided = false;
                Square1Done = true;
                Green1.SetActive(true);
                Square1 = 3f;
            }
            if (Square1Done && !Square2Done && collided && Square1txt != "Green")
            {
                Square2txt = "Green";
                collided = false;
                Square2Done = true;
                Green2.SetActive(true);
                if (Square1 == 1)
                {
                    Square2 = 0;
                }
                if (Square1 != 1)
                {
                    Square2 = 3f;
                }
                if (Square1txt == "Yellow")
                {
                    Square1txt = "Green";
                    Square2txt = "";
                    Green2.SetActive(false);
                    Green1.SetActive(true);
                    Yellow1.SetActive(false);
                    Square1 = 3f;
                    Square2Done = false;
                }
                if (Square1txt == "Pink")
                {
                    Square2 = 0;
                    Square1txt = "Pink";
                    Square2txt = "";
                    Green2.SetActive(false);
                    Square2Done = false;
                }
                if (Square1txt == "Blue")
                {
                    Square2 = -3f;
                }
                if (Square1txt == "Red")
                {
                    Square2 = 3f;
                }
            }
            if (Square1Done && Square2Done && !Square3Done && collided && Square2txt != "Green")
            {
                Square3txt = "Green";
                collided = false;
                Square3Done = true;
                Green3.SetActive(true);
                if (Square2 == 1)
                {
                    Square3 = 0;
                }
                if (Square2 != 1)
                {
                    Square3 = 3f;
                }
                if (Square2txt == "Yellow")
                {
                    Square2txt = "Green";
                    Square3txt = "";
                    Green3.SetActive(false);
                    Green2.SetActive(true);
                    Yellow2.SetActive(false);
                    Square2 = 3f;
                    Square3Done = false;
                }
                if (Square2txt == "Pink")
                {
                    Square3 = 0;
                    Square2txt = "Pink";
                    Square3txt = "";
                    Green3.SetActive(false);
                    Square3Done = false;
                }
                if (Square2txt == "Blue")
                {
                    Square3 = -3f;
                }
                if (Square2txt == "Red")
                {
                    Square3 = 3f;
                }
            }
            if (Square1Done && Square2Done && Square3Done && !Square4Done && collided && Square3txt != "Green")
            {
                Square4txt = "Green";
                collided = false;
                Square4Done = true;
                Green4.SetActive(true);
                if (Square3 == 1)
                {
                    Square4 = 0;
                }
                if (Square3 != 1)
                {
                    Square4 = 3f;
                }
                if (Square3txt == "Pink")
                {
                    Square4 = 0;
                    Square3txt = "Pink";
                    Square4txt = "";
                    Green4.SetActive(false);
                    Square4Done = false;
                }
                if (Square3txt == "Yellow")
                {
                    Square3txt = "Green";
                    Square4txt = "";
                    Green4.SetActive(false);
                    Green3.SetActive(true);
                    Yellow3.SetActive(false);
                    Square3 = 3f;
                    Square4Done = false;
                }
                if (Square3txt == "Blue")
                {
                    Square4 = -3f;
                }
                if (Square3txt == "Red")
                {
                    Square4 = 3f;
                }
            }
            if (Square1Done && Square2Done && Square3Done && Square4Done && !Square5Done && collided && Square4txt != "Green")
            {
                Square5txt = "Green";
                collided = false;
                Square5Done = true;
                Green5.SetActive(true);
                if (Square4 == 1)
                {
                    Square5 = 0;
                }
                if (Square4 != 1)
                {
                    Square5 = 3f;
                }
                if (Square4txt == "Yellow")
                {
                    Square4txt = "Green";
                    Square5txt = "";
                    Green5.SetActive(false);
                    Green4.SetActive(true);
                    Yellow4.SetActive(false);
                    Square4 = 3f;
                    Square5Done = false;
                }
                if (Square4txt == "Pink")
                {
                    Square5 = 0;
                    Square4txt = "Pink";
                    Square5txt = "";
                    Green5.SetActive(false);
                    Square5Done = false;
                }
                if (Square4txt == "Blue")
                {
                    Square5 = -3f;
                }
                if (Square4txt == "Red")
                {
                    Square5 = 3f;
                }
            }
        }
        if (col.gameObject.tag == "Pink")
        {
            collided = true;
            if (!Square1Done && collided)
            {
                lights = lights / 2;
                Square1 = 0;
                Square1txt = "Pink";
                collided = false;
                Square1Done = true;
                Pinkin1 = true;
                Pink1.SetActive(true);
            }
            if (Square1Done && !Square2Done && collided && Square1txt != "Pink")
            {
                Square1 = Square1 / 2;
                Square2txt = "Pink";
                collided = false;
                Square2Done = true;
                Pinkin2 = true;
                Pink2.SetActive(true);
            }
            if (Square1Done && Square2Done && !Square3Done && collided && Square2txt != "Pink")
            {
                Square2 = Square2 / 2;
                Square3txt = "Pink";
                collided = false;
                Square3Done = true;
                Pinkin3 = true;
                Pink3.SetActive(true);
            }
            if (Square1Done && Square2Done && Square3Done && !Square4Done && collided && Square3txt != "Pink")
            {
                Square3 = Square3 / 2;
                Square4txt = "Pink";
                collided = false;
                Square4Done = true;
                Pinkin4 = true;
                Pink4.SetActive(true);
            }
            if (Square1Done && Square2Done && Square3Done && Square4Done && !Square5Done && collided && Square4txt != "Pink")
            {
                Square4 = Square4 / 2;
                Square5txt = "Pink";
                collided = false;
                Square5Done = true;
                Pinkin5 = true;
                Pink5.SetActive(true);
            }
        }


        if (col.gameObject.tag == "Black" && Resultado != Respuesta)
        {
            Decam();
            tablero1.GetComponent<SpriteRenderer>().color = Color.red;
            tablero2.GetComponent<SpriteRenderer>().color = Color.red;
            callingReset = true;
            FindObjectOfType<AudioManager>().Play("Error");
            Invoke("Reset", 2f);

        }
    }


}