﻿using UnityEngine;
using System.Collections;

public class Furnace : MonoBehaviour
{

    public GameObject Objeto;
    public GameObject ObjetoADestruir;
    public bool destroyed = false;
    public GameObject combinador1;
    public GameObject interact;
    public GameObject combinador2;
    public GameObject combinedgems;

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player" && !destroyed && NPCWGEM.furnace == true)
        {
            combinador1.SetActive(false);
            combinador2.SetActive(true);
            destroyed = true;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && destroyed && NPCWGEM.furnace == true)
        {
            Destroy(ObjetoADestruir);
            FindObjectOfType<AudioManager>().Play("Furnace");
            NPCWGEM.furnace = false;
            combinedgems.SetActive(true);
            combinador1.SetActive(true);
            combinador2.SetActive(false);
            SteamScript.whichLogro = "Logro9";
            SteamScript.logro = "Logro9";
            Debug.Log("Destroying...");
            Objeto.GetComponent<CircleCollider2D>().enabled = true;
            Objeto.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
        }
        if (NPCWGEM.furnace == true)
        {
            interact.SetActive(true);
        }
    }
}