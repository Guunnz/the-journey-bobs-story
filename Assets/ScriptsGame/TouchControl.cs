﻿using UnityEngine;
using System.Collections;

public class TouchControl : MonoBehaviour
{
    public delegate void Touched(GameObject touchedGO);
    public static event Touched OnTouched;

    private Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            ScreenToMouseRay();
        }

#elif UNITY_ANDROID
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            ScreenToMouseRay();
        }
#endif

    }

    private void ScreenToMouseRay()
    {

        Vector2 worldMousePos = Vector2.zero;

#if UNITY_EDITOR

        worldMousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        
#elif UNITY_ANDROID

        worldMousePos = mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);

#endif

        RaycastHit2D hitInfo = Physics2D.Raycast(worldMousePos, Vector2.zero, 0f);
        if (hitInfo && OnTouched != null)
        {
            OnTouched(hitInfo.collider.gameObject);
        }
    }
}

