﻿using UnityEngine;
using System.Collections;

public class ParticleEnabler : MonoBehaviour {

	public GameObject Particle;
	
	// Update is called once per frame
	void OnMouseDown ()
	{
		Particle.GetComponent<ParticleSystem>().enableEmission = true;
		Invoke ("ParticleOff", 0.5f);
	}


void ParticleOff()
{
		Particle.GetComponent<ParticleSystem>().enableEmission = false;
}
}
