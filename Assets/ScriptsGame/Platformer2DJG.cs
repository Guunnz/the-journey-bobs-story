﻿using UnityEngine;
using System.Collections;

public class Platformer2DJG : MonoBehaviour
{
    [SerializeField]
    private LayerMask whatIsGround; //
    static public bool onBox;
    private float groundedRadius = 1.6f; // 
    private bool grounded = false; // 
    public bool ground;
    private bool walledr = false;
    private float walledrRadius = 2f;
    private float walledlRadius = 0.8f;
    private bool walledr2 = false;
    private bool walledl = false;
    private Transform groundCheck; //
    private Transform rwallCheck;
    private Transform rwallCheck2;
    private Transform lwallCheck;
    public float speed = 25f;
    public float jumpForce = 45f;
    private Animator anim; //
    public float move;
    public float facing;
    public Transform player;
    public bool water;
    private float Lspeed = 9f;
    private float Dspeed = 9f;
    private float Uspeed = 9f;
    private float Rspeed = 9f;
    private float URspeed = 9f;
    private float ULspeed = 9f;
    private float DRspeed = 9f;
    private float DLspeed = 9f;
    public Transform targetL;
    public Transform targetR;
    public Transform targetU;
    public Transform targetD;
    public Transform targetDL;
    public Transform targetDR;
    public Transform targetUL;
    public Transform targetUR;
    static public bool inmovible;
    static public bool jetOnWater = false;
    public float DW = -400;
    public float negxx = -2;
    public float xx = 2;
    public float yy = 2;
    public float zz = 2;
    public int gravity = 12;
    public ParticleSystem jetOnWaterAnim;
    public int negSpeedOnWater = -25;
    public int posSpeedOnWater = 25;
    public bool waterCalled;
    public bool superJump;
    public Transform backtrigger;
    public Transform frontcollider;
    public bool world1;
    public bool world2;
    public bool world3;
    public float watertiming;
    public bool badEnding;

    public GameObject underwater;

    private void Awake()
    {
        // Setting up references.
        player = transform.Find("Player");
        groundCheck = transform.Find("GroundCheck");
        rwallCheck = transform.Find("rwallCheck");
        rwallCheck2 = transform.Find("rwallCheck2");
        lwallCheck = transform.Find("lwallCheck");
        frontcollider = transform.Find("FrontCollider");
        backtrigger = transform.Find("BackTrigger");
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        FindObjectOfType<AudioManager>().Play("Footsteps");
        OpenPause.squarethon = false;
        if (world1)
        {
            anim.SetBool("World1", world1);
        }
        if (world2)
        {
            anim.SetBool("World2", world2);
        }
        if (world3)
        {
            anim.SetBool("World3", world3);
        }
    }
  
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "BouncingPlatform")
        {
            FindObjectOfType<AudioManager>().Play("Bounce");
        }
    }

// Update is called once per frame
void Update()

    {  // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
        anim.SetBool("isGrounded", grounded);
        walledr = Physics2D.OverlapCircle(rwallCheck.position, walledrRadius, whatIsGround);
        anim.SetBool("walledR", walledr);
        walledr2 = Physics2D.OverlapCircle(rwallCheck2.position, walledrRadius, whatIsGround);
        anim.SetBool("walledR", walledr);
        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(move));
        walledl = Physics2D.OverlapCircle(lwallCheck.position, walledlRadius, whatIsGround);
        anim.SetBool("walledL", walledl);
        anim.SetBool("onWater", water);
        anim.SetBool("OnBox", onBox);

        if (Input.GetAxisRaw("Horizontal") < 0 && !inmovible && Time.timeScale != 0 && speed != 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(negxx, yy, zz);
            facing = -0.01f;
        }
        if (Input.GetAxisRaw("Horizontal") > 0 && !inmovible && Time.timeScale != 0 && speed != 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localScale = new Vector3(xx, yy, zz);
            facing = 0.01f;
        }
        if (Input.GetAxisRaw("Horizontal") < 0 && !inmovible && Time.timeScale != 0 && grounded && speed != 0 && !walledr && !walledr2)
        {
            FindObjectOfType<AudioManager>().VolumeIn("Footsteps");
        }
        if (Input.GetAxisRaw("Horizontal") > 0 && !inmovible && Time.timeScale != 0 && grounded && speed != 0 && !walledr && !walledr2)
        {
            FindObjectOfType<AudioManager>().VolumeIn("Footsteps");
        }
        move = facing * GetComponent<Rigidbody2D>().velocity.x;
        if (Input.GetKeyDown(InputManager.Spacebar) && grounded && !inmovible)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpForce);
        }
        if (Input.GetAxisRaw("Horizontal") == 0 && grounded)
        {
            FindObjectOfType<AudioManager>().VolumeOut("Footsteps");
            NoMove();
        }
        if (speed == 0)
        {
            FindObjectOfType<AudioManager>().VolumeOut("Footsteps");
        }
        if (walledr)
        {
            NoMove();
        }
        if (walledr2)
        {
            NoMove();
        }
        if (walledl)
        {
            grounded = false;
        }
        if (move >= 1)
        {
            move = 1;
        }
        if (move <= 0)
        {
            move = 0;
        }
        if (water && Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        }
        if (water)
        {
            underwater.SetActive(true);
            jetOnWaterAnim.enableEmission = true;
            jetOnWater = true;
            grounded = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            watertiming = 0;
        }
        if (!grounded)
        {
            FindObjectOfType<AudioManager>().VolumeOut("Footsteps");
        }
        if (water && !waterCalled)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -50f);
            waterCalled = true;
            Lspeed = 9f;
            Uspeed = 9f;
            Rspeed = 9f;
            URspeed = 9f;
            ULspeed = 9f;
            DRspeed = 9f;
            DLspeed = 9f;
        }
        if (superJump)
        {
            if (Input.GetAxisRaw("Vertical") > 0)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, posSpeedOnWater * 2);
            }
        }
        if (water == false)
        {
            jetOnWater = false;
            RotateUp();
            waterCalled = false;
            jetOnWaterAnim.enableEmission = false;
            GetComponent<Rigidbody2D>().gravityScale = gravity;
            watertiming += Time.deltaTime;
        }
        if (watertiming >= 0.25f)
        {
            underwater.SetActive(false);
            watertiming = 0.25f;
        }
        if (water && Input.GetAxisRaw("Vertical") < 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, negSpeedOnWater);
            RotateDown();
        }
        if (water && Input.GetAxisRaw("Vertical") > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, posSpeedOnWater);
            RotateUp();
        }
        if (water && Input.GetAxisRaw("Horizontal") > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(posSpeedOnWater, 0f);
            RotateRight();


        }
        if (water && Input.GetAxisRaw("Horizontal") < 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(negSpeedOnWater, 0f);
            RotateLeft();


        }
        if (water && Input.GetAxisRaw("Horizontal") < 0 && water && Input.GetAxisRaw("Vertical") > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(negSpeedOnWater, posSpeedOnWater);
            RotateUL();


        }
        if (water && Input.GetAxisRaw("Horizontal") < 0 && Input.GetAxisRaw("Vertical") < 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(negSpeedOnWater, negSpeedOnWater);
            RotateDL();


        }
        if (water && Input.GetAxisRaw("Horizontal") > 0 && Input.GetAxisRaw("Vertical") < 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(posSpeedOnWater, negSpeedOnWater);
            RotateDR();


        }
        if (water && Input.GetAxisRaw("Horizontal") > 0 && Input.GetAxisRaw("Vertical") > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(posSpeedOnWater, posSpeedOnWater);
            RotateUR();
        }
        if (water && Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        }
    }

    void NoMove()
    {
        FindObjectOfType<AudioManager>().VolumeOut("Footsteps");
        grounded = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
    }
    void Up()
    {
        DW = 0;
    }
    void RotateLeft()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetL.rotation, Lspeed);
        Dspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateRight()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetR.rotation, Rspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Uspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateUp()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetU.rotation, Uspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateDown()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetD.rotation, Dspeed);
        Lspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateUR()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetUR.rotation, URspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateUL()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetUL.rotation, ULspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        DRspeed = 9f;
        DLspeed = 9f;
    }

    void RotateDR()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetDR.rotation, DRspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DLspeed = 9f;
    }

    void RotateDL()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetDL.rotation, DLspeed);
        Lspeed = 9f;
        Dspeed = 9f;
        Uspeed = 9f;
        Rspeed = 9f;
        URspeed = 9f;
        ULspeed = 9f;
        DRspeed = 9f;
    }
    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "water")
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            backtrigger.GetComponent<BoxCollider2D>().enabled = false;
            frontcollider.GetComponent<BoxCollider2D>().enabled = false;

            water = true;
        }
        if (info.tag == "SuperJump")
        {
            superJump = true;
            water = false;
        }
        if (info.tag == "BadEnd")
        {
            if (badEnding)
            {
                inmovible = true;
                speed = 0;
                anim.SetBool("BadEnding", true);
                NoMove();
                walledr = true;
            }
        }
    }
    void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "water")
        {
            water = false;
            gameObject.GetComponent<CircleCollider2D>().enabled = true;
            backtrigger.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            frontcollider.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }
        if (info.tag == "SuperJump")
        {
            superJump = false;
        }
    }
}
