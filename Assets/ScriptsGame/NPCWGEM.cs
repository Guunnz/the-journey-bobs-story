﻿using UnityEngine;
using System.Collections;

public class NPCWGEM : MonoBehaviour {

    static public int dialogue = 0;
    public int endIn;
    public GameObject Dialogo1;
    public GameObject Dialogo2;
    public GameObject Dialogo3;
    public GameObject Dialogo4;
    public GameObject Dialogo5;
    public GameObject Dialogo6;
    public GameObject Dialogo7;
    public GameObject Dialogo8;
    public GameObject Dialogo9;
    public GameObject Dialogo10;
    public GameObject door;
    public GameObject door2;
    static public bool thisdialogue = false;
    static public bool furnace;

    // Update is called once per frame
    void Update()
    {
        Debug.Log(dialogue);
        if (dialogue >= 1 && Input.GetKeyDown(InputManager.E))
        {
            dialogue += 1;
        }
        if (dialogue == 1 && endIn != 1)
        {
            Dialogo1.SetActive(true);
        }
        if (dialogue == 1 && endIn != 1)
        {
            door.SetActive(true);
            door2.SetActive(false);
            Dialogo1.SetActive(true);
        }
        if (dialogue == 2 && endIn != 2)
        {
            Dialogo1.SetActive(false);
            Dialogo2.SetActive(true);
        }
        if (dialogue == 3 && endIn != 3)
        {
            Dialogo2.SetActive(false);
            Dialogo3.SetActive(true);
        }
        if (dialogue == 4 && endIn != 4)
        {
            Dialogo3.SetActive(false);
            Dialogo4.SetActive(true);
        }
        if (dialogue == 5 && endIn != 5)
        {
            Dialogo4.SetActive(false);
            Dialogo5.SetActive(true);
        }
        if (dialogue == 6 && endIn != 6)
        {
            Dialogo5.SetActive(false);
            Dialogo6.SetActive(true);
        }
        if (dialogue == 7 && endIn != 7)
        {
            Dialogo6.SetActive(false);
            Dialogo7.SetActive(true);
        }
        if (dialogue == 8 && endIn != 8)
        {
            Dialogo7.SetActive(false);
            Dialogo8.SetActive(true);
        }
        if (dialogue == 9 && endIn != 9)
        {
            Dialogo8.SetActive(false);
            Dialogo9.SetActive(true);
        }
        if (dialogue == 10 && endIn != 10)
        {
            Dialogo9.SetActive(false);
            Dialogo10.SetActive(true);
        }
        if (dialogue == 11 && endIn != 11)
        {
            Dialogo10.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }


        ///////////////


        if (dialogue == 2 && endIn == 2)
        {
            Dialogo1.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 3 && endIn == 3)
        {
            Dialogo2.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 4 && endIn == 4)
        {
            Dialogo3.SetActive(false);
            thisdialogue = false;
            dialogue = 0;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 5 && endIn == 5)
        {
            Dialogo4.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 6 && endIn == 6)
        {
            dialogue = 0;
            Dialogo5.SetActive(false);
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 7 && endIn == 7)
        {
            furnace = true;
            Platformer2DJG.inmovible = false;
            Dialogo6.SetActive(false);
            thisdialogue = false;
            dialogue = 8;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 8 && endIn == 8)
        {
            Dialogo7.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 9 && endIn == 9)
        {
            Dialogo8.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
        if (dialogue == 10 && endIn == 10)
        {
            Dialogo9.SetActive(false);
            dialogue = 0;
            thisdialogue = false;
            MiscControl.onDialogue = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            thisdialogue = true;
            MiscControl.onDialogue = true;
        }
    }
}
