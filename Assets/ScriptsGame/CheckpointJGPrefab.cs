﻿using UnityEngine;
using System.Collections;

public class CheckpointJGPrefab : MonoBehaviour
{
    public int RockOn;
    public int Gemoff;
    public int Npcoff;
    public int yetiDone;
    public int hugly1Off;
    public int hugly2On;
    public int check1;
    public int check2;
    public int check3;


    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            JGCheckpoint.RockOn = RockOn;
            JGCheckpoint.GEMoff = Gemoff;
            JGCheckpoint.NPCoff = Npcoff;
            JGCheckpoint.yetiDone = yetiDone;
            JGCheckpoint.hugly1Off = hugly1Off;
            JGCheckpoint.hugly2On = hugly2On;
            JGCheckpoint.check1 = check1;
            JGCheckpoint.check2 = check2;
            JGCheckpoint.check3 = check3;
        }
    }
}