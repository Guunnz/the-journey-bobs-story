﻿using UnityEngine;
using System.Collections;

public class YetiDialogueWGem : MonoBehaviour {
    static public int dialogue = 0;
    public int endIn;
    public GameObject Dialogo1;
    public GameObject Dialogo2;
    public GameObject Dialogo3;
    public GameObject Dialogo4;
    public GameObject Dialogo5;
    public GameObject GameEngine;
    public GameObject CanvasGame;
    public GameObject Dialogo7win;
    public GameObject Dialogo7lose;
    static public bool resetgame;
    static public bool thisdialogue = false;
    public bool called;
    public bool called1;
    public bool called2;
    public bool ingame;
    public GameObject checkpointwin;

    // Update is called once per frame
    void Update()
    {
        if (YetiMinigame.playerwins == true)
        {
            ingame = false;
        }
        if (YetiMinigame.yetiwins == true)
        {
            ingame = false;
        }
        if (dialogue >= 1 && Input.GetKeyDown(InputManager.E) && !ingame)
        {
            dialogue += 1;
        }
        if (dialogue == 1 && endIn != 1)
        {
            YetiMinigame.yetiwins = false;
            Dialogo1.SetActive(true);
        }
        if (dialogue == 2 && endIn != 2)
        {
            Dialogo1.SetActive(false);
            Dialogo2.SetActive(true);
        }
        if (dialogue == 3 && endIn != 3)
        {
            Dialogo2.SetActive(false);
            Dialogo3.SetActive(true);
        }
        if (dialogue == 4 && endIn != 4)
        {
            Dialogo3.SetActive(false);
            Dialogo4.SetActive(true);
        }
        if (dialogue == 5 && endIn != 5)
        {
            Dialogo4.SetActive(false);
            Dialogo5.SetActive(true);
        }
        if (dialogue == 6 && endIn != 6 && called == false)
        {
            called = true;
            called1 = true;
            YetiMinigame.StartGame = true;
            ingame = true;
            Dialogo5.SetActive(false);
            GameEngine.SetActive(true);
            CanvasGame.SetActive(true);
            Dialogo7lose.SetActive(false);
        }
        if (dialogue == 7 && endIn != 7 && YetiMinigame.playerwins == true)
        {
            GameEngine.SetActive(false);
            CanvasGame.SetActive(false);
            Dialogo7win.SetActive(true);
            checkpointwin.SetActive(true);
        }
        if (dialogue == 7 && endIn != 7 && YetiMinigame.yetiwins == true)
        {
            called = false;
            GameEngine.SetActive(false);
            CanvasGame.SetActive(false);
            Dialogo7lose.SetActive(true);
        }
        if (dialogue == 8 && endIn != 8 && YetiMinigame.yetiwins == true)
        {
            YetiMinigame.yetiwins = false;
            resetgame = true;
            Dialogo7lose.SetActive(false);
            dialogue = 6;
        }
        if (dialogue == 8 && endIn != 8 && YetiMinigame.playerwins == true)
        {

            YetiMinigame.StartGame = false;
            Platformer2DJG.inmovible = false;
            Dialogo7win.SetActive(false);
            dialogue = 9;
        }



        ///////////////


        if (dialogue == 2 && endIn == 2)
        {
        thisdialogue = false;
            Dialogo1.SetActive(false);
            MiscControl.onDialogue = false;
        }
        if (dialogue == 3 && endIn == 3)
        {
            thisdialogue = false;
            Dialogo2.SetActive(false);
            MiscControl.onDialogue = false;
        }
        if (dialogue == 4 && endIn == 4)
        {
            thisdialogue = false;
            Dialogo3.SetActive(false);
            MiscControl.onDialogue = false;
        }
        if (dialogue == 5 && endIn == 5)
        {
            thisdialogue = false;
            Dialogo4.SetActive(false);
            MiscControl.onDialogue = false;
        }
        if (dialogue == 6 && endIn == 6)
        {
            thisdialogue = false;
            Dialogo5.SetActive(false);
            MiscControl.onDialogue = false;
        }
        if (dialogue == 9 && endIn == 9)
        {
            thisdialogue = false;
            MiscControl.onDialogue = false;
            Invoke("Destroythis", 0.2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D info)
{
    if (info.tag == "Player")
    {
        thisdialogue = true;
        MiscControl.onDialogue = true;
    }
}

    void Destroythis()
    {
        Destroy(this.gameObject);
    }
}

