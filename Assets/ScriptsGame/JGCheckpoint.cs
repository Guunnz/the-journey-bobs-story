﻿using UnityEngine;
using System.Collections;

public class JGCheckpoint : MonoBehaviour {

    static public int RockOn;
    static public int GEMoff;
    static public int NPCoff;
    static public int yetiDone;
    static public int hugly1Off;
    static public int hugly2On;
    static public int check1;
    static public int check2;
    static public int check3;
    public GameObject endgamelluvia;
    public GameObject Rock;
    public GameObject Gem;
    public GameObject NPC;
    public GameObject Hugly1;
    public GameObject Hugly2;
    public GameObject Check1;
    public GameObject Check2;
    public GameObject Check3;
    public GameObject Check4;
    public GameObject npcaftergem;
    public GameObject yetidialogo;


    private void Start()
    {
        RockOn = PlayerPrefs.GetInt("RockOn");
        GEMoff = PlayerPrefs.GetInt("GEMOff");
        NPCoff = PlayerPrefs.GetInt("NPCoff");
        yetiDone = PlayerPrefs.GetInt("yetiDone");
        hugly1Off = PlayerPrefs.GetInt("hugly1Off");
        hugly2On = PlayerPrefs.GetInt("hugly2On");
        check1 = PlayerPrefs.GetInt("check1");
        check2 = PlayerPrefs.GetInt("check2");
        check3 = PlayerPrefs.GetInt("check3");
        if (StartupCheck.onStartup == 1)
        {
            LoadCheckpointFunction();
        }
    }


void LoadCheckpointFunction()
{
       RockOn = PlayerPrefs.GetInt("RockOn");
        GEMoff = PlayerPrefs.GetInt("GEMOff");
        NPCoff = PlayerPrefs.GetInt("NPCoff");
        yetiDone = PlayerPrefs.GetInt("yetiDone");
        hugly1Off = PlayerPrefs.GetInt("hugly1Off");
        hugly2On = PlayerPrefs.GetInt("hugly2On");
        check1 = PlayerPrefs.GetInt("check1");
        check2 = PlayerPrefs.GetInt("check2");
        check3 = PlayerPrefs.GetInt("check3");
    }
private void Update()
    {
        if(yetiDone == 1)
        {
            Destroy(yetidialogo);
            Check4.SetActive(true);
            endgamelluvia.SetActive(true);
        }
        if (check1 == 1)
        {
            Check1.SetActive(false);
        }
        if (check2 == 1)
        {
            Check2.SetActive(false);
        }
        if (check3 == 1)
        {
            Check3.SetActive(false);
        }
        if (NPCoff == 1)
        {
            npcaftergem.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
            NPCAftergem.doors = 1;
            NPC.SetActive(false);
        }
        if (RockOn == 1)
        {
            Rock.SetActive(true);
        }
        if (GEMoff == 1)
        {
            Gem.SetActive(false);
        }
        if (yetiDone == 1)
        {
            YetiMinigame.playerRoundsWonnered = 5;
        }
        if (hugly1Off == 1)
        {
            Hugly1.SetActive(false);
        }
        if (hugly2On == 1 && Hugly2.GetComponent<CircleCollider2D>() != null)
        {
            Hugly2.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
            Hugly2.GetComponent<CircleCollider2D>().enabled = true;
        }
    }



    void CheckWG2()
    {
        RockOn = 1;
        NPCoff = 1;
        GEMoff = 1;
    }
    void CheckWG3()
    {
        RockOn = 1;
        NPCoff = 1;
        GEMoff = 1;
    }
    void CheckWG4()
    {
        yetiDone = 1;
        hugly1Off = 1;
        hugly2On = 1;
    }
}
