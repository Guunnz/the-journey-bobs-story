﻿using UnityEngine;
using System.Collections;

public class PressEToActivateaGameobject : MonoBehaviour {

    public bool onTrigger;
    public GameObject objectToActivate;
    public bool dontDissapear;


	// Update is called once per frame
	void Update ()
    {
        if (onTrigger && Input.GetKeyDown(InputManager.E))
        {
            objectToActivate.SetActive(!objectToActivate.activeSelf);
        }
	}

    void OnTriggerEnter2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            onTrigger = true;
        }
    }
    void OnTriggerExit2D(Collider2D info)

    {
        if (info.tag == "Player" && !dontDissapear)  
        {
            onTrigger = false;
            objectToActivate.SetActive(false);
        }
        if (info.tag == "Player")
        {
            onTrigger = false;
        }
    }
}
