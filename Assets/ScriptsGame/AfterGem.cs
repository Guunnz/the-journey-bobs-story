﻿using UnityEngine;
using System.Collections;

public class AfterGem : MonoBehaviour {

    public GameObject Objeto;
    public GameObject ObjetoADestruir;

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            FindObjectOfType<AudioManager>().VolumeIn("Collectible");
            SteamScript.whichLogro = "Logro10";
            SteamScript.logro = "Logro10";
            Objeto.GetComponent<CircleCollider2D>().enabled = true;
            Objeto.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
            Invoke("Destroying", 0.01f);
        }
    }
   

    



    private void Destroying()
    {
        Destroy(this.gameObject);
        Destroy(ObjetoADestruir);
    }
}
