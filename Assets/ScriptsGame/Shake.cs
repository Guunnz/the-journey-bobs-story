﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{

    public float ShakeTimer;
    public float ShakeAmount;
    public float shakeAmountValue = 0.25f;
    public float shakeTimerValue = 2;
    static public bool isShaking = false;

    // Update is called once per frame
    void Update()

    {
        if (ShakeTimer >= 0)
        {
            Vector2 ShakePos = Random.insideUnitCircle * ShakeAmount;

            transform.position = new Vector3(transform.position.x + ShakePos.x, transform.position.y + ShakePos.y, transform.position.z);

            ShakeTimer -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        if (isShaking)
            {
            ShakeCamera(shakeAmountValue, shakeTimerValue);
            isShaking = false;
        }
    }

    public void ShakeCamera(float shakePwr, float shakeDur)
    {
        ShakeAmount = shakePwr;
        ShakeTimer = shakeDur;
    }
}

    