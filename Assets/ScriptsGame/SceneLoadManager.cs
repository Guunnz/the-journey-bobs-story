﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoadManager : MonoBehaviour {

	static public int sceneToLoad;
    public string sceneToPlay = "";
	
	// Update is called once per frame

    void Start ()
    {
       sceneToPlay = PlayerPrefs.GetString("LevelToLoad");
    }


    public void LoadGameScene()
    {
        SceneManager.LoadScene(sceneToPlay, LoadSceneMode.Single);
    }
}
