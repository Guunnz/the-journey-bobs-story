﻿using UnityEngine;
using System.Collections;

public class LabElevator1 : MonoBehaviour {

    public bool elevatorGoingUp;
    public float elevatorActualY;
    public float elevatorYTarget;
    public GameObject labElevator;
    public GameObject Player;
    public bool onElevator;
    public GameObject elevatorClosed;
    public bool elevatorOn;
    public GameObject sparks;
    
    public float elevatorSpeed = 20;


    private void Update()
    {
        if (Input.GetKeyDown(InputManager.E) && onElevator)
        {
            elevatorOn = true;
            elevatorClosed.SetActive(true);
        }
        if (elevatorOn && elevatorGoingUp && onElevator)
        {
            elevatorClosed.SetActive(true);
            Player.transform.position = new Vector2(Player.transform.position.x, elevatorActualY - 5.20974f);
            labElevator.transform.position = new Vector3(labElevator.transform.position.x, elevatorActualY, labElevator.transform.position.z);
            elevatorActualY += elevatorSpeed * Time.deltaTime;
           
        }
        if (elevatorActualY >= elevatorYTarget && elevatorGoingUp)
        {
            sparks.SetActive(true);
            elevatorClosed.SetActive(false);
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().mass = 100;
            GetComponent<Rigidbody2D>().gravityScale = 14;
            elevatorOn = false;
        }
        if (elevatorOn && onElevator && !elevatorGoingUp)
        {
            elevatorClosed.SetActive(true);
            Player.transform.position = new Vector2(Player.transform.position.x, elevatorActualY - 5.20974f);
            labElevator.transform.position = new Vector3(labElevator.transform.position.x, elevatorActualY, labElevator.transform.position.z);
            elevatorActualY -= elevatorSpeed * Time.deltaTime;
           
        }
        if (elevatorActualY <= elevatorYTarget && !elevatorGoingUp)
        {
            elevatorOn = false;
            elevatorClosed.SetActive(false);
            elevatorActualY = elevatorYTarget;
        }
    }

    void OnTriggerEnter2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            onElevator = true;
        }
    }
    void OnTriggerExit2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            onElevator = false;
        }
    }
}