﻿using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour {

    public float ElevatorY;
    public float ElevatorMinY;
    public float ElevatorYMovement;
    public GameObject ElevatorObj;
    public bool OnElevator;

    private void Update()
    {
        if (OnElevator)
        {
            ElevatorObj.transform.position = new Vector3(ElevatorObj.transform.position.x, ElevatorYMovement, ElevatorObj.transform.position.z);
            ElevatorYMovement += 35 * Time.deltaTime;
        }
        if (!OnElevator)
        {
            ElevatorObj.transform.position = new Vector3(ElevatorObj.transform.position.x, ElevatorYMovement, ElevatorObj.transform.position.z);
           ElevatorYMovement -= 35 * Time.deltaTime;
        }
        if (ElevatorYMovement >= ElevatorY)
        {
            ElevatorYMovement = ElevatorY;
        }
        if (ElevatorYMovement <= ElevatorMinY)
        {
            ElevatorYMovement = ElevatorMinY;
        }
    }

    void OnTriggerEnter2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            OnElevator = true;
        }
    }
    void OnTriggerExit2D(Collider2D info)

    {
        if (info.tag == "Player")
        {
            OnElevator = false;
        }
    }
}
