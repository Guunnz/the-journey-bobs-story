﻿using UnityEngine;
using System.Collections;

public class StatueControl : MonoBehaviour
{
    void Start()
    {
        TouchControl.OnTouched += OnTouched;
    }

    public void OnTouched(GameObject touchedGO)
    {
        if (touchedGO == this.gameObject)
        {
            if (PlayerMode.freezeMode)
            {
                PlayerMode.freezeMode = false;
            }
            else
            {
                PlayerMode.freezeMode = true;
            }   
        }
    }
}

