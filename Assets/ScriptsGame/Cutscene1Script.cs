﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Cutscene1Script : MonoBehaviour {


    public string Opcion1 = "2- JunglaWithGem";
    public string Opcion2 = "2- JunglaWithoutGem";
    public GameObject endLevelFade;
    public bool decision2;


    public void GiveRing()
    {
        endLevelFade.SetActive(true);
        Invoke("LoadScene1", 1.5f);
        if (decision2)
        {
            SteamScript.whichLogro = "Logro39";
            SteamScript.logro = "Logro39";
        }
        if (!decision2)
        {
            SteamScript.whichLogro = "Logro3";
            SteamScript.logro = "Logro3";
        }
    }

    public void DoNotGive()
    {
        if (decision2)
        {
            SteamScript.whichLogro = "Logro38";
            SteamScript.logro = "Logro38";
        }
        if (!decision2)
        {
            SteamScript.whichLogro = "Logro2";
            SteamScript.logro = "Logro2";
        }
        endLevelFade.SetActive(true);
        Invoke("LoadScene2", 1.5f);
    }

    void LoadScene1()
    {
        SceneManager.LoadScene(Opcion1, LoadSceneMode.Single);
    }

    void LoadScene2()
    {
        SceneManager.LoadScene(Opcion2, LoadSceneMode.Single);
    }
}
