﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuOptions : MonoBehaviour
{
    public int deleteData;
    public string sceneToPlay = "";
    public GameObject endLevelFade;
    public GameObject pauseMenu;
    public GameObject BeforeNewGames;
    public int squareUnlocked;
    public GameObject Squarethon;
    public int secretsquare;
    public GameObject squarewrong;
    public int secretTemple;

    void Start()
    {
        squareUnlocked = PlayerPrefs.GetInt("Square");
        sceneToPlay = PlayerPrefs.GetString("LevelToLoad");
    }

    public void LoadGame()
    {
        FindObjectOfType<AudioManager>().Play("NewGame");
        OpenPause.squarethon = false;
        pauseMenu.SetActive(true);
        endLevelFade.SetActive(true);
        Invoke("Load", 1.1f);
    }
    public void BeforeNewGame()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        BeforeNewGames.SetActive(true);
    }
    public void CloseBeforeNewGame()
    {
        FindObjectOfType<AudioManager>().Play("MenuClick");
        BeforeNewGames.SetActive(false);
    }
    public void NewGame()
    {
        FindObjectOfType<AudioManager>().Play("NewGame");
        BeforeNewGames.SetActive(false);
        OpenPause.squarethon = false;
        pauseMenu.SetActive(true);
        SteamScript.whichLogro = "Logro1";
        SteamScript.logro = "Logro1";
        endLevelFade.SetActive(true);
        Invoke("LoadNewGame", 1.1f);
    }
    public void Load()
    {
        SceneManager.LoadScene(sceneToPlay, LoadSceneMode.Single);
    }
        public void LoadSquarethon()
        {
            SceneManager.LoadScene("IsaacInfiniteMenu", LoadSceneMode.Single);
        }
        public void LoadNewGame()
    {
        PlayerPrefs.SetInt("MenuSprite", 0);
        PlayerPrefs.SetInt("torrenteCheck", 0);
        PlayerPrefs.SetFloat("playerXPos", 0);
        PlayerPrefs.SetFloat("playerYPos", 0);
        PlayerPrefs.SetFloat("camXPos", 0);
        PlayerPrefs.SetFloat("camYPos", 0);
        PlayerPrefs.SetInt("onStartup", 0);
        PlayerPrefs.SetString("LevelToLoad", "");
        PlayerPrefs.SetInt("torrenteCheck", 0);
        PlayerPrefs.SetInt("onStartup", 0);
        PlayerPrefs.SetFloat("playerXPos", 0);
        PlayerPrefs.SetFloat("playerYPos", 0);
        PlayerPrefs.SetFloat("camXPos", 0);
        PlayerPrefs.SetFloat("camZPos", 0);
        PlayerPrefs.SetFloat("camYPos", 0);
        PlayerPrefs.SetInt("RockOn", 0);
        PlayerPrefs.SetInt("GEMOff", 0);
        PlayerPrefs.SetInt("NPCoff", 0);
        PlayerPrefs.SetInt("yetiDone", 0);
        PlayerPrefs.SetInt("hugly1Off", 0);
        PlayerPrefs.SetInt("hugly2On", 0);
        PlayerPrefs.SetInt("check1", 0);
        PlayerPrefs.SetInt("check2", 0);
        PlayerPrefs.SetInt("check3", 0);
        SceneManager.LoadScene("Cutscene1", LoadSceneMode.Single);
    }
    public void Quit()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            secretsquare = 1;
        }
        if (Input.GetKey(KeyCode.Q) && secretsquare == 1)
        {
            secretsquare = 2;
        }
        if (Input.GetKey(KeyCode.U) && secretsquare == 2)
        {
            secretsquare = 3;
        }
        if (Input.GetKey(KeyCode.A) && secretsquare == 3)
        {
            secretsquare = 4;
        }
        if (Input.GetKey(KeyCode.R) && secretsquare == 4)
        {
            secretsquare = 5;
        }
        if (Input.GetKey(KeyCode.E) && secretsquare == 5)
        {
            PlayerPrefs.SetInt("Square", 1);
            Squarethon.SetActive(true);
            squarewrong.SetActive(false);
        }

        if (Input.GetKey(KeyCode.T))
        {
            secretTemple = 1;
        }
        if (Input.GetKey(KeyCode.E) && secretTemple == 1)
        {
            secretTemple = 2;
        }
        if (Input.GetKey(KeyCode.M) && secretTemple == 2)
        {
            secretTemple = 3;
        }
        if (Input.GetKey(KeyCode.P) && secretTemple == 3)
        {
            secretTemple = 4;
        }
        if (Input.GetKey(KeyCode.L) && secretTemple == 4)
        {
            secretTemple = 5;
        }
        if (Input.GetKey(KeyCode.E) && secretTemple == 5)
        {
            SceneManager.LoadScene("PuzzleTemple0", LoadSceneMode.Single);
          
        }
        if (squareUnlocked == 1)
        {
            squarewrong.SetActive(false);
            Squarethon.SetActive(true);
        }
        if (Input.GetKey(KeyCode.L))
        {
            deleteData += 1;
        }
        if (deleteData >= 10)
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Data Deleted");
            sceneToPlay = "";
        }
    }
}
