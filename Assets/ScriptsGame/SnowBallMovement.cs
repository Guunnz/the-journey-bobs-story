﻿using UnityEngine;
using System.Collections;

public class SnowBallMovement : MonoBehaviour {

    public float SnowBallSPEED = 40f;
    public bool itstime = true;
    public GameObject Player;
	// Update is called once per frame
	void Update ()

    {
        if (itstime)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(SnowBallSPEED, GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    void OnTriggerEnter2D (Collider2D info)
    {
        if (info.tag == "StopShaking")
        {
            Shake.isShaking = false;
        }
    }
}
