﻿using UnityEngine;
using System.Collections;

public class AGOoT : MonoBehaviour {

    public GameObject Objeto;
    public GameObject ObjetoaDestruir;

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            Objeto.SetActive(true);
            Invoke("Destroying", 0.01f);
        }
    }



    private void Destroying()
    {
        Destroy(this.gameObject);
        Destroy(ObjetoaDestruir);
    }
}
