﻿using UnityEngine;
using System.Collections;

public class ParralaxingDepth : MonoBehaviour {
    public bool BG3;
    public bool BG2;
    public bool BG1;
    public bool FG1;
    public bool FG2;
  
    public Vector3 IMGPosition;


    // Update is called once per frame
       

void Start()
{
    IMGPosition = gameObject.transform.position;
}

    void Update () {



    if (BG1)
        {
            IMGPosition.z = ParrallaxingDepthModif.BG1Distance;
        }
        if (BG2)
        {
            IMGPosition.z = ParrallaxingDepthModif.BG2Distance;
        }
        if (BG3)
        {
            IMGPosition.z = ParrallaxingDepthModif.BG3Distance;
        }
        if (FG1)
        {
            IMGPosition.z = ParrallaxingDepthModif.FG1Distance;
        }
        if (FG2)
        {
            IMGPosition.z = ParrallaxingDepthModif.FG2Distance;
        }
        gameObject.transform.position = IMGPosition;
    }
}
