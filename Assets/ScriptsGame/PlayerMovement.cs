﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    public float speed;
    public float jumpForce;

    private Rigidbody2D rbPlayer;


    // Use this for initialization
    void Awake()
    {
        rbPlayer = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rbPlayer.velocity = new Vector2(speed, rbPlayer.velocity.y);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Vector3 hitDirection = col.collider.transform.position - transform.position;
        hitDirection.z = 0;
        hitDirection.Normalize();

        if (Mathf.Abs(hitDirection.x) > Mathf.Abs(hitDirection.y))
        {
            print("Murio");
        }
    }

    public void Jump()
    {
        if (Mathf.Approximately(rbPlayer.velocity.y, 0))
        {
            rbPlayer.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }
}
