﻿using UnityEngine;
using System.Collections;

public class YetiMinigame : MonoBehaviour {


    public bool iaPick;
    public bool playerPick;
   static public bool StartGame;
    public bool round1;
    public bool round2;
    public bool round3;
    public bool round4;
    public bool round5;
    public bool round6;
    public bool round7;
    public bool round8;
    public bool round9;
    public bool roundcalled;
    public bool roundfinished;
    public bool playerPicksWater;
    public bool playerPicksEarth;
    public bool playerPicksThunder;
    public bool iaPicksWater;
    public bool iaPicksEarth;
    public bool iaPicksThunder;
    public int IAPickOption;
  static  public int IARoundsWonnered;
   static public int playerRoundsWonnered;
    static public bool playerwins;
    static public bool yetiwins;
    public string whichround;
    public GameObject Selections;
    public GameObject AfterYeti;
    public GameObject AfterYetiHugly;
    public GameObject BobScore1;
    public GameObject BobScore2;
    public GameObject BobScore3;
    public GameObject BobScore4;
    public GameObject BobScore5;
    public GameObject YetiScore1;
    public GameObject YetiScore2;
    public GameObject YetiScore3;
    public GameObject YetiScore4;
    public GameObject YetiScore5;
    public GameObject iaWaterPlayerThunder;
    public GameObject iaThunderPlayerEarth;
    public GameObject iaEarthPlayerWater;
    public GameObject iaEarthPlayerThunder;
    public GameObject iaThunderPlayerWater;
    public GameObject iaWaterPlayerEarth;
    public GameObject iaEarthPlayerEarth;
    public GameObject iaThunderPlayerThunder;
    public GameObject iaWaterPlayerWater;
    public GameObject ResultsCanvas;




    // Update is called once per frame
    void Update ()
    {
        if (playerRoundsWonnered == 0)
        {
            BobScore1.SetActive(false);
            BobScore2.SetActive(false);
            BobScore3.SetActive(false);
            BobScore4.SetActive(false);
            BobScore5.SetActive(false);
            YetiScore1.SetActive(false);
            YetiScore2.SetActive(false);
            YetiScore3.SetActive(false);
            YetiScore4.SetActive(false);
            YetiScore5.SetActive(false);
        }
        if (playerRoundsWonnered == 1)
        {
            BobScore1.SetActive(true);
        }
        if (playerRoundsWonnered == 2)
        {
            BobScore1.SetActive(true);
            BobScore2.SetActive(true);
        }
        if (playerRoundsWonnered == 3)
        {
            BobScore1.SetActive(true);
            BobScore2.SetActive(true);
            BobScore3.SetActive(true);
        }
        if (playerRoundsWonnered == 4)
        {
            BobScore1.SetActive(true);
            BobScore2.SetActive(true);
            BobScore3.SetActive(true);
            BobScore4.SetActive(true);
        }
        if (IARoundsWonnered == 1)
        {
            YetiScore1.SetActive(true);
        }
        if (IARoundsWonnered == 2)
        {
            YetiScore1.SetActive(true);
            YetiScore2.SetActive(true);
        }
        if (IARoundsWonnered == 3)
        {
            YetiScore1.SetActive(true);
            YetiScore2.SetActive(true);
            YetiScore3.SetActive(true);
        }
        if (IARoundsWonnered == 4)
        {

            YetiScore1.SetActive(true);
            YetiScore2.SetActive(true);
            YetiScore3.SetActive(true);
            YetiScore4.SetActive(true);
        }
        if (playerRoundsWonnered == 5)
        {
            ResultsCanvas.SetActive(false);
            SteamScript.whichLogro = "Logro12";
            SteamScript.logro = "Logro12";
            RemoveResults();
            RemoveImages();
            Invoke("PlayerWinsGame", 0.1f);
        }
        if (IARoundsWonnered == 5)
        {
            ResultsCanvas.SetActive(false);
            RemoveResults();
            RemoveImages();
            Invoke("IAWinsGame", 0.1f);
        }
        if (YetiDialogueWGem.resetgame == true)
        {
            CompleteReset();
            YetiDialogueWGem.resetgame = false;
        }
        if (!StartGame)
        {
            Selections.SetActive(false);
        }
      if (StartGame)
      {
            if (round1 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round1";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round1 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round1 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round2 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round2";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round2 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round2 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round3 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round3";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round3 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round3 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round4 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round4";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round4 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round4 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round5 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round5";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round5 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round5 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round6 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round6";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round6 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round6 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round7 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round7";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round7 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round7 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round8 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round8";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round8 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round8 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            if (round9 && !playerPick && !iaPick && playerRoundsWonnered != 5 && IARoundsWonnered != 5)
            {
                ResultsCanvas.SetActive(false);
                iaWaterPlayerThunder.SetActive(false);
                iaThunderPlayerEarth.SetActive(false);
                iaEarthPlayerWater.SetActive(false);
                iaEarthPlayerThunder.SetActive(false);
                iaThunderPlayerWater.SetActive(false);
                iaWaterPlayerEarth.SetActive(false);
                iaEarthPlayerEarth.SetActive(false);
                iaThunderPlayerThunder.SetActive(false);
                iaWaterPlayerWater.SetActive(false);
                whichround = "round9";
                roundfinished = false;
                Selections.SetActive(true);
            }
            if (round9 && playerPick && !iaPick)
            {
                Selections.SetActive(false);
                //Grafica De Lo Elegido
                iaPick = true;
                IAPickOption = Random.Range(1, 4);
                IASelection();
            }
            if (round9 && playerPick && iaPick && !roundfinished)
            {
                roundfinished = true;
                roundcalled = false;
                RoundResult();
            }
            /////////////////////////////////
            /////////////////////////////////

        }


    }
    void RoundResult()
    {
        if (iaPicksWater && playerPicksThunder && IARoundsWonnered != 5 && playerRoundsWonnered !=5)
        {
            ResultsCanvas.SetActive(true);
            iaWaterPlayerThunder.SetActive(true);
            Invoke("PlayerWins", 2);
        }
        if (iaPicksThunder && playerPicksEarth && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            TierraAchievement.tierra += 1;
            ResultsCanvas.SetActive(true);
            iaThunderPlayerEarth.SetActive(true);
            Invoke("PlayerWins", 2);
        }
        if (iaPicksEarth && playerPicksWater && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaEarthPlayerWater.SetActive(true);
            Invoke("PlayerWins", 2);
        }
        if (iaPicksEarth && playerPicksThunder && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaEarthPlayerThunder.SetActive(true);
            Invoke("IAWins", 2);
        }
        if (iaPicksThunder && playerPicksWater && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaThunderPlayerWater.SetActive(true);
            Invoke("IAWins", 2);
        }
        if (iaPicksWater && playerPicksEarth && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaWaterPlayerEarth.SetActive(true);
            Invoke("IAWins", 2);
        }
        if (iaPicksEarth && playerPicksEarth && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaEarthPlayerEarth.SetActive(true);
            Invoke("Tie", 2);
        }
        if (iaPicksThunder && playerPicksThunder && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaThunderPlayerThunder.SetActive(true);
            Invoke("Tie", 2);
        }
        if (iaPicksWater && playerPicksWater && IARoundsWonnered != 5 && playerRoundsWonnered != 5)
        {
            ResultsCanvas.SetActive(true);
            iaWaterPlayerWater.SetActive(true);
            Invoke("Tie", 2);
        }
    }
    void Reset()
    {
        playerPick = false;
        playerPicksEarth = false;
        playerPicksThunder = false;
        playerPicksWater = false;
        iaPick = false;
        iaPicksEarth = false;
        iaPicksThunder = false;
        iaPicksWater = false;
    }
    void CompleteReset()
    {
        TierraAchievement.tierra = 0;
        playerPick = false;
        playerPicksEarth = false;
        playerPicksThunder = false;
        playerPicksWater = false;
        iaPick = false;
        iaPicksEarth = false;
        iaPicksThunder = false;
        iaPicksWater = false;
        playerRoundsWonnered = 0;
        IARoundsWonnered = 0;
        round1 = true;
        round2 = false;
        round3 = false;
        round4 = false;
        round5 = false;
        round6 = false;
        round7 = false;
        round8 = false;
        round9 = false;
    }
    void Tie()
    {
            ResultsCanvas.SetActive(false);
            Debug.Log("Tie");
        Reset();
        round1 = true;
        Debug.Log("Player Score " + playerRoundsWonnered.ToString());
        Debug.Log("Ia Score " + IARoundsWonnered.ToString());
    }
    void IAWins()
    {
        FindObjectOfType<AudioManager>().Play("LoseElements");
        ResultsCanvas.SetActive(false);
        Debug.Log("Yeti Wins This Round");
        Reset();
        IARoundsWonnered += 1;
        Debug.Log("Player Score " + playerRoundsWonnered.ToString());
        Debug.Log("Ia Score " + IARoundsWonnered.ToString());
        if (whichround == "round1" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round1 = false;
            round2 = true;
        }
        ///////////
        if (whichround == "round2" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round2 = false;
            round3 = true;
        }
        ///////////
        if (whichround == "round3" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round3 = false;
            round4 = true;
        }
        ///////////
        if (whichround == "round4" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round4 = false;
            round5 = true;
        }
        ///////////
        if (whichround == "round5" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round5 = false;
            round6 = true;
        }
        ///////////
        if (whichround == "round6" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round6 = false;
            round7 = true;
        }
        ///////////
        if (whichround == "round7" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round7 = false;
            round8 = true;
        }
        ///////////
        if (whichround == "round8" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round8 = false;
            round9 = true;
        }
        ///////////
        if (whichround == "round9" && !roundcalled && StartGame)
        {
            roundcalled = true;
            WhoWins();
        }
        ///////////
    }
    void PlayerWins()
    {
        FindObjectOfType<AudioManager>().Play("WinElements");
        ResultsCanvas.SetActive(false);
        Debug.Log("Player Wins This Round");
        playerRoundsWonnered += 1;
        Reset();
        Debug.Log("Player Score " + playerRoundsWonnered.ToString());
        Debug.Log("Yeti Score " + IARoundsWonnered.ToString());
        if (whichround == "round1" && !roundcalled)
        {
            roundcalled = true;
            round1 = false;
            round2 = true;
        }
        ///////////
        if (whichround == "round2" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round2 = false;
            round3 = true;
        }
        ///////////
        if (whichround == "round3" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round3 = false;
            round4 = true;
        }
        ///////////
        if (whichround == "round4" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round4 = false;
            round5 = true;
        }
        ///////////
        if (whichround == "round5" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round5 = false;
            round6 = true;
        }
        ///////////
        if (whichround == "round6" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round6 = false;
            round7 = true;
        }
        ///////////
        if (whichround == "round7" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round7 = false;
            round8 = true;
        }
        ///////////
        if (whichround == "round8" && !roundcalled && StartGame)
        {
            roundcalled = true;
            round8 = false;
            round9 = true;
        }
        ///////////
        if (whichround == "round9" && !roundcalled && StartGame)
        {
            roundcalled = true;
            WhoWins();
        }
        ///////////
    }

    void WhoWins()
    {
        if (playerRoundsWonnered == 5)
        {
            PlayerWinsGame();
        }
        if (IARoundsWonnered == 5)
        {
            IAWinsGame();
        }
    }
    void PlayerWinsGame()
    {
            AfterYeti.SetActive(true);
            AfterYetiHugly.GetComponent<CircleCollider2D>().enabled = true;
            AfterYetiHugly.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
            round1 = false;
            round2 = false;
            round3 = false;
            round4 = false;
            round5 = false;
            round6 = false;
            round7 = false;
            round8 = false;
            round9 = false;
            StartGame = false;
            Debug.Log("Player wins the match");
            playerwins = true;
            YetiDialogueWGem.dialogue = 7;
            playerRoundsWonnered = 0;
            StartGame = false;
            //Static value to continue
    }
    void IAWinsGame()
    {
        Debug.Log("Ia wins the match");
        YetiDialogueWGem.dialogue = 7;
        IARoundsWonnered = 0;
        yetiwins = true;
        StartGame = false;
        //Static value to ask reset
    }
    void IASelection()
    {
        if (IAPickOption == 1)
        {
            Debug.Log("IA Picks Water");
            IAWater();
        }
        if (IAPickOption == 2)
        {
            Debug.Log("IA Picks Earth");
            IAEarth();
        }
        if (IAPickOption == 3)
        {
            Debug.Log("IA Picks Thunder");
            IAThunder();
        }
    }
    void RemoveImages()
    {
        BobScore1.SetActive(false);
        BobScore2.SetActive(false);
        BobScore3.SetActive(false);
        BobScore4.SetActive(false);
        BobScore5.SetActive(false);
        YetiScore1.SetActive(false);
        YetiScore2.SetActive(false);
        YetiScore3.SetActive(false);
        YetiScore4.SetActive(false);
        YetiScore5.SetActive(false);
    }
    void RemoveResults()
    {
        ResultsCanvas.SetActive(false);
    }
    void IAWater()
    {
        iaPicksWater = true;
    }
    void IAEarth()
    {
        iaPicksEarth = true;
    }

    void IAThunder()
    {
        iaPicksThunder = true;
    }


    public void Water()
    {
        Debug.Log("Player Picks Water");
        playerPicksWater = true;
        playerPick = true;
    }
    public void Earth()
    {
        Debug.Log("Player Picks Earth");
        playerPicksEarth = true;
        playerPick = true;
    }
    public void Thunder()
    {
        Debug.Log("Player Picks Thunder");
        playerPicksThunder = true;
        playerPick = true;
    }
}
