﻿using UnityEngine;

public class UserControl2D : MonoBehaviour
{


    public bool Jetpack;
    public bool MagicGauntlet;
    public bool Suit;
    private bool GauntletAvailable = true;
    static public bool JetpackWaterRecharge = false;
    public float JetpackFuel = 10;
    public float GauntletDelay = 3;
    public int RechargeNumber;
    public GameObject JetpackFuelGraphics;
    public GameObject TorrentGraphics;
    public GameObject JetpackGraphics;
    public GameObject CanvasJetpackFuelGraphics;
    public GameObject Gauntlet;
    public GameObject FreezeTrigger;
    public GameObject TorrentMode;
    public ParticleSystem SnowControl;
    public bool torrente = false;
    public bool RechargeControl;
    public bool activado;
    public ParticleSystem CamFuelps;
    public ParticleSystem JetFuelps;
    public float jetpackforce = 35;
    public float suitForce;
    public float SuitFuel;
    static public bool storm;
    public bool SuitRecharge;
    private Animator anim;
    public bool gauntlet = false;
    public bool RocketBoots;
    public bool playingsuitaudio;
    public bool playingjetaudio;
    public bool playinggauntletaudio;



    private void Start()
    {
        anim.SetBool("OnBox", false);
    }

    void Update()
    {
        if (anim == null)
        {
            anim = GetComponent<Animator>();
        }
        anim.SetBool("Gauntlet", gauntlet);
            if (storm == true)
            {
                torrente = true;
            }
            if (storm == false)
            {
                torrente = false;
            }
            if (Suit)
        {
                if (Input.GetKey(InputManager.LShift) && SuitFuel <= 0.6f && SuitRecharge == false && SuitFuel > 0.1f)
            {
               
                RocketBoots = true;
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, suitForce);
                SuitFuel -= Time.deltaTime * 1.5f;
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                if (!playingsuitaudio)
                {
                    playingsuitaudio = true;
                    FindObjectOfType<AudioManager>().Play("RocketBoots");
                }
            }
                  if (SuitFuel >= 0.5f)
                   {
                     SuitFuel = 0.5f;
                   SuitRecharge = false;
                   }
                 
                  if (SuitFuel <= 0.1f)
                   {
                FindObjectOfType<AudioManager>().Stop("RocketBoots");
                playingsuitaudio = false;
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
                TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = false;
                SuitRecharge = true;
                   }
                  if (SuitFuel < 0.1f)
            {
                playingsuitaudio = false;
                FindObjectOfType<AudioManager>().Stop("RocketBoots");
                SuitFuel = 0.1f;
            }
                  if (SuitRecharge == true)
                   {
                     SuitFuel += Time.deltaTime * 0.4f;
                   }
            if (Input.GetKeyUp(InputManager.LShift))
            {
                SuitFuel = 0.1f;
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
                TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = false;
            }

        }
        //JetPack Scripting.
        if (Jetpack)
        {
            if (Input.GetKey(InputManager.LShift) && JetpackFuel > RechargeNumber && !torrente)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jetpackforce);
                JetpackFuel -= Time.deltaTime * 1.5f;
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed -= Time.deltaTime * 4.65f ;
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed -= Time.deltaTime * 90;
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                if (!playingjetaudio)
                {
                    FindObjectOfType<AudioManager>().Play("Jetpack");
                    playingjetaudio = true;
                }
            }
            if (JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed < 0)
            {
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            }
            if (CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed < 0)
            {
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            }
            if (Input.GetKey(InputManager.LShift) && JetpackFuel >= 3.9f && torrente)
            {
                TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 75);
                JetpackFuel -= 4.1f;
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
                Invoke("RemoveTorrentAnimation", 0.3f);
                if (!playingjetaudio)
                {
                    playingjetaudio = true;
                    FindObjectOfType<AudioManager>().Play("Jetpack");
                }
            }
            if (Input.GetKeyUp(InputManager.LShift))
            {
                playingjetaudio = false;
                FindObjectOfType<AudioManager>().Stop("Jetpack");
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
            }
            if (RechargeControl == true && torrente)
            {
                FuelRecharge();
            }
            if (RechargeControl == true)
            {
                FuelRecharge();
            }
            if (JetpackFuel >= 3.9f)
            {
                JetpackWaterRecharge = false;
            }
            if (JetpackFuel <= 0)
            {
                playingjetaudio = false;
                FindObjectOfType<AudioManager>().Stop("Jetpack");
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
                activado = false;
                JetpackFuel = 0;
                RechargeControl = true;
                RechargeNumber = 1;
            }
            if (JetpackFuel >= 0.9f)
            {
                RechargeControl = false;
                RechargeNumber = 0;
            }
            if (torrente)
            {
                SnowControl.startColor = new Color(255F, 255F, 255F, 0.02f);
                Torrent();
                var colorBySpeed = CamFuelps.colorBySpeed;
                var colorBySpeed2 = JetFuelps.colorBySpeed;

                colorBySpeed.enabled = false;
                colorBySpeed2.enabled = false;
                TorrentMode.SetActive(true);

            }
            if (torrente && JetpackFuel <= 4)
            {
                TorrentRecharge();
            }
            if (!torrente)
            {
                var colorBySpeed = CamFuelps.colorBySpeed;
                var colorBySpeed2 = JetFuelps.colorBySpeed;
                colorBySpeed.enabled = true;
                colorBySpeed2.enabled = true;
                TorrentMode.SetActive(false);
            }
            //JetPack Scripting.
            //
            //
            //Magic Gauntlet Scripting.
            
            }
        if (MagicGauntlet)
        {
            if (Input.GetKey(InputManager.LControl) && GauntletAvailable)
            {
                gauntlet = true;
                SteamScript.whichLogro = "Logro20";
                SteamScript.logro = "Logro20";
                Gauntlet.SetActive(true);
                GauntletAvailable = false;
                Invoke("DelayControl", GauntletDelay);
                Invoke("TriggerControl", 1);
                if (!playinggauntletaudio)
                {
                    playinggauntletaudio = true;
                    FindObjectOfType<AudioManager>().Play("GauntletFreeze");
                }
            }
            if (JetpackWaterRecharge == true && JetpackFuel <= 3.9f)
            {
                WaterRecharge();
            }
        }
    }
    //Magic Gauntlet Scripting.


    public void RemoveTorrentAnimation()
    {
        playingjetaudio = false;
        FindObjectOfType<AudioManager>().Stop("Jetpack");
        TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = false;
    }
    public void FuelRecharge()
    {
        JetpackFuel += Time.deltaTime * 0.8f;
        JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 2.4f;
        CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 48;
    }
    public void TorrentRecharge()
    {
        JetpackFuel += Time.deltaTime * 1.6f;
        JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 4.8f;
        CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 96;
    }

    public void WaterRecharge()
    {
        JetpackFuel += Time.deltaTime * 1.5f;
        JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 4.5f;
        CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 90;
    }

    public void DelayControl()
    {
        playinggauntletaudio = false;
        gauntlet = false;
        Gauntlet.SetActive(false);
        GauntletAvailable = true;
        FreezeTrigger.SetActive(false);
    }
    public void TriggerControl()
    {
        FreezeTrigger.SetActive(true);
    }
    public void Torrent()
    {
        storm = true;
        torrente = true;
    }

    public void DeTorrent()
    {
        storm = false;
        torrente = false;
        SnowControl.startColor = new Color(255, 255, 255, 0f);
    }

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "TorrentTrigger" && !torrente)
        {
            Invoke("Torrent", 2);
            SnowControl.startColor = new Color(255F, 255F, 255F, 0.02f);
        }
        if (info.tag == "DeTorrentTrigger" && torrente)
        {
            JetpackFuel = 0;
            JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            SnowControl.startColor = new Color(255, 255, 255, 0f);
            Invoke("DeTorrent", 1);
        }
    }
}

    