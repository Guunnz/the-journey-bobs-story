﻿using UnityEngine;
public class ParticleFreeze : MonoBehaviour
{

    ParticleSystem.ShapeModule pShape;

    public ParticleSystem particles;
    public bool IsFrozen = false;
    static public bool AOETrue = false;
    public bool FreezeAnim = false;
    public float Unfreeze;
    public GameObject FreezeCollider;
    public GameObject CascadeRecharge;
    private float delay = 1.75f;
    public bool isWaterFrozen;
    public bool reset;
    public Transform AS;
    private bool sonidofreeze = false;


    void Start()
    {
        Invoke("sonidofreezeON", 5);
        AS = transform.Find("WaterAOEReset");
        pShape = particles.shape;
    }


    // Update is called once per frame
    void Update()
    {
        reset = MiscControl.reset;
        if (reset == true && isWaterFrozen == true)
        {
            IsFrozen = true;
            delay = 1.75f;
            Invoke("IsFrozenFalse", 0.1f);
        }
        if (IsFrozen)
        {
            if (sonidofreeze)
            {
                FindObjectOfType<AudioManager>().Play("WaterFreeze");
            }
            AS.gameObject.GetComponent<AudioSource>().Stop();
        }
        if (reset == true && isWaterFrozen == false)
        {
            FreezeAnim = false;
            DeFreeze();
            Invoke("Delay", 2);
        }
        if (Unfreeze >= 1)
        {
            Unfreeze = 1;
        }

        if (IsFrozen && !FreezeAnim && delay >= 1.7f)
        {
           
            Invoke("IsFrozenFalse", 0.1f);
            delay = 1.75f;
            FreezeAnim = true;
            FreezeCollider.SetActive(true);
            CascadeRecharge.SetActive(false);
            pShape.shapeType = ParticleSystemShapeType.ConeShell;
            particles.Stop();
            CompleteFreeze();
        }
        if (FreezeAnim)
        {
            delay -= Time.deltaTime * 0.5f;
        }
        if (IsFrozen && delay <= 0)
        {
            FreezeAnim = false;
            DeFreeze();
            Invoke("Delay", 2);
        }
        if (Unfreeze <= 0)
        {
            Unfreeze = 0;
        }
        if (Unfreeze >= 1)
        {
            Unfreeze = 1.05f;
        }
    }
    void sonidofreezeON()
    {
        sonidofreeze = true;
    }

    void Delay()
    {
        delay = 1.75f;
    }

    void CompleteFreeze()
    {
        if (sonidofreeze)
        {
            FindObjectOfType<AudioManager>().Play("WaterFreeze");
        }
        AS.gameObject.GetComponent<AudioSource>().Stop();
        particles.Play();
        particles.playbackSpeed = 0;
    }
    void DeFreeze()
    {
        FindObjectOfType<AudioManager>().Stop("WaterFreeze");
        AS.gameObject.GetComponent<AudioSource>().Play();
        FreezeCollider.SetActive(false);
        CascadeRecharge.SetActive(true);
        pShape.shapeType = ParticleSystemShapeType.Cone;
        FinishDeFreeze();
    }
    void FinishDeFreeze()
    {
        FindObjectOfType<AudioManager>().Stop("WaterFreeze");
        IsFrozen = false;
        FreezeAnim = false;
        particles.playbackSpeed = 1;
    }

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "DefreezeTemple" && FreezeAnim == true)
        {
            delay = 0;
            IsFrozen = true;
        }
        if (info.tag == "Gauntlet")
        {
            IsFrozen = true;
        }
    }
    void IsFrozenFalse()
    {
        IsFrozen = false;
    }
}


