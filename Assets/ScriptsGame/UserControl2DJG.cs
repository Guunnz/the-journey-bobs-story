﻿using UnityEngine;

public class UserControl2DJG : MonoBehaviour
{


    public bool Jetpack;
    public bool MagicGauntlet;
    private bool GauntletAvailable = true;
    static public bool JetpackWaterRecharge = false;
    public float JetpackFuel = 10;
    public float GauntletDelay = 3;
    public int RechargeNumber;
    public GameObject JetpackFuelGraphics;
    public GameObject TorrentGraphics;
    public GameObject JetpackGraphics;
    public GameObject CanvasJetpackFuelGraphics;
    public GameObject Gauntlet;
    public GameObject FreezeTrigger;
    public GameObject TorrentMode;
    public ParticleSystem SnowControl;
    public bool torrente = false;
    public bool RechargeControl;
    public bool activado;
    public ParticleSystem CamFuelps;
    public ParticleSystem JetFuelps;
    public GameObject JetpackObject;
    static public bool storm;
    private Animator anim;
    public bool playingaudio;



    private void Start()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (JetpackWaterRecharge == true && JetpackFuel <= 3.8f)
        {
            WaterRecharge();
        }
        if (Platformer2DJG.jetOnWater && JetpackFuel <= 4)
        {
            JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
            FuelRecharge();
        }
            if (storm == true)
            {
                torrente = true;
            }
            if (storm == false)
            {
                torrente = false;
            }
        //JetPack Scripting.
        if (Jetpack && Platformer2DJG.jetOnWater == false)
        {
            if (Input.GetKey(InputManager.LShift) && JetpackFuel > RechargeNumber && !torrente && !Platformer2DJG.inmovible)
            {
                SteamScript.whichLogro = "Logro7";
                SteamScript.logro = "Logro7";
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 30f);
                JetpackFuel -= Time.deltaTime * 1.5f;
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed -= Time.deltaTime * 4.65f ;
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed -= Time.deltaTime * 90;
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                if (!playingaudio)
                {
                    FindObjectOfType<AudioManager>().Play("Jetpack");
                    playingaudio = true;
                }
            }
            if (JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed < 0)
            {
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            }
            if (CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed < 0)
            {
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            }
            if (Input.GetKey(InputManager.LShift) && JetpackFuel >= 3.9f && torrente)
            {
                TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = true;
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 75);
                JetpackFuel -= 4.1f;
                JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
                CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
                Invoke("RemoveTorrentAnimation", 0.3f);
            }
            if (Input.GetKeyUp(InputManager.LShift))
            {
                playingaudio = false;
                FindObjectOfType<AudioManager>().Stop("Jetpack");
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
            }
            if (RechargeControl == true && torrente)
            {
                FuelRecharge();
            }
            if (RechargeControl == true)
            {
                FuelRecharge();
            }
            if (JetpackFuel >= 3.9f)
            {
                JetpackWaterRecharge = false;
            }
            if (JetpackFuel <= 0)
            {
                playingaudio = false;
                FindObjectOfType<AudioManager>().Stop("Jetpack");
                JetpackGraphics.GetComponent<ParticleSystem>().enableEmission = false;
                activado = false;
                JetpackFuel = 0;
                RechargeControl = true;
                RechargeNumber = 1;
            }
            if (JetpackFuel >= 0.9f)
            {
                RechargeControl = false;
                RechargeNumber = 0;
            }
            if (torrente)
            {
                SnowControl.startColor = new Color(255F, 255F, 255F, 0.02f);
                Torrent();
                var colorBySpeed = CamFuelps.colorBySpeed;
                var colorBySpeed2 = JetFuelps.colorBySpeed;

                colorBySpeed.enabled = false;
                colorBySpeed2.enabled = false;
                TorrentMode.SetActive(true);

            }
            if (torrente && JetpackFuel <= 4)
            {
                FuelRecharge();
            }
            if (!torrente)
            {
                var colorBySpeed = CamFuelps.colorBySpeed;
                var colorBySpeed2 = JetFuelps.colorBySpeed;
                colorBySpeed.enabled = true;
                colorBySpeed2.enabled = true;
                TorrentMode.SetActive(false);
            }
            //JetPack Scripting.
            //
            //
            //Magic Gauntlet Scripting.
            if (MagicGauntlet)
            {
                if (Input.GetKey(InputManager.LControl) && GauntletAvailable)
                {
                    Gauntlet.SetActive(true);
                    GauntletAvailable = false;
                    Invoke("DelayControl", GauntletDelay);
                    Invoke("TriggerControl", 1);
                }
            }
        }
    }
    //Magic Gauntlet Scripting.


    public void RemoveTorrentAnimation()
    {
        TorrentGraphics.GetComponent<ParticleSystem>().enableEmission = false;
    }
    public void FuelRecharge()
    {
        JetpackFuel += Time.deltaTime * 0.8f;
        JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 2.4f;
        CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 48;
    }

    public void WaterRecharge()
    {
        JetpackFuel += Time.deltaTime * 1.5f;
        JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 4.5f;
        CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed += Time.deltaTime * 90;
    }

    public void DelayControl()
    {
        Gauntlet.SetActive(false);
        GauntletAvailable = true;
        FreezeTrigger.SetActive(false);
    }
    public void TriggerControl()
    {
        FreezeTrigger.SetActive(true);
    }
    public void Torrent()
    {
        storm = true;
        torrente = true;
    }

    public void DeTorrent()
    {
        storm = false;
        torrente = false;
        SnowControl.startColor = new Color(255, 255, 255, 0f);
    }

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "TorrentTrigger" && !torrente)
        {
            Invoke("Torrent", 2);
            SnowControl.startColor = new Color(255F, 255F, 255F, 0.02f);
        }
        if (info.tag == "DeTorrentTrigger" && torrente)
        {
            JetpackFuel = 0;
            JetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            CanvasJetpackFuelGraphics.GetComponent<ParticleSystem>().startSpeed = 0;
            SnowControl.startColor = new Color(255, 255, 255, 0f);
            Invoke("DeTorrent", 1);
        }
        if (info.tag == "JetpackPickup")
        {
            Destroy(info.gameObject);
            JetpackObject.SetActive(true);
        }
    }
}

    