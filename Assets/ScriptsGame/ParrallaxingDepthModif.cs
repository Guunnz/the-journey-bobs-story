﻿using UnityEngine;
using System.Collections;

public class ParrallaxingDepthModif : MonoBehaviour {

    // Use this for initialization
    public float realBG3Distance;
    public float realBG2Distance;
    public float realBG1Distance;
    public float realFG1Distance;
    public float realFG2Distance;

    static public float BG3Distance;
    static public float BG2Distance;
    static public float BG1Distance;
    static public float FG1Distance;
    static public float FG2Distance;

    // Update is called once per frame
    void Update ()


    {
        BG3Distance = realBG3Distance;
        BG2Distance = realBG2Distance;
        BG1Distance = realBG1Distance;
        FG1Distance = realFG1Distance;
        FG2Distance = realFG2Distance;
    }
}
