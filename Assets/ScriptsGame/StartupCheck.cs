﻿using UnityEngine;
using System.Collections;

public class StartupCheck : MonoBehaviour {

    static public int onStartup = 0;
	
	// Update is called once per frame
	void Start ()
    {
        onStartup = PlayerPrefs.GetInt("onStartup");
    }
}
