﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timerText;
    public float tiem;
    static public bool isOn = true;
    public float startTime;

    private void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
     
        tiem = Time.time - startTime;


            PlayerPrefs.SetFloat("Time", tiem);
            float t = Time.time - startTime;
            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f2");
            timerText.text = minutes + ":" + seconds;

        if (IsaacController.isaacHP <= 0)
        {
            GetComponent<Timer>().enabled = false;
        }
}
}
