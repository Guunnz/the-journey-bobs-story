﻿using UnityEngine;
using System.Collections;

public class FreezeScript : MonoBehaviour
{
    public enum State
    {
        Normal,
        Frozen
    }

    public State currentState = State.Normal;

    private GameObject player;
    private Collider2D playerCollider;
    private Collider2D objectCollider;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        
        playerCollider = player.GetComponent<Collider2D>();
        objectCollider = GetComponent<Collider2D>();
    }


    void Start()
    {
        TouchControl.OnTouched += OnTouched;

        if (currentState == State.Normal)
        {
            GetComponent<SpriteRenderer>().color = Color.cyan;
            Physics2D.IgnoreCollision(playerCollider, objectCollider, true);
        }
        else if (currentState == State.Frozen)
        {
            GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }


    void OnDestroy()
    {
        TouchControl.OnTouched -= OnTouched;
    }

    private void OnTouched(GameObject touchedGO)
    {
        if (touchedGO == this.gameObject)
        {
            switch (currentState)
            {
                case State.Normal:
                    if (PlayerMode.freezeMode)
                    {
                        Physics2D.IgnoreCollision(playerCollider, objectCollider, false);
                        GetComponent<SpriteRenderer>().color = Color.blue;
                        currentState = State.Frozen;
                    }
                    break;
                case State.Frozen:
                    if (!PlayerMode.freezeMode)
                    {
                        Physics2D.IgnoreCollision(playerCollider, objectCollider, true);
                        GetComponent<SpriteRenderer>().color = Color.cyan;
                        currentState = State.Normal;
                    }
                    break;
            }
        }
    }
}

