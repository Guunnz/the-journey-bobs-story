﻿using UnityEngine;
using System.Collections;

public class TrineoController : MonoBehaviour {

    // Use this for initialization
    // Update is called once per frame
    [SerializeField]
    private LayerMask whatIsGround; //
    private float groundedRadius = 2.5f; // 
    private bool grounded = false; // 
    private Transform groundCheck; //
    public Transform player;
    public float rotation;

    private void Awake()
    {
        // Setting up references.
        groundCheck = transform.Find("groundCheck");
    }

    void Update ()

    {
        rotation = gameObject.transform.rotation.z * 100;
        if (rotation <= -90)
        {
            SteamScript.whichLogro = "Logro19";
            SteamScript.logro = "Logro19";
            Debug.Log("gotit");
        }
        if (Input.GetAxisRaw("Horizontal") > 0 && !grounded)
        {
           this.gameObject.transform.Rotate(Vector3.back * 3);
        }
        if (Input.GetAxisRaw("Horizontal") < 0 && !grounded)
        {
            this.gameObject.transform.Rotate(Vector3.forward * 4);
        }
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
    }
}
