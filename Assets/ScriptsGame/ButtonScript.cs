﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

    public float buttonY;
    public float buttonYMovement;
    public GameObject buttonToTouch;
    public bool buttonTouched;
    public float elevateSpeed = 100;
    public bool goingDown;
    private float yValue;
    private float syValue;
    private float syMovementValue;
    public bool TwoPressButton;
    public bool IFDoor;
    public bool IFPlatform;
    private bool played;
    public bool boxin;
    public bool playerin;

    private void Start()
    {
        syMovementValue = buttonYMovement;
        syValue = transform.localPosition.y;
        yValue = transform.localPosition.y - 0.3f;
    }
    private void Update()
    {
        if (boxin)
        {
            buttonTouched = true;
        }
        if (playerin)
        {
            buttonTouched = true;
        }
        if (!playerin && !boxin)
        {
            buttonTouched = false;
        }
        if (buttonTouched && !goingDown && buttonYMovement <= buttonY)
        {
            buttonToTouch.transform.position = new Vector3(buttonToTouch.transform.position.x, buttonYMovement, buttonToTouch.transform.position.z);
            buttonYMovement += elevateSpeed * Time.deltaTime;
        }
        if (buttonYMovement >= buttonY && !goingDown && !TwoPressButton)
        {
            buttonTouched = false;
            buttonYMovement = buttonY;
        }
        if (buttonTouched && goingDown)
        {
            buttonToTouch.transform.position = new Vector3(buttonToTouch.transform.position.x, buttonYMovement, buttonToTouch.transform.position.z);
            buttonYMovement -= elevateSpeed * Time.deltaTime;
        }
        if (buttonYMovement <= buttonY && goingDown)
        {
            buttonTouched = false;
            buttonYMovement = buttonY;
        }
        if (TwoPressButton && buttonTouched == false && buttonYMovement >= syMovementValue)
        {
            buttonToTouch.transform.position = new Vector3(buttonToTouch.transform.position.x, buttonYMovement, buttonToTouch.transform.position.z);
            buttonYMovement -= elevateSpeed * Time.deltaTime;
        }
    }

    void OnTriggerEnter2D (Collider2D info)

    {
	    if (info.tag == "Player")
        {
           
            FindObjectOfType<AudioManager>().Play("PressurePlate");
            gameObject.transform.localPosition = new Vector2(transform.localPosition.x, yValue);
            buttonTouched = true;
            playerin = true;
            if (IFDoor && !played)
            {
                played = true;
                FindObjectOfType<AudioManager>().Play("LabDoor");
            }
        }
        if (info.tag == "Box")
        {
            boxin = true;
            FindObjectOfType<AudioManager>().Play("PressurePlate");
            gameObject.transform.localPosition = new Vector2(transform.localPosition.x, yValue);
            buttonTouched = true;
            if (IFDoor && !played)
            {
                played = true;
                FindObjectOfType<AudioManager>().Play("LabDoor");
            }
            if (IFPlatform)
            {
                FindObjectOfType<AudioManager>().Play("PlatformUP");
            }
        }
    }
    private void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player" && TwoPressButton)
        {
            playerin = false;
            buttonTouched = false;
            gameObject.transform.localPosition = new Vector2(transform.localPosition.x, syValue);
        }
        if (info.tag == "Box" && TwoPressButton)
        {
            boxin = false;
            buttonTouched = false;
            gameObject.transform.localPosition = new Vector2(transform.localPosition.x, syValue);
        }
    }
}
