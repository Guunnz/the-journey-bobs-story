﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour
{

   
    public GameObject cam;
    public string ThisSceneIs;
    public GameObject Saving;
    public bool stormcheck;
    public bool saved = false;

    void Start()
    {
        if (stormcheck == true)
        {
            UserControl2D.storm = true;
        }
        if (stormcheck == false)
        {
            UserControl2D.storm = false;
        }
            saved = false;
        }
    



    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player" && saved == false)
        {
            StartupCheck.onStartup = 0;
            Saving.SetActive(true);
            PlayerPrefs.SetString("LevelToLoad", ThisSceneIs);
            MiscControl.camXPos = cam.transform.position.x;
            MiscControl.camYPos = cam.gameObject.transform.position.y;
            MiscControl.playerXPos = this.gameObject.transform.position.x;
            MiscControl.playerYPos = this.gameObject.transform.position.y;
            PlayerPrefs.SetInt("torrenteCheck", MiscControl.torrenteCheck);
            PlayerPrefs.SetFloat("playerXPos", MiscControl.playerXPos);
            PlayerPrefs.SetFloat("playerYPos", MiscControl.playerYPos);
            PlayerPrefs.SetFloat("camXPos", MiscControl.camXPos);
            PlayerPrefs.SetFloat("camYPos", MiscControl.camYPos);
            PlayerPrefs.SetInt("onStartup", StartupCheck.onStartup);
            Invoke("CheckpointStats", 0.1f);
            Invoke("SavingFade", 2.07f);
            saved = true;
            PlayerPrefs.Save();
        }
    }

    void SavingFade()
    {
        Saving.SetActive(false);
    }

    void CheckpointStats()
    {
        Debug.Log(PlayerPrefs.GetString("LevelToLoad"));
        Debug.Log(PlayerPrefs.GetFloat("playerXPos"));
        Debug.Log(PlayerPrefs.GetFloat("playerYPos"));
        Debug.Log(PlayerPrefs.GetFloat("camXPos"));
        Debug.Log(PlayerPrefs.GetFloat("camYPos"));
    }
}