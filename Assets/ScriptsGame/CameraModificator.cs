﻿using UnityEngine;
using System.Collections;

public class CameraModificator : MonoBehaviour
{
    public float maxy;
    public float miny;
    public float switchSpeedMaxY = -10f;
    public float switchSpeedMinY = -10f;
    public bool cameraminmodif;
    public bool cameramaxmodif;
    public bool maxPositive;
    public bool minPositive;
    public bool done;
    public GameObject Camara;

    private void Start()
    {
        done = false;
        cameraminmodif = false;
        cameramaxmodif = false;
    }

    public void Update()
    {
        if (cameraminmodif)
        {
            CameraFollow.ymodif += switchSpeedMinY * Time.deltaTime;
        }
        if (cameramaxmodif)
        {
            CameraFollow.ymaxmodif += switchSpeedMaxY * Time.deltaTime;
        }
        if(CameraFollow.ymodif <= miny && !minPositive)

        {
            cameraminmodif = false;
        }
        if (CameraFollow.ymaxmodif <= maxy && !maxPositive)

        {
            cameramaxmodif = false;
        }
        if (CameraFollow.ymodif >= miny && minPositive)

        {
            cameraminmodif = false;
        }
        if (CameraFollow.ymaxmodif >= maxy && maxPositive)

        {
            cameramaxmodif = false;
        }
    }



    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Player" && done == false)
        {
            CameraFollow.ymaxmodif = Camara.transform.position.y;
            cameraminmodif = true;
            cameramaxmodif = true;
            done = true;
        }
    }
}