﻿using UnityEngine;
using System.Collections;

public class BigTreeCollider : MonoBehaviour {

    public GameObject Collider;
	
    void Start()
    {
        Collider.SetActive(false);
    }
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D info)
    {
        if (info.tag == "Player")
        {
            Collider.SetActive(true);
        }
	}
    void OnTriggerExit2D(Collider2D info)
    {
        if (info.tag == "Player")
        {
            Collider.SetActive(false);
        }   
    }
}
