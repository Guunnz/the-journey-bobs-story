﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MiscControl : MonoBehaviour
{
   
    public GameObject Snowball;
    public GameObject EndLevelFade;
    public GameObject Player;
    public GameObject cam;
   [SerializeField] static public bool onDialogue;
   static public float playerXPos;
   static public float playerYPos;
    static public float camYPos;
   static public float camXPos;
    public string LoadOnRestart;
    public string LoadOnLevelWonnered;
    public string CheckpointScene;
    public string sceneToLoad;
   static public bool reset = false;
    public int deleteData;
    static public int torrenteCheck;
    static public float camZPos;
    static public bool trineoDeath = false;
   public float CamZValue;
    public bool ifEnding;
    public bool ifBugged;


    void Start()
    {
        if (!ifBugged)
        {
            Application.targetFrameRate = 500;
        }
        if (ifBugged)
        {
            Application.targetFrameRate = 60;
        }
        torrenteCheck = PlayerPrefs.GetInt("torrenteCheck");
        sceneToLoad = PlayerPrefs.GetString("LevelToLoad");
        if (camZPos == 0)
        {
            camZPos = CamZValue;
        }
        if (StartupCheck.onStartup == 1)
        {
            LoadCheckpointFunction();
        }
    }
    
    void Update()
    {
        if (ifEnding)
        {
            LoadOnLevelWonnered = PlayerPrefs.GetString("Ending");
        }
        if (onDialogue && Input.GetKeyDown(KeyCode.E) && DialogueMaster.dialogue == 0 && DialogueMaster.thisdialogue) 
        {
            DialogueMaster.dialogue = 1;
            Platformer2DJG.inmovible = true;
        }
        if (!onDialogue)
        {
            Platformer2DJG.inmovible = false;
        }
        if (onDialogue && Input.GetKeyDown(KeyCode.E) && DialogueMaster1.dialogue == 0 && DialogueMaster1.thisdialogue)
        {
            DialogueMaster1.dialogue = 1;
            Platformer2DJG.inmovible = true;
        }
        if (onDialogue && Input.GetKeyDown(KeyCode.E) && NPCWGEM.dialogue == 0 && NPCWGEM.thisdialogue)
        {
            NPCWGEM.dialogue = 1;
            Platformer2DJG.inmovible = true;
        }
        if (onDialogue && Input.GetKeyDown(KeyCode.E) && NPCAftergem.dialogue == 0 && NPCAftergem.thisdialogue)
        {
            NPCAftergem.dialogue = 1;
            Platformer2DJG.inmovible = true;
        }
        if (onDialogue && Input.GetKeyDown(KeyCode.E) && YetiDialogueWGem.dialogue == 0 && YetiDialogueWGem.thisdialogue)
        {
            YetiDialogueWGem.dialogue = 1;
            Platformer2DJG.inmovible = true;
        }
        if (Input.GetKey(KeyCode.L))
        {
            deleteData += 1;
        }
        if (deleteData >= 10)
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Data Deleted");
        }
        if (trineoDeath)
        {
            trineoDeath = false;
            Shake.isShaking = false;
            EndLevelFade.SetActive(true);
            Invoke("LoadHardCheckpoint", 1.2f);
        }
    }
    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "SnowballTrigger")
        {
            Snowball.SetActive(true);
        }
        if (info.tag == "ShakeTrigger")
        {
            Shake.isShaking = true;
        }
        if (info.tag == "DeathTrigger")
        {
            Shake.isShaking = false;
            EndLevelFade.SetActive(true);
            Invoke("RestartLevel", 1);
        }
        if (info.tag == "EndLevel")
        {
            StartupCheck.onStartup = 0;
            PlayerPrefs.SetInt("onStartup", StartupCheck.onStartup);
            EndLevelFade.SetActive(true);
            Invoke("LoadLevel", 1.2f);
        }
        if (info.tag == "BadEnding")
        {
            StartupCheck.onStartup = 0;
            PlayerPrefs.SetInt("onStartup", StartupCheck.onStartup);
            EndLevelFade.SetActive(true);
            Invoke("LoadLevel", 5);
        }
        if (info.tag == "Snowball")
        {
            Shake.isShaking = false;
            Invoke("LoadHardCheckpoint", 0.2f);
        }
        if (info.tag == "TimerTrigger")
        {
            Timer.isOn = false;
        }
        if (info.tag == "HardDeath")
        {
            Shake.isShaking = false;
            EndLevelFade.SetActive(true);
            Invoke("LoadHardCheckpoint", 1.2f);
        }
        if (info.tag == "SoftDeath")
        {
            EndLevelFade.SetActive(true);
            Invoke("LoadCheckpointFunction", 1.2f);
        }
        if (info.tag == "JetpackPickup")
        {
            EndLevelFade.SetActive(true);
            Invoke("LoadLevel", 1.2f);
        }
    }

    void LoadCheckpoint()
        {
        Player.transform.position = new Vector3(playerXPos, playerYPos, 0);
        cam.transform.position = new Vector3(camXPos, camXPos, camZPos);
    }

    void LoadLevel()
    {
        SceneManager.LoadScene(LoadOnLevelWonnered, LoadSceneMode.Single);
    }

    void LoadHardCheckpoint()
    {
        SceneManager.LoadScene(CheckpointScene, LoadSceneMode.Single);
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(LoadOnRestart, LoadSceneMode.Single);
    }

    void LoadCheckpointFunction()
    {
        reset = true;
        Debug.Log(reset);
        playerXPos = PlayerPrefs.GetFloat("playerXPos");
        playerYPos = PlayerPrefs.GetFloat("playerYPos");
        camXPos = PlayerPrefs.GetFloat("camXPos");
        camYPos = PlayerPrefs.GetFloat("camYPos");
        LoadCheckpoint();
        Invoke("Reset",0.1f);
        Invoke("FadeOff", 1.2f);
    }
    void FadeOff()
    {
        EndLevelFade.SetActive(false);
        AudioListener.volume = 0.7f;
    }
    void Reset()
    {
        reset = false;
        Debug.Log(reset);
        
    }
}