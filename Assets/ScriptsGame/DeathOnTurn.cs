﻿using UnityEngine;
using System.Collections;

public class DeathOnTurn : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "Platform")
        {
            MiscControl.trineoDeath = true;
        }
    }
}