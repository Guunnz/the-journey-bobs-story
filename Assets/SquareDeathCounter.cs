﻿using UnityEngine;
using System.Collections;

public class SquareDeathCounter : MonoBehaviour {


    private int deaths;

	// Use this for initialization

    void Awake()
    {
        deaths = PlayerPrefs.GetInt("Deaths");
    }
	void Start ()

    {  
        deaths += 1;
        PlayerPrefs.SetInt("Deaths", deaths);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (deaths >= 10)
        {
            SteamScript.whichLogro = "Logro24";
            SteamScript.logro = "Logro24";
        }
	}
}
