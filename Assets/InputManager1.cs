﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputManager1 : MonoBehaviour {

   static public KeyCode LShift = KeyCode.Mouse0;
   static public KeyCode LControl = KeyCode.Mouse1;
   static public KeyCode Spacebar = KeyCode.Space;
    static public KeyCode E = KeyCode.E;
    public bool changingRightArrow;
   public bool changingLeftArrow;
   public bool changingUpArrow;
   public bool changingDownArrow;
   public bool changingLShift;
   public bool changingLControl;
   public bool changingSpacebar;
    public bool changingInteract;
    public Text LeftShift;
    public Text LeftControl;
    public Text Space;
    public Text Interact;
    public static InputManager1 instance;
    public bool dontdestroy;

    private void Awake()
    {
        if (dontdestroy)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        LeftShift.text = LShift.ToString();
        LeftControl.text = LControl.ToString();
        Space.text = Spacebar.ToString();
        Interact.text = E.ToString();
        if (PlayerPrefs.GetString("Jetpack") != "")
        {
            LShift = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jetpack"));
        }
        if (PlayerPrefs.GetString("Gauntlet") != "")
        {
            LControl = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Gauntlet"));
        }
        if (PlayerPrefs.GetString("Jump") != "")
        {
            Spacebar = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump"));
        }
        if (PlayerPrefs.GetString("Interact") != "")
        {
            E = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Interact"));
        }
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            Debug.Log("Derecha");
        }
        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            Debug.Log("Izquierda");
        }
        if (Input.GetAxisRaw("Vertical") < 0)
        {
            Debug.Log("Abajo");
        }
        if (Input.GetAxisRaw("Vertical") > 0)
        {
            Debug.Log("Arriba");
        }

        ////////////////////////////////////////////////////////////////////////////////////////
        foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(vKey) && changingLShift)
            {
                changingLShift = false;
                LShift = vKey;
                Debug.Log("LShift Arrow es:" + LShift);
                PlayerPrefs.SetString("Jetpack", LShift.ToString());
                LeftShift.text = LShift.ToString();
            }
            if (Input.GetKey(vKey) && changingLControl)
            {
                changingLControl = false;
                LControl = vKey;
                Debug.Log("LControl Arrow es:" + LControl);
                PlayerPrefs.SetString("Gauntlet", LControl.ToString());
                LeftControl.text = LControl.ToString();
            }
            if (Input.GetKey(vKey) && changingSpacebar)
            {
                changingSpacebar = false;
                Spacebar = vKey;
                Debug.Log("Spacebar es:" + Spacebar);
                PlayerPrefs.SetString("Jump", Spacebar.ToString());
                Space.text = Spacebar.ToString();
            }
            if (Input.GetKey(vKey) && changingInteract)
            {
                changingInteract = false;
                E = vKey;
                Debug.Log("Interact es:" + E);
                PlayerPrefs.SetString("Interact", E.ToString());
                Interact.text = E.ToString();
            }
        }
    }

    public void ChangeLShift()
    {
        changingLShift = true;
    }
    public void ChangeLControl()
    {
        changingLControl = true;
    }
    public void ChangeSpacebar()
    {
        changingSpacebar = true;
    }
    public void ChangeE()
    {
        changingInteract = true;
    }

}

 
