﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class CutsceneManager : MonoBehaviour
{
    public string sceneToLoad;
    private int dialogueNumber;
    private int dialogueManage;
    public int maxDialogue;
    public int dialogueReach = 5;
    public GameObject[] dialogos;
    public GameObject[] whoTalks;
    public GameObject EmptySpot;
    public GameObject endLevelFade;
    public bool called;
    public int emptySpot;
    public int dialogueReachAfterEmpty = 5;
    public int dialogueReachSuma = 5;
    public bool decision;
    
    // Update is called once per frame
    void Update()
    {
        if (dialogueNumber == emptySpot)
        {
            EmptySpot.SetActive(false);
            dialogueReach = dialogueReachAfterEmpty;
        }
        if (dialogueNumber == dialogueReach)
        {
            DisableText();
            dialogueReach += dialogueReachSuma;
            Debug.Log("Siguiente Hoja");
        }
        if (dialogueNumber == dialogueManage && dialogueNumber < maxDialogue && !called)
        {
            called = true;
            if (dialogueNumber != 0)
            {
                whoTalks[dialogueManage - 1].SetActive(false);
                dialogos[dialogueManage - 1].GetComponent<Text>().color = Color.gray;
            }
            whoTalks[dialogueManage].SetActive(true);
            Invoke("DialogueTiming", 0.2f);
        }
        if (Input.GetKeyDown(InputManager.E) && !decision && dialogueNumber != maxDialogue)
        {
            NextDialogue();
        }
        if (Input.GetKeyDown(InputManager.E) && decision && dialogueNumber < maxDialogue - 1)
        {
            NextDialogue();
        }
        if (dialogueNumber == maxDialogue)
        {
            endLevelFade.SetActive(true);
            Invoke("SiguienteEscena", 1.1f);
        }
    }
    void SiguienteEscena()
    {
        SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
    }
    void DialogueTiming()
    {
        dialogos[dialogueManage].SetActive(true);
    }
    void NextDialogue()
    {
        FindObjectOfType<AudioManager>().Play("Dialogo");
        called = false;
        dialogueNumber += 1;
        dialogueManage += 1;
    }

    void DisableText()
    {
        for (int i = 0; i < dialogos.Length; i++)
        {
            dialogos[i].SetActive(false);
        }
        for (int i = 0; i < whoTalks.Length; i++)
        {
            whoTalks[i].SetActive(false);
        }
    }
}

