﻿using UnityEngine;
using System.Collections;

public class SuperJump : MonoBehaviour

    
{
    static public bool EnableCollider = false;

    private void Update()
    {
        if (EnableCollider)
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
        if (!EnableCollider)
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            EnableCollider = false;
        }
    }
}
