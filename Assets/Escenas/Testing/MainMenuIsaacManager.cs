﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuIsaacManager : MonoBehaviour
{


    public Text highScore;
    static public int sceneToLoad;
    public string sceneToPlay = "";

    public int hScore;
    // Update is called once per frame

    void Start()
    {
        PlayerPrefs.SetInt("Square", 1);
        sceneToPlay = PlayerPrefs.GetString("LevelToLoad");
    }

    void Awake()
    {
        hScore = PlayerPrefs.GetInt("HighScore");
    }

    void Update()
    {
        highScore.text = "High Score: " + hScore.ToString();

    }



    public void Play()
    {
        SceneManager.LoadScene("IsaacInfinite", LoadSceneMode.Single);
    }
    public void Quit()
    {
    SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }
}
