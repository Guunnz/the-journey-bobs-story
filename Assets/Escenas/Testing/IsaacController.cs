﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class IsaacController : MonoBehaviour {


    public float Xspeed;
    public float Yspeed;
    public float diagXspeed = 8;
    public float diagYspeed = 8;
    public bool usingMovementX1;
    public bool usingMovementX2;
    public bool usingMovementY1;
    public bool usingMovementY2;
   static public int isaacHP = 3;
    static public int points;
    static public int monay;
    public int startupMoney = 0;
    public Text PointsText;
    public Text Money;
    public Text Lives;
    public Text Highscore;
    public bool gotHit;
    public int HighScore;
        

     void Start()
    {
        Application.targetFrameRate = 60;
        OpenPause.squarethon = true;
        points = 0;
        monay = startupMoney;
        UpgradeSystem.gm.onToggleUpgradeMenu += OnUpgradeMenuToggle;
        HighScore = PlayerPrefs.GetInt("HighScore");
        
    }
    // Update is called once per frame
    void Update()

    {
        PointsText.text = "Score: " + points.ToString();
        Highscore.text = "H. Score: " + HighScore.ToString();
        Money.text = "Money : $" + monay.ToString();
        Lives.text = "♥: " + isaacHP.ToString();
        PlayerPrefs.SetInt("Points", points);
        if (points >= HighScore)
        {
            HighScore = points;
            PlayerPrefs.SetInt("HighScore", HighScore);
        }
        if (isaacHP <= 0)
        {
            SceneManager.LoadScene("IsaacInfiniteGameOver");  
        }
        if (!usingMovementX1 && !usingMovementX2 && !usingMovementY1 && !usingMovementY2)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-Xspeed, 0);
            usingMovementX2 = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Xspeed, 0);
            usingMovementX1 = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, Yspeed);
            usingMovementY2 = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -Yspeed);
            usingMovementY1 = true;
        }
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(diagXspeed, -diagYspeed);
            usingMovementY1 = true;
            usingMovementX1 = true;
        }
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(diagXspeed, diagYspeed);
            usingMovementY2 = true;
            usingMovementX1 = true;
        }
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-diagXspeed, -diagYspeed);
            usingMovementY1 = true;
            usingMovementX2 = true;
        }
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-diagXspeed, diagYspeed);
            usingMovementY2 = true;
            usingMovementX2 = true;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            usingMovementX1 = false;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            usingMovementX2 = false;
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            usingMovementY2 = false;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            usingMovementY1 = false;
        }
    }
    void OnUpgradeMenuToggle(bool active)
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        if (active)
        {
            Xspeed = 0;
            Yspeed = 0;
            diagXspeed = 0;
            diagYspeed = 0;
        }
        if (!active)
        {
            diagXspeed = 8;
            diagYspeed = 8;
            Xspeed = 10;
            Yspeed = 10;
        }
    }
    private void OnParticleCollision(GameObject other)
        {
            if (other.CompareTag("Enemy") && gotHit == false)
        {
            FindObjectOfType<AudioManager>().Play("Tdmg");
            gotHit = true;
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
            isaacHP -= 1;
            Debug.Log(isaacHP + "HP / 3");
            Invoke("InvencibilityFrames", 0.5f);
            }
        }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Enemy" && gotHit == false)
        {
            FindObjectOfType<AudioManager>().Play("Tdmg");
            gotHit = true;
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.black;
            isaacHP -= 1;
            Debug.Log(isaacHP + "HP / 3 HP");
            Invoke("InvencibilityFrames", 0.5f);
        }
    }
    private void OnTriggerEnter2D(Collider2D info)
    {
        if (info.tag == "X")
        {
            transform.position = new Vector2(-22.18f, transform.position.y);
        }
        if (info.tag == "XM")
        {
            transform.position = new Vector2(22.18f, transform.position.y);
        }
        if (info.tag == "Y")
        {
            transform.position = new Vector2(transform.position.x, -12.45f);
            
        }
        if (info.tag == "YM")
        {
            transform.position = new Vector2(transform.position.x, 12.45f);
        }
    }

    void InvencibilityFrames()
        {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
        gotHit = false;
    }
}

