﻿using UnityEngine;
using System.Collections;

public class IsaacBrimstoneManager : MonoBehaviour {

    public GameObject BrimstoneX;
    public GameObject BrimstoneMinusX;
    public GameObject BrimstoneY;
    public GameObject BrimstoneMinusY;
    //

    public GameObject BrimstoneX1;
    public GameObject BrimstoneMinusX1;
    public GameObject BrimstoneY1;
    public GameObject BrimstoneMinusY1;
    //

    public GameObject BrimstoneX2;
    public GameObject BrimstoneMinusX2;
    public GameObject BrimstoneY2;
    public GameObject BrimstoneMinusY2;
    //
    public ParticleSystem.MinMaxCurve y;
    public ParticleSystem.MinMaxCurve x;
    public ParticleSystem.MinMaxCurve bounce;
    public float brimstoneCharge;
    public bool subBS;
    public bool brimX;
    public bool brimMx;
    public bool brimY;
    public bool brimMy;
    public float moveEffectPositive;
    public float moveEffectNegative;
   static public float StaticBrimDamage;
    public float brimDamage;
    static public float upgradeDamage;
    static public bool bouncing;
    public bool bouncingCalled;
   static public bool increaseLength;
    static public bool increaseSpeed;
    public GameObject camera;
    public GameObject chargeBar;
    static public float upgradeChargeSpeed;
    static bool started;

    void Start()
    {
        if (!started)
        {
            started = true;
            upgradeDamage = 0;
            upgradeChargeSpeed = 3.5f;
        }
    }
    // Update is called once per frame
    void Update ()
    {
        StaticBrimDamage = brimDamage + upgradeDamage;
        var xAxisGravityEffect = BrimstoneX.GetComponent<ParticleSystem>().forceOverLifetime;
        var xMinAxisGravityEffect = BrimstoneMinusX.GetComponent<ParticleSystem>().forceOverLifetime;
        var yAxisGravityEffect = BrimstoneY.GetComponent<ParticleSystem>().forceOverLifetime;
        var yMinAxisGravityEffect = BrimstoneMinusY.GetComponent<ParticleSystem>().forceOverLifetime;
        var bounceX = BrimstoneX.GetComponent<ParticleSystem>().collision;
        var bounceMX = BrimstoneMinusX.GetComponent<ParticleSystem>().collision;
        var bounceMY = BrimstoneMinusY.GetComponent<ParticleSystem>().collision;
        var bounceY = BrimstoneY.GetComponent<ParticleSystem>().collision;
        //
        var bounceX1 = BrimstoneX1.GetComponent<ParticleSystem>().collision;
        var bounceMX1 = BrimstoneMinusX1.GetComponent<ParticleSystem>().collision;
        var bounceMY1 = BrimstoneMinusY1.GetComponent<ParticleSystem>().collision;
        var bounceY1 = BrimstoneY1.GetComponent<ParticleSystem>().collision;
        //
        var bounceX2 = BrimstoneX2.GetComponent<ParticleSystem>().collision;
        var bounceMX2 = BrimstoneMinusX2.GetComponent<ParticleSystem>().collision;
        var bounceMY2 = BrimstoneMinusY2.GetComponent<ParticleSystem>().collision;
        var bounceY2 = BrimstoneY2.GetComponent<ParticleSystem>().collision;
        chargeBar.transform.localScale = new Vector2(brimstoneCharge, transform.localScale.y);
        if (bouncing && !bouncingCalled)
        {
            camera.layer = 14;
            bouncingCalled = true;
            bounceX.bounce = 0.7f;
            bounceMX.bounce = 0.7f;
            bounceMY.bounce = 0.7f;
            bounceY.bounce = 0.7f;
            //
            bounceX1.bounce = 0.7f;
            bounceMX1.bounce = 0.7f;
            bounceMY1.bounce = 0.7f;
            bounceY1.bounce = 0.7f;
            //
            bounceX2.bounce = 0.7f;
            bounceMX2.bounce = 0.7f;
            bounceMY2.bounce = 0.7f;
            bounceY2.bounce = 0.7f;
        }
        if (!bouncing && !bouncingCalled)
        {
            camera.layer = 1;
            bouncingCalled = false;
            bounceX.bounce = 0f;
            bounceMX.bounce = 0f;
            bounceMY.bounce = 0f;
            bounceY.bounce = 0f;
            //
            bounceX1.bounce = 0f;
            bounceMX1.bounce = 0f;
            bounceMY1.bounce = 0f;
            bounceY1.bounce = 0f;
            //
            bounceX2.bounce = 0f;
            bounceMX2.bounce = 0f;
            bounceMY2.bounce = 0f;
            bounceY2.bounce = 0f;
        }
        if (increaseLength)
        {
           increaseLength = false;
            BrimstoneY.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneX.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusY.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusX.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            //
            BrimstoneY1.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneX1.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusY1.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusX1.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            //
            BrimstoneY2.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneX2.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusY2.GetComponent<ParticleSystem>().startLifetime += 0.13f;
            BrimstoneMinusX2.GetComponent<ParticleSystem>().startLifetime += 0.13f;
        }
        if (increaseSpeed)
        {
            increaseSpeed = false;
            BrimstoneY.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneX.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusY.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusX.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            //
            BrimstoneY1.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneX1.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusY1.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusX1.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            //
            BrimstoneY2.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneX2.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusY2.GetComponent<ParticleSystem>().startSpeed += 1.2f;
            BrimstoneMinusX2.GetComponent<ParticleSystem>().startSpeed += 1.2f;
        }
        if (Input.GetKey(KeyCode.S) && subBS)
        {
            moveEffectPositive += 40 * Time.deltaTime;
            yAxisGravityEffect.x = 0f;
            yMinAxisGravityEffect.x = 0f;
            xAxisGravityEffect.y = moveEffectPositive;
            xMinAxisGravityEffect.y = moveEffectPositive;
        }
        if (Input.GetKey(KeyCode.W) && subBS)
        {
            moveEffectNegative -= 40 * Time.deltaTime;
            yAxisGravityEffect.x = 0f;
            yMinAxisGravityEffect.x = 0f;
            xAxisGravityEffect.y = moveEffectNegative;
            xMinAxisGravityEffect.y = moveEffectNegative;
        }
        if (Input.GetKey(KeyCode.A) && subBS)
        {
            moveEffectPositive += 40 * Time.deltaTime;
            xAxisGravityEffect.y = 0f;
            xMinAxisGravityEffect.y = 0f;
            yAxisGravityEffect.x = moveEffectPositive;
            yMinAxisGravityEffect.x = moveEffectPositive;
        }
        if (Input.GetKey(KeyCode.D) && subBS)
        {
            moveEffectNegative -= 40 * Time.deltaTime;
            xAxisGravityEffect.y = 0f;
            xMinAxisGravityEffect.y = 0f;
            yAxisGravityEffect.x = moveEffectNegative;
            yMinAxisGravityEffect.x = moveEffectNegative;
        }
        if (!subBS)
        {
            moveEffectNegative = 0;
            moveEffectPositive = 0;
            xAxisGravityEffect.y = 0f;
            xMinAxisGravityEffect.y = 0f;
            yAxisGravityEffect.x = 0f;
            yMinAxisGravityEffect.x = 0f;
        }
        if (subBS)
        {
            brimstoneCharge -= 2f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Stop("Charge");
            BrimstoneX.SetActive(false);
            BrimstoneMinusX.SetActive(true);
            BrimstoneY.SetActive(false);
            BrimstoneMinusY.SetActive(false);
            brimMx = true;
            brimMy = false;
            brimX = false;
            brimY = false;
        }
        if (Input.GetKey(KeyCode.RightArrow) && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Stop("Charge");
            BrimstoneX.SetActive(true);
            BrimstoneMinusX.SetActive(false);
            BrimstoneY.SetActive(false);
            BrimstoneMinusY.SetActive(false);
            brimMx = false;
            brimMy = false;
            brimX = true;
            brimY = false;
        }
        if (Input.GetKey(KeyCode.UpArrow) && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Stop("Charge");
            BrimstoneX.SetActive(false);
            BrimstoneMinusX.SetActive(false);
            BrimstoneY.SetActive(true);
            BrimstoneMinusY.SetActive(false);
            brimMx = false;
            brimMy = false;
            brimX = false;
            brimY = true;
        }
        if (Input.GetKey(KeyCode.DownArrow) && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Stop("Charge");
            BrimstoneX.SetActive(false);
            BrimstoneMinusX.SetActive(false);
            BrimstoneY.SetActive(false);
            BrimstoneMinusY.SetActive(true);
            brimMx = false;
            brimMy = true;
            brimX = false;
            brimY = false;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && brimMx && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Play("Charge");
            brimstoneCharge += upgradeChargeSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow) && brimY && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Play("Charge");
            brimstoneCharge += upgradeChargeSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow) && brimMy && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Play("Charge");
            brimstoneCharge += upgradeChargeSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow) && brimX && !subBS && !UpgradeSystem.onPause)
        {
            FindObjectOfType<AudioManager>().Play("Charge");
            brimstoneCharge += upgradeChargeSpeed * Time.deltaTime;

        }

        if (brimstoneCharge >= 2)
            {
            brimstoneCharge = 2;
            }
        if (Input.GetKeyUp(KeyCode.RightArrow) && brimstoneCharge >= 2 && brimX)
        {
            gameObject.GetComponent<AudioSource>().Play();
            subBS = true;
            BrimstoneMinusX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneX.GetComponent<ParticleSystem>().enableEmission = true;
            BrimstoneY.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneMinusY.GetComponent<ParticleSystem>().enableEmission = false; 
        }
        if (Input.GetKeyUp(KeyCode.UpArrow) && brimstoneCharge >= 2 && brimY)
        {
            gameObject.GetComponent<AudioSource>().Play();
            subBS = true;
            BrimstoneMinusX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneY.GetComponent<ParticleSystem>().enableEmission = true;
            BrimstoneMinusY.GetComponent<ParticleSystem>().enableEmission = false;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) && brimstoneCharge >= 2 && brimMy)
        {
            gameObject.GetComponent<AudioSource>().Play();
            subBS = true;
            BrimstoneMinusX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneY.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneMinusY.GetComponent<ParticleSystem>().enableEmission = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) && brimstoneCharge >= 2 && brimMx)
        {
            gameObject.GetComponent<AudioSource>().Play();
            subBS = true;
            BrimstoneMinusX.GetComponent<ParticleSystem>().enableEmission = true;
            BrimstoneX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneY.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneMinusY.GetComponent<ParticleSystem>().enableEmission = false;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow) && brimstoneCharge < 2 && brimX)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            subBS = true;
        }
        if (Input.GetKeyUp(KeyCode.UpArrow) && brimstoneCharge < 2 && brimY)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            subBS = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) && brimstoneCharge < 2 && brimMx)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            subBS = true;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) && brimstoneCharge < 2 && brimMy)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            subBS = true;
        }
        if (brimstoneCharge <= 0)
        {
            BrimstoneX.SetActive(false);
            BrimstoneMinusX.SetActive(false);
            BrimstoneY.SetActive(false);
            BrimstoneMinusY.SetActive(false);
            BrimstoneMinusX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneX.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneY.GetComponent<ParticleSystem>().enableEmission = false;
            BrimstoneMinusY.GetComponent<ParticleSystem>().enableEmission = false;
            subBS = false;
            brimstoneCharge = 0;
        }
    }
}
